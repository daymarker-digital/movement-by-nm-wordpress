<?php

  /*
  *
  *	 Filename: category.php
  *
  */

  get_header();


  $DD = new DaymarkerDigital();
  $Templates = new DDTemplates();

  // ---------------------------------------- Template Data
  $template = 'category';
  $queried_object = get_queried_object() ?: [];
  $queried_object_name = $queried_object->name ?? '';
  $queried_object_taxonomy = $queried_object->taxonomy ?? '';
  $queried_object_term_id = $queried_object->term_id ?? '';
  $term_desc = term_description( $queried_object_term_id, $queried_object_taxonomy ) ?: '';
  $paged = get_query_var( 'paged' ) ?: 1;
  $wp_query_index = 1;

  echo '<div class="magazine">';

    include( locate_template( './snippets/magazine-header.php' ) );

    echo '<div class="magazine__body">';
      // ---------------------------------------- Template
      if ( have_posts() ) {

        $grid_items_count = $wp_query->found_posts;

        while ( have_posts() ) {

          the_post();

          $post_id = get_the_ID();
          $post_type = get_post_type( $post_id ) ?: '';
          $post_title = get_the_title( $post_id ) ?: '';

          echo $Templates->render( 'article.preview', [ 'post_id' => $post_id, 'count' => $wp_query_index, 'type' => 'magazine' ] );

          $wp_query_index++;

        }

        if ( $wp_query->max_num_pages > 1 )  {

          $pagination_links = [];
          $max_pages = $wp_query->max_num_pages;
          $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
          $pagination_args = [
            'base' => get_pagenum_link(1) . '%_%',
            'format' => $pagination_format,
            'total' => $max_pages,
            'current' => $paged,
            'aria_current' => false,
            'show_all' => true,
            'end_size' => 1,
            'mid_size' =>2,
            'prev_next' => false,
            'prev_text' => '',
            'next_text' => '',
            'type' => 'array',
            'add_args' => false,
            'add_fragment' => '',
            'before_page_number' => '',
            'after_page_number' => ''
          ];

          $pagination_links = paginate_links( $pagination_args );

          echo '<div class="magazine__pagination pagination">';

            echo '<div class="pagination__button pagination__button--prev">';
              if ( get_previous_posts_link( 'prev' ) ) {
                echo get_previous_posts_link( 'prev' );
              } else {
                echo '<span>prev</span>';
              }
            echo '</div>';

            echo '<div class="pagination__vr"></div>';

            if ( $pagination_links && !empty( $pagination_links ) ) {
              echo '<ul class="pagination__links">';
              foreach( $pagination_links as $i => $link ) {
                echo '<li class="pagination__links-item">' . $link . '</li>';
              }
              echo '</ul>';
            }

            echo '<div class="pagination__vr"></div>';

            echo '<div class="pagination__button pagination__button--next">';
              if ( get_next_posts_link( 'next', $max_pages ) ) {
                echo get_next_posts_link( 'next', $max_pages );
              } else {
                echo '<span>next</span>';
              }
            echo '</div>';

          echo '</div>';

        }

      }
    echo '</div>';

  echo '</div>';

  get_footer();

?>
