<?php

/*
*
* Template Name: Page [ Contact Us ]
* Filename: page-contact-us.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

include( locate_template( './snippets/layout--hero.php' ) );

include( locate_template( './snippets/layout--contact-us.php' ) );

get_footer();

?>
