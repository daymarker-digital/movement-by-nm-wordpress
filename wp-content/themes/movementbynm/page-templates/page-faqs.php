<?php

/*
*
* Template Name: Page [ FAQs ]
* Filename: page-faqs.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

include( locate_template( './snippets/layout--hero.php' ) );

include( locate_template( './snippets/layout--faqs.php' ) );

get_footer();

?>
