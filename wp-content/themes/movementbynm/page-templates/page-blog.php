<?php

/*
*
*  Template Name: Page [ Blog ]
*  Filename: page-blog.php
*
*/

get_header();

echo '<div class="magazine">';
  include( locate_template( './snippets/magazine/header.php' ) );
  include( locate_template( './snippets/magazine/body.php' ) );
echo '</div>';

get_footer();

?>
