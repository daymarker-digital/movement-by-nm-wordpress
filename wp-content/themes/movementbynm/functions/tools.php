<?php

$tools = new Tools();
add_action( 'init', [ $tools, 'init' ] );

class Tools {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Tools';
  private $version = '1.0';

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Instance
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Register Menus
  public function register_menus(){
  	register_nav_menus([
      'main-menu' => __( 'Main Menu' )
    ]);
  }

  // ---------------------------------------- Is Localhost
  public function is_localhost() {
  	$whitelist = [ '127.0.0.1', '::1' ];
  	if ( in_array( $_SERVER['REMOTE_ADDR'], $whitelist ) ) {
  		return true;
  	}
  }

  // ---------------------------------------- Initialize
  public function init() {

    $this->register_menus();

    if ( $this->is_localhost() ) {
      add_filter( 'http_request_args', function ( $r ) {
      	$r['sslverify'] = false;
      	return $r;
      });
    }

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Static
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Debug This
  public static function debug_this( $object = null, $object_name = '', $print = true ) {

    $html = '<div class="php-debugger" style="background: black; width: 90%; display: block; margin: 65px auto; padding: 40px 25px; position: relative; z-index: 10;">';
  	  $html .= '<pre style="color: #fff;">';
        $html .= $object_name ? '<h3 style="margin-bottom: 20px;">Name: ' . $object_name . '</h3>' : '';
  		  $html .= '<h3 style="margin-bottom: 20px;">Type:</h3>';
  		  $html .= gettype ( $object );
  		  $html .= '<h3 style="margin-bottom: 20px;">Contents:</h3>';
  		  $html .= print_r( $object, true );
  	  $html .= '</pre>';
	  $html .= '</div>';

    if ( $print ) {
      echo $html;
    } else {
      return $html;
    }

  }

  // ---------------------------------------- Handleize
  public static function handleize( $string = '' ) {

    // removes html
  	$string = strip_tags( $string );

    //Lower case everything
    $string = strtolower( $string );

    //Make alphanumeric (removes all other characters)
    $string = preg_replace( "/[^a-z0-9_\s-]/", "", $string );

    //Clean up multiple dashes or whitespaces
    $string = preg_replace( "/[\s-]+/", " ", $string );

    //Convert whitespaces and underscore to dash
    $string = preg_replace( "/[\s_]/", "-", $string );

    return $string;

  }

  // ---------------------------------------- Is Child
  public static function is_child() {

    global $post;

	  if ( is_page() && $post->post_parent ) {
		  return true;
	  }

    return false;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
