<?php

$acf_options = new ACFOptions();
add_action( 'acf/init', [ $acf_options, 'init' ] );
add_filter( 'allowed_block_types_all', [ $acf_options, 'allowed_blocks' ], 10, 2 );

class ACFOptions {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'VP ACF Options';
  private $version = '2.0';

  public $gutenberg_blocks = [
    'app_feature' => [
      'title' => 'App Feature',
      'name' => 'app-feature',
    ],
    'feature_carousel' => [
      'title' => 'Feature Carousel',
      'name' => 'feature-carousel',
    ],
    'fifty_fifty' => [
      'title' => 'Fifty-Fifty',
      'name' => 'fifty-fifty',
    ],
    'instagram_feed' => [
      'title' => 'Instagram Feed',
      'name' => 'instagram-feed',
    ],
    'hero' => [
      'title' => 'Hero',
      'name' => 'hero',
    ],
    'lead_generation' => [
      'title' => 'Lead Generation',
      'name' => 'lead-generation',
    ],
    'logo_carousel' => [
      'title' => 'Logo Carousel',
      'name' => 'logo-carousel',
    ],
    'pricing' => [
      'title' => 'Pricing',
      'name' => 'pricing',
    ],
    'testimonials' => [
      'title' => 'Testimonials',
      'name' => 'testimonials',
    ],
    'text' => [
      'title' => 'Text',
      'name' => 'text',
    ],
    'text_feature' => [
      'title' => 'Text Feature',
      'name' => 'text-feature',
    ],
  ];
  public $theme_settings = [
    'slug' => 'theme-settings',
    'options' => [
      'Announcements',
      'Footer',
      'Header',
      'Lead Generation',
      'Magazine',
      'Mobile Menu',
      'Typography',
      '404',
    ]
  ];

  /*
  //////////////////////////////////////////////////////////
  ////  Methods | Instance
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Register Block Types
  public function register_block_types() {

    if ( $this->gutenberg_blocks ) {
      foreach( $this->gutenberg_blocks as $id => $block ) {
        acf_register_block_type( $this->block_settings( $block ) );
      }
    }

  }

  // ---------------------------------------- Register Options
  public function register_main_options() {

    acf_add_options_page(
  		array(
  			'page_title' => 'Theme Settings',
  			'menu_title' => 'Theme Settings',
  			'menu_slug' => $this->theme_settings['slug'],
  			'capability' => 'edit_posts',
  			'parent_slug' => '',
  			'position' => '2.1',
  			'icon_url' => false
  		)
  	);

  }

  // ---------------------------------------- Register Sub Options
  public function register_sub_options() {

    $main_slug = $this->theme_settings['slug'];

    foreach ( $this->theme_settings['options'] as $title ) {

      $title_slug = Tools::handleize( $title );

      $args = [
        'capability' => 'edit_posts',
        'menu_slug' => $main_slug . '-' . $title_slug,
  			'menu_title' => $title,
        'page_title' => $title,
  			'parent_slug' => $main_slug,
      ];

      acf_add_options_sub_page( $args );

    }

  }

  // ---------------------------------------- Allowed Gutenberg Blocks
  public function allowed_blocks( $allowed_block_types, $editor_context ) {

    if ( ! empty( $editor_context->post ) ) {

      if ( 'post' === $editor_context->post->post_type ) {

        return array(
          'core/paragraph',
        );

      } else {

        $allowed_blocks = [];

        if ( $this->gutenberg_blocks ) {
          foreach( $this->gutenberg_blocks as $id => $block ) {
            $allowed_blocks[] = 'acf/' . $block['name'];
          }
        }

        $allowed_blocks[] = 'core/paragraph';

        return $allowed_blocks;

      }
    }

    return $allowed_block_types;

  }

  // ---------------------------------------- Block Settings
  public function block_settings( $block = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'category' => 'common',
        'description' => '',
        'icon' => 'admin-comments',
        'keywords' => [],
        'name' => '',
        'post_types' => [ 'post', 'page' ],
        'title' => '',
      ],
      $block
    ));

    $render_template = '/snippets/blocks/' . $name . '.php';

    return [
      'name'              => $name,
      'title'             => __( $title ),
      'description'       => __( $description ),
      'render_template'   => $render_template,
      'category'          => $category,
      'icon'              => $icon,
      'keywords'          => $keywords,
      'post_types'        => $post_types,
      'mode'              => 'edit',
      'enqueue_assets'    => function(){},
    ];

  }

  // ---------------------------------------- Init
  public function init() {

    if ( function_exists('acf_register_block_type') ) {
      $this->register_block_types();
    }

    if ( function_exists('acf_add_options_page') ) {
      $this->register_main_options();
      $this->register_sub_options();
    }

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
