<?php

class CustomThemeBase {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Custom Theme Base';
  private $version = '2.0';

  public $custom_image_title = 'custom-image-size';
  public $custom_image_sizes = [ 1, 10, 90, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];

  /*
  //////////////////////////////////////////////////////////
  ////  Methods
  //////////////////////////////////////////////////////////
  */

  // --------------------------- Get Acronym from Words
  public function get_acronym_from_words( $words = '' ) {

    $acronym = '';

    $words = preg_split("/[\s,_-]+/", $words );

    if ( $words ) {
      foreach ( $words as $i => $word ) {
        $acronym .= $word[0];
      }
    }

    return $acronym;

  }

  // --------------------------- Featured Image
  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = [];
    $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  // --------------------------- Google Maps Directions Link
  public function get_google_maps_directions_link( $params = [] ) {

    $html = '';
    $base = 'https://www.google.com/maps/search/?api=1&query=';

    if ( $params ) {

      // default data
      $name = $city = $country = $region = $postal = $address = $address_2 = false;

      // extract $params
      extract( $params );

      $html .= ( $address ) ? $address : '';
      $html .= ( $city ) ? ' ' . $city : '';
      $html .= ( $region ) ? ' ' . $region : '';
      $html .= ( $postal ) ? ' ' . $postal : '';
      $html .= ( $country ) ? ' ' . $country : '';
      $html .= ( $name ) ? ' ' . $name : '';

      if ( $address_2 ) {
        $html = $address_2 . '–' . $html;
      }

      if ( $html ) {
        $html = $base . trim($html);
      }

    }

    return $html;

  }

  // --------------------------- Theme Classes
  public function get_theme_classes() {

    global $post;
    global $template;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$template = basename( $template, '.php' );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--' . $template;
  		$classes .= ' ' . $template;

      if ( is_front_page() ) {
        $classes .= ' is-front-page';
      }

      if ( is_page() ) {
        $classes .= ' is-page';
      }

      if ( is_page_template( 'page-templates/page--about-us.php') ) {
        $classes .= ' is-about-us-page';
      }

      if ( is_single() ) {
        $classes .= ' is-single';
      }

  	}

    if ( is_archive() ) {
      $classes .= ' is-archive';
    }

    if ( is_category() ) {
      $classes .= ' is-category';
    }

    if ( is_404() ) {
      $classes .= ' is-404';
    }

  	return trim($classes);

  }

  // --------------------------- Theme Directory
  public function get_theme_directory( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  // --------------------------- Theme Info
  public function get_theme_info( $param = 'version' ) {

    global $post;

    switch ( $param ) {
      case 'object_ID':
				$html = get_queried_object_id();
				break;
      case 'php_version':
				$html = phpversion();
				break;
      case 'post_ID':
				$html = ( $post ) ? $post->ID : 'no-post-id';
				break;
			case 'post_type':
				$html = get_post_type( $post->ID );
				break;
      case 'template':
				$html = basename( get_page_template(), ".php" );
				break;
      case 'version':
				$html = $this->version;
				break;
      case 'vitals':
        $html = "<!-- PHP Version: " . $this->get_theme_info( 'php_version' ) . " -->";
  	    $html .= "<!-- WP Version: " . $this->get_theme_info( 'wp_version' ) . " -->";
  	    $html .= "<!-- Current Template: " . $this->get_theme_info( 'template' ) . " -->";
  	    $html .= "<!-- Post ID: " . $this->get_theme_info( 'post_ID' ) . " -->";
        $html .= "<!-- Object ID: " . $this->get_theme_info( 'object_ID' ) . " -->";
				break;
      case 'wp_version':
				$html = get_bloginfo( "version" );
				break;
      default:
        $html = '';
        break;
    }

    return $html;

  }

  // --------------------------- Get Unique ID
  public function get_unique_id( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'prefix' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $html .= $prefix ? $prefix . md5(uniqid(rand(), true)) : md5(uniqid(rand(), true));

    // ---------------------------------------- Template
    return $html;

  }

  // --------------------------- AOS Attributes
  public function render_aos_attributes( $params = [] ) {

    /*
    *  Note:
    *  AOS library (https://www.npmjs.com/package/aos) needs to be installed
    *  and initialized. JS and CSS files are required for anything to happen.
    *
    *  Timing:
    *  Both delay and duration must be increments of 50
    *
    *  Bugs:
    *  Offset and Mirror are buggy. Disable them for now.
    */

    // ---------------------------------------- Defaults
    $settings = array_merge(
      [
        'anchor' => '',                         // element id, include '#'
        'anchor_placement' => 'top-bottom',
        'delay' => $this->aos_delay,
        'duration' => 550,
        'easing' => 'ease-in-out',
        // 'mirror' => 'false',                 // DEF buggy, hide for now
        'offset' => 175,                          // buggy, so hide for now (...or?)
        'once' => 'true',
        'transition' => 'fade-up',
      ],
      $params
    );

    $html = '';

    foreach( $settings as $key => $value ) {
      switch( $key ) {
        case 'transition':
          $html .= ' data-aos="' . $value . '"';
          break;
        case 'anchor_placement':
          $html .= ' data-aos-anchor-placement="' . $value . '"';
          break;
        default:
          $html .= ' data-aos-' . $key . '="' . $value . '"';
      }
    }

    return $html;

  }

  // --------------------------- Container
  public function render_container( $state = 'open', $col = 'col-12', $container = 'container' ) {

    $html = '<div class="' . $container . '"><div class="row"><div class="' . $col . '">';
    if ( 'open' !== $state ) {
      $html = '</div></div></div>';
    }
    return $html;

  }

  // --------------------------- Call to Action
  public function render_cta( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'cta',
        'classes' => '',
        'cta' => [],
        'html' => '',
        'justification' => 'not-set',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $cta_link = '';
    $cta_rel = 'noopener';
    $cta_style = $cta['style'] ?? 'not-set';
    $cta_target = '_self';
    $cta_title = $cta['title'] ?? '';
    $cta_type = $cta['type'] ?? 'not-set';

    switch( $cta_type ) {
      case 'category':
        $link_id = $cta['link_category'] ?? '';
        $cta_link = get_category_link( $link_id ) ?: '';
        break;
      case 'external':
        $cta_link = $cta['link_external'] ?? '';
        $cta_rel = 'noopener';
        $cta_target = '_blank';
        break;
      case 'internal':
        $cta_link = $cta['link_internal'] ?? '';
        break;
    }

    // ---------------------------------------- Template
    if ( $cta_link && $cta_title ) {
      $html .= '<div class="' . $block_classes . '" data-justify="' . $justification . '">';
        $html .= $this->render_link([
          'link' => $cta_link,
          'title' => $cta_title,
          'style' => $cta_style,
          'target' => $cta_target,
          'rel' => $cta_rel
        ]);
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Form Field
  public function render_form_field( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'accept' => '',
        'block_name' => 'form',
        'checkbox_options' => [],
        'checbox_type' => 'boolean',
        'disabled' => false,
        'error_message' => [
          'checkbox' => 'This field is required',
          'email' => 'Please enter a valid email address',
          'file' => 'File type or file size incorrect',
          'tel' => 'Please enter a valid phone number',
          'select' => 'Please select a valid option',
          'text' => 'This field cannot be blank',
          'textarea' => 'This field cannot be blank',
        ],
        'html' => '',
        'label' => '',
        'multiple' => false,
        'name' => '',
        'placeholder' => '',
        'required' => false,
        'select_options' => [],
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    $input_classes = $block_name . '__input input input--' . $type;
    $input_classes .= $required ? ' required' : '';

    $field_classes = $block_name . '__field field field--' . $type;
    $field_classes .= $multiple ? ' field--' . $type . '-multiple' : '';
    $field_classes .= 'rude' === $name ? ' field--' . $name : '';

    $label_classes = $block_name . '__label label label--' . $type;
    $label_classes .= $multiple ? ' label--' . $type . '-multiple' : '';

    $select_classes = $block_name . '__select select';
    $select_classes .= $required ? ' required' : '';
    $select_classes .= $multiple ? ' ' . $type . '--multiple' : '';

    $textarea_classes = $block_name . '__textarea textarea';
    $textarea_classes .= $required ? ' required' : '';

    $label = ( $required && $label ) ? $label . '<span class="required-marker">*</span>' : $label;

    if ( $name && $type ) {
      $html .= '<div class="' . $field_classes . '">';

      switch ( $type ) {

        /*
        //////////////////////////////////////////////////////////
        ////  Checkbox
        //////////////////////////////////////////////////////////
        */

        case 'checkbox': {
          if ( $label ) {
            $html .= '<div class="checkbox">';
              $html .= '<label><div class="checkbox__label">' . $label . '</div>';
                $html .= '<input
                  class="' . $input_classes . '"
                  type="' . $type . '"
                  name="' . $name . '"
                  value="' . $value . '"
                />';
                $html .= '<div class="checkbox__content">';
                  if ( 'boolean' === $checbox_type ) {
                    $html .= '<div class="checkbox__value">' . $value . '</div>';
                  }
                $html .= '</div>';
              $html .= '</label>';
            $html .= '</div>';
          }
          break;
        }

        /*
        //////////////////////////////////////////////////////////
        ////  Email, File, Tel & Text
        //////////////////////////////////////////////////////////
        */

        case 'email':
        case 'file':
        case 'tel':
        case 'text': {

          $html .= $label ? '<label class="' . $label_classes . '">' . $label . '</label>' : '';
          $html .= '<input
            class="' . $input_classes . '"
            type="' . $type . '"
            name="' . $name . '"
            ' . ( 'rude' === $name ? ' tabindex="-1"' : '' ) .'
            ' . ( $value ? ' value="' . $value . '"' : '' ) . '
            ' . ( $placeholder ? ' placeholder="' . $placeholder . '"' : '' ) . '
            ' . ( $disabled ? ' disabled' : '' ) . '
            ' . ( $multiple ? ' multiple' : '' ) . '
            ' . ( $accept ? ' accept="' . $accept . '"' : '' ) . '
          >';

          break;

        }

        /*
        //////////////////////////////////////////////////////////
        ////  Select
        //////////////////////////////////////////////////////////
        */

        case 'select': {
          $html .= $label ? '<label class="' . $label_classes . '">' . $label . '</label>' : '';
          $html .= '<select
            class="' . $select_classes . '"
            name="' . $name . '"
            ' . ( $multiple ? ' multiple' : '' ) . '
          >';
            $html .= !$multiple ? '<option value="">Select one</option>' : '';
            if ( $select_options ) {
              foreach ( $select_options as $item ) {
                $value = $item['value'] ?? '';
                $title = $item['title'] ?? $value;
                if ( $value ) {
                  $html .= '<option value="' . $value . '">' . $title . '</option>';
                }
              }
            }
          $html .= '</select>';
          break;
        }

        /*
        //////////////////////////////////////////////////////////
        ////  Textarea
        //////////////////////////////////////////////////////////
        */

        case 'textarea':

          $html .= $label ? '<label class="' . $label_classes . '">' . $label . '</label>' : '';
          $html .= '<textarea
            class="' . $textarea_classes . '"
            name="' . $name . '"
            ' . ( $placeholder ? ' placeholder="' . $placeholder . '"' : '' ) . '
          ></textarea>';

          break;

      }

      $html .= '<div class="field__error-message error-message">' . ( $error_message[$type] ?? '' ) . '</div>';

      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Form Input
  public function render_form_input( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'classes' => '',
        'hidden' => false,
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html = '<input
        ' . ( $classes ? 'class="' . trim( $classes ) . '"' : '' ) . '
        type="' . $type . '"
        name="' . $name . '"
        autocomplete="' . $type . '"
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $type === 'email' ? 'spellcheck="false" autocapitalize="off"' : '' ) . '
        ' . ( $type === 'tel' ? 'pattern="[0-9\-]*"' : '' ) . '
      />';

    }

    return $html;

  }

  // --------------------------- Google Analytics
  public function render_google_analytics( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'type' => 'default',
        'id' => '',
        'html' => '',
      ],
      $params
    ));

    if ( $id ) {
      switch ( $type ) {
        case 'default':
          $html .= '<script>';
          $html .= '(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":
            new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src=
            "https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,"script","dataLayer",' . $id . ');';
          $html .= '</script>';
          break;
      }
    }

    return $html;

  }

  // ---------------------------------------- Lazyload iFrame
  public function render_lazyload_iframe( $params = [] ) {

    $defaults = [
      'aspect_ratio' => '16-9',
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'preload' => false,
      'video_id' => '', // 160730254, 163590531, 221632885
      'video_source' => 'vimeo',
    ];

    extract( array_merge( $defaults, $params ) );

    $iframe_classes = 'lazyload lazyload-item lazyload-item--iframe';
    $iframe_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $iframe_classes .= $preload ? ' lazypreload' : '';
    $iframe_classes = $classes ? $classes . ' ' . $iframe_classes : $iframe_classes;

    $video_source_url = ( 'vimeo' == $video_source ) ? 'https://player.vimeo.com/video/' : 'https://www.youtube.com/embed/';
    $video_source_url .= $video_id;
    $video_source_url .= ( $background ) ? '?autoplay=1&loop=1&autopause=0&muted=1&background=1' : '?autoplay=0&loop=1&autopause=0&muted=0&background=0';

    if ( $video_source_url && $video_id ) {

      $html = '
        <iframe
          class="' . trim($iframe_classes) . '"
          data-aspect-ratio="' . $aspect_ratio . '"
          data-src="' . $video_source_url . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Embed
  public function render_lazyload_embed( $params = [] ) {

    $defaults = [
      'aspect_ratio' => '16-9',
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'id' => '', // 160730254, 163590531, 221632885
      'preload' => false,
      'source' => 'vimeo',
    ];

    extract( array_merge( $defaults, $params ) );

    $embed_classes = 'lazyload lazyload-item lazyload-item--embed';
    $embed_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $embed_classes .= $preload ? ' lazypreload' : '';
    $embed_classes = $classes ? $classes . ' ' .  $embed_classes :  $embed_classes;

    switch ( $source ) {
      case 'vimeo': {
        $embed_source = 'https://player.vimeo.com/video/' . $id;
        if ( $background ) {
          $embed_source .= '?controls=0&keyboard=0&background=1&loop=1';
        } else {
          $embed_source .= '?controls=1&keyboard=1&background=0&loop=0';
        }
        break;
      }
      case 'youtube': {
        $embed_source = 'https://www.youtube.com/embed/' . $id;
        if ( $background ) {
          $embed_source .= '?controls=0&disablekb=1&loop=1';
        } else {
          $embed_source .= '?controls=1&disablekb=0&loop=0';
        }
        break;
      }
    }

    if ( $embed_source && $id ) {

      $html = '
        <iframe
          class="' . trim($embed_classes) . '"
          data-aspect-ratio="' . $aspect_ratio . '"
          data-src="' . $embed_source . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_lazyload_image( $params = [] ) {

    $defaults = [
      'alt_text' => '',
      'background' => false,
      'classes' => '',
      'custom_sizes_title' => $this->custom_image_title,
      'custom_sizes' => $this->custom_image_sizes,
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'image' => [],
      'image_srcset' => '',
      'is_svg' => false,
      'preload' => false,
    ];

    extract( array_merge( $defaults, $params ) );

    $image_classes = 'lazyload lazyload-item lazyload-item--image';
    $image_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $image_classes .= $preload ? ' lazypreload' : '';
    $image_classes = $classes ? $classes . ' ' . $image_classes : $image_classes;

    $image_alt_text = $alt_text ? $alt_text : ( isset($image['alt']) && !empty($image['alt']) ? $image['alt'] : '' );
    $image_full_source = isset($image['url']) ? $image['url'] : false;
    $image_placeholder_source = isset($image['sizes'][$custom_sizes_title . '-10']) ? $image['sizes'][$custom_sizes_title . '-10'] : false;
    $image_width = isset($image['width']) ? $image['width'] : '';
    $image_height = isset($image['height']) ? $image['height'] : '';

    $is_svg = ( isset($image['subtype']) && 'svg+xml' == $image['subtype'] ) ? true : false;

    foreach ( $custom_sizes as $i => $size ) {
      $image_srcset .= ( $i > 0 ) ? ', ' : '';
      $image_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
    }

    if ( $image_full_source ) {
      if ( $background ) {

        $html = '<div
          class="' . $image_classes . '"
          data-bg="' . $image_full_source . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"';
        $html .= $image_placeholder_source ? ' style="background-url(' . $image_placeholder_source . ')"' : '';
        $html .= '></div>';

       } else {

        if ( $is_svg ) {
          $html = '<img
            src="' . $image_full_source . '"
            data-src="' . $image_full_source . '"
            class="' . $image_classes . '"';
            $html .= $image_width ? ' width="' . $image_width . '"' : '';
            $html .= $image_height ? ' height="' . $image_height . '"' : '';
            $html .= $image_alt_text ? ' alt="' . $image_alt_text . '"' : '';
          $html .= ' />';
        } else {
          $html = '<img
            class="' . $image_classes . '"
            data-src="' . $image_full_source . '"
            data-transition-delay="' . $delay . '"
            data-transition-duration="' . $duration . '"';
          $html .= $image_width ? ' width="' . $image_width . '"' : '';
          $html .= $image_height ? ' height="' . $image_height . '"' : '';
          $html .= $image_srcset ? ' data-sizes="auto" data-srcset="' . $image_srcset . '"' : '';
          $html .= $image_placeholder_source ? ' src="' . $image_placeholder_source . '"' : '';
          $html .= $image_alt_text ? ' alt="' . $image_alt_text . '"' : '';
          $html .= ' />';
        }

      }
    }

    return $html;

  }

  // ---------------------------------------- Lazyload Video
  public function render_lazyload_video( $params = [] ) {

    $defaults = [
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'mime_type' => '',
      'preload' => false,
      'video' => [],
      'video_url' => '',
    ];

    extract( array_merge( $defaults, $params ) );

    $video_classes = 'lazyload lazyload-item lazyload-item--video';
    $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $video_classes .= $preload ? ' lazypreload' : '';
    $video_classes = $classes ? $classes . ' ' . $video_classes : $video_classes;

    $video_url = isset($video['url']) ? $video['url'] : false;
    $mime_type = isset($video['mime_type']) ? $video['mime_type'] : false;

    if ( $video_url && $mime_type ) {
      $html = '<video
        class="' . $video_classes . '"
        src="' . $video_url . '"
        data-transition-delay="' . $delay . '"
        data-transition-duration="' . $duration . '"
        preload="none"
        muted=""
        data-autoplay=""
        data-poster=""
        loop
        playsinline
        muted
      >';
        $html .= '<source src="' . $video_url . '" type="' . $mime_type . '">';
      $html .= '</video>';
    }

    return $html;

  }

  // ---------------------------------------- Link
  public function render_link( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'active' => false,
        'block_name' => 'link',
        'classes' => '',
        'html' => '',
        'link' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $block_classes .= $style ? ' ' . $block_name . '--' . $style : '';
    $block_classes .= $active ? ' active' : '';

    // ---------------------------------------- Template
    if ( $title && $link ) {
      $html .= '<a';
      $html .= ' class="' . $block_classes . '"';
      $html .= ' href="' . $link . '"';
      $html .= ' target="' . $target . '"';
      $html .= ' title="' . str_replace( '<br>', ', ', $title ) . '"';
      $html .= $rel ? ' rel="' . $rel . '"' : '';
      $html .= '>';
        $html .= '<span class="link__title">' . $title . '</span>';
        switch( $style ) {
          case 'text-with-arrow':
            $html .= '<span class="link__icon">' . $this->render_svg([ 'type' => 'icon.arrow' ]) . '</span>';
            break;
        }
      $html .= '</a>';
    }

    return $html;

  }

  // --------------------------- Navigation for WP
  public function render_navigation_wp( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'navigation',
        'current_id' => false,
        'html' => '',
        'menu_title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $menu_items = wp_get_nav_menu_items( $menu_title ) ?: [];

    // ---------------------------------------- Template
    foreach ( $menu_items as $item ) {

      $id = $item->object_id;
      $link = $item->url;
      $title = $item->title;
      $object_type = $item->object;
      $link_type = $item->type;
      $active = ( $id == $current_id ) ? true : false;
      $target = ( 'custom' == $link_type ) ? '_blank' : '_self';
      $rel = ( 'custom' == $link_type ) ? 'noopener' : '';

      $html .= '<div class="' . $block_name . '__item">';
        $html .= $this->render_link([
          'active' => $active,
          'classes' => $block_name . '__link',
          'target' => $target,
          'rel' => $rel,
          'title' => $title,
          'link' => $link,
        ]);
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Pre-connect Scripts
  public function render_preconnect_resources( $resources = [] ) {
    $html = '';
    $relationships = [ 'preconnect', 'dns-prefetch' ];
    if ( !empty( $resources ) && !empty( $relationships ) ) {
      foreach ( $relationships as $rel ) {
        foreach ( $resources as $resource ) {
          $html .= "<link rel='{$rel}' href='{$resource}'>";
        }
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_preload_fonts( $fonts = [] ) {
    $html = '';
    if ( !empty( $fonts ) ) {
      foreach ( $fonts as $font ) {
        $font_src = $this->get_theme_directory('assets') . '/fonts/' . $font . '.woff2';
        $html .= '<link rel="preload" href="' . $font_src . '" as="font" type="font/woff2" crossorigin>';
      }
    }
    return $html;
  }

  // --------------------------- SEO
  public function render_seo( $enable = true ) {
    $html = '<meta name="robots" content="noindex, nofollow">';
    if ( $enable && !is_attachment( $this->get_theme_info('post_ID') ) ) {
			if ( defined( 'WPSEO_VERSION' ) ) {
				$html = '<!-- Yoast Plugin IS ACTIVE! -->';
			} else {
				$html = '<!-- Yoast Plugin IS NOT ACTIVE! -->';
				$html .= '<meta name="description" content="' . get_bloginfo( 'description' ) . '">';
			}
    }
    return $html;
  }

  // --------------------------- SVG
  public function render_svg( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => 'burger'
      ],
      $params
    ));

    // ---------------------------------------- Template
    switch( $type ) {


      case 'brand.logo':
        $html = '<svg x="0px" y="0px" viewBox="0 0 700 195" style="enable-background:new 0 0 700 195;" xml:space="preserve">
          <g>
	          <g>
		          <path d="M617.3,170.7c-2.3,0-7.2,5-8.9,7c-0.2,0.3-0.4,0.3-0.4,0.3c0,0-0.1-0.2,0-0.8c0.5-2.5,1.8-6.6,2.8-10.3
			          c0.8-2.8,1.5-5.2,1.8-6.4c0-0.1,0.1-0.1,0.2-0.2c0.3-0.1,0.4-0.4,0.5-0.6c0.3-1.3,0.1-2.7-0.5-3.8c-0.4-0.5-1-0.8-1.7-0.7
			          c-0.1,0-0.3,0.1-0.3,0.2c-0.4,0.5-0.3,2.4,0,4.1c-0.8,4.2-2.1,9.4-3.2,13.2c-0.5,1.8-0.9,3.3-1.1,4.4c-0.8,1.1-1,2.6-0.5,3.9
			          c0.2,0.4,0.6,0.6,1.1,0.6c1.2-0.1,2.3-0.8,2.9-1.8c0.1-0.2,0.2-0.5,0.2-0.7c0-0.2,0-0.3,0.1-0.3l0.2-0.2c1.5-1.6,3.7-4.1,5.6-5.1
			          c0.4-0.2,0.6-0.2,0.8-0.1s0.2,0.5,0.2,0.8c0,2.2-2.2,7.1-3.8,7.1c-0.5,0-1.1-0.7-1.7-1.3l-0.6-0.6l0,0l0,0
			          c-0.3,0.1-0.5,0.3-0.6,0.6c0.1,0.4,0.3,0.7,0.7,0.9c0.6,1.2,1.8,2.1,3.1,2.2c0.3,0,0.5-0.1,0.7-0.4c0,0,0.1-0.1,0.1-0.1
			          c0,0,0.1,0,0.2,0c0.2,0.1,0.5,0,0.7-0.1c2.1-2.3,3.3-5.1,3.6-8.2c0.2-1.1-0.1-2.1-0.7-3C618.2,170.9,617.8,170.7,617.3,170.7z"/>
		          <path d="M636.8,179c-0.6,0.1-1.3,0.3-1.8,0.6c0.2-3.3-0.3-6.4-1.3-7.4c-0.2-0.2-0.5-0.4-0.8-0.4
			          c-1.5,0-7.6,7.2-7.7,7.2c-0.1,0.1-0.2,0.2-0.4,0.3c0,0-0.1-0.1,0-0.7c0.6-2.5,1.6-4.9,3-7.1c0.2-0.4,0.5-0.8,0.6-1.2
			          c0.1-0.2,0.1-0.5,0-0.7c-0.2-0.3-0.5-0.5-0.9-0.6c-0.4-0.1-1.1-0.1-1.3,0.4c-1.5,3-3.4,7.3-3.7,10.8c0,0.1-0.1,0.2-0.1,0.3
			          c-0.1,0.1-0.1,0.3-0.1,0.5c0,0.5,0.2,1.1,0.6,1.4c0.3,0.3,0.7,0.4,1.1,0.4c1-0.1,2-1.5,3.2-3.2c1.3-2,2.9-3.8,4.7-5.3
			          c0.3-0.2,0.5-0.3,0.6-0.2c0.2,0.1,0.3,0.3,0.3,0.5c0.4,2.2,0.5,4.4,0.1,6.6c-2.9,3.1-5.7,8.8-5,12.1c0.1,0.6,0.5,1.2,1.1,1.5
			          c0.1,0,0.1,0,0.2,0c0.2,0,0.4-0.1,0.5-0.2c1.9-1.8,4-7.6,4.8-12c0.5-0.5,1.2-0.8,1.9-1l0.4,0.2c0.4,0.3,0.7,0.4,1.1,0.3
			          c0.6-0.2,0.9-0.9,0.8-1.5C638.5,179.8,637.7,179,636.8,179z M629.8,191.6l-0.1,0.2c0-0.1,0-0.2,0-0.3c0.2-2.4,1.1-4.7,2.6-6.6
			          C631.8,187.3,630.9,189.5,629.8,191.6z"/>
		          <path d="M665.6,158.3c0-0.4-0.1-0.7-0.2-1c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.2-0.5-0.2-0.8-0.2l0,0
			          c-0.2,0.1-0.4,0.2-0.5,0.3c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.1-0.1,0.3-0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
			          c0,0.1,0,0.3-0.1,0.5l0,0c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4,0,0.6c0,0.1,0,0.2,0,0.3c0,0.1,0.1,0.1,0.2,0.2c0,0.6,0,1.3,0,2.1v0.9
			          c0.1,2.6,0.3,5.4,0.3,8.1c0,2,0,3.8-0.1,5.5c0,0.8-0.1,1.5-0.2,2.2c0,0.3,0,0.6,0,0.9c-0.1,0.6-0.2,1.2-0.4,1.8
			          c-0.2,0.4-0.2,0.4-0.3,0.4l0,0c-0.3,0.1-0.5,0-1.1-0.4c-0.6-0.4-1.2-0.9-1.6-1.6c-0.8-1.1-1.5-2.3-2-3.6c-0.4-0.9-0.9-1.8-1.3-2.8
			          c-0.6-1.4-1.3-3-1.8-4.7c-0.4-1-0.7-2.1-1-3.2l-0.5-1.4c-0.2-0.6-0.4-1.2-0.6-1.8l-0.4-1.1c-0.4-0.9-0.7-1.7-1-2.5
			          c-0.3-0.7-0.5-1.3-0.8-1.6c-0.1-0.2-0.2-0.3-0.3-0.5c-0.1-0.2-0.3-0.3-0.5-0.4h-0.1l0,0c-0.1,0-0.1-0.1-0.2-0.1l0,0
			          c-0.4,0-0.7,0.2-1,0.5c-0.1,0.2-0.2,0.4-0.2,0.7c0,0.2,0,0.6,0,1c0,0.4,0,1.3,0,2.2c0,1.2,0.1,2.6,0.2,4.3v0.7
			          c0.1,2.8,0.3,5.7,0.3,8.1c0,1.5,0.1,2.9,0.1,4.2c0,1.3,0,2.3,0.1,3.2v0.5c0,0.9,0,1.6,0.1,2.2c0,0.5,0.1,1,0.2,1.6v0.1l0,0l0,0
			          c0,0.1,0.1,0.1,0.1,0.2c0.1,0.2,0.4,0.4,0.7,0.4c0.1,0,0.2,0,0.3-0.1l0,0l0,0c0.2-0.1,0.3-0.3,0.3-0.6l0,0l0,0l0,0
			          c0.1-0.5,0.1-1,0.1-1.5c0-0.7,0.1-1.7,0.1-2.8c0-0.9,0-2,0-3.3s0-2.7-0.1-4.2c-0.1-2.4-0.2-5.3-0.4-8.2l-0.1-1.9
			          c0-0.5,0-1.1-0.1-1.6l0,0c0.1,0.3,0.3,0.7,0.4,1.1s0.4,1.1,0.6,1.7l0.4,1.4c0.3,1.1,0.7,2.2,1,3.3c0.6,1.7,1.3,3.3,1.9,4.8
			          c0.5,1,0.9,2,1.4,2.9c0.7,1.3,1.4,2.6,2.3,3.8c0.6,0.8,1.3,1.5,2.1,2c0.8,0.6,1.8,0.8,2.7,0.6c0.8-0.3,1.4-0.8,1.7-1.6
			          c0.3-0.7,0.4-1.5,0.5-2.3c0.1-0.3,0.2-0.6,0.3-1c0.1-0.7,0.2-1.5,0.2-2.3c0.1-1.7,0.2-3.6,0.2-5.7c0-2.8-0.2-5.8-0.3-8.1
			          C665.9,160.8,665.7,159.2,665.6,158.3z"/>
		          <path d="M696.9,182c0-0.2,0-0.3-0.1-0.4c-0.1-0.2-0.3-0.4-0.5-0.4l0,0l-0.1-0.1l0,0c0-0.2-0.1-0.4-0.2-0.6l0,0l0,0
			          c-0.1-0.1-0.1-0.2-0.2-0.4c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.5-0.3-1-0.3-1.6c-0.1-0.3-0.1-0.6-0.2-1c-0.1-0.4-0.2-1-0.3-1.5
			          c-0.2-0.8-0.3-1.6-0.4-2.6c-0.3-2.2-0.5-5-0.5-7.7c-0.1-2.2-0.1-4.3,0-5.7c0-0.1,0-0.2,0-0.2c0-0.5,0-1,0-1.3v-0.5
			          c0-0.1,0-0.2,0-0.3l0,0c-0.1-0.2-0.2-0.4-0.4-0.5c-0.2-0.1-0.5-0.1-0.7-0.1c-0.2,0.1-0.4,0.2-0.5,0.4c0,0-0.1,0.1-0.1,0.2l0,0v0.2
			          c0,0.2,0,0.5-0.1,0.9c-0.3,2.3-0.4,4.7-0.4,7l0,0c-0.3,1.4-0.8,2.7-1.4,4c-0.2,0.5-0.4,0.9-0.6,1.3c-0.8,1.4-1.7,2.7-2.9,3.8
			          c-0.9,0.9-2,1.7-3.1,2.2c-0.7,0.4-1.6,0.4-2.3-0.1c-0.6-0.4-1.1-0.8-1.4-1.4c-0.9-1.7-1.5-3.4-1.8-5.3c-0.3-1.4-0.5-3-0.7-4.4
			          c-0.2-1.1-0.3-2.1-0.5-3c-0.3-1.9-0.5-3-0.6-3.7c0-0.2-0.1-0.5-0.2-0.7c-0.1-0.2-0.2-0.3-0.3-0.4c-0.3-0.4-0.8-0.5-1.3-0.3l0,0
			          l-0.3,0.1c-0.2,0.1-0.3,0.3-0.4,0.4c-0.1,0.2-0.2,0.4-0.3,0.6c-0.1,0.5-0.3,0.9-0.3,1.4c-0.3,1.4-0.6,3.4-0.8,5.5
			          c0,0.9-0.1,1.8-0.2,2.7c-0.1,1.6-0.2,3.2-0.3,4.5c-0.1,1.6-0.1,3.1-0.2,4.5v1.5c0,1.5-0.1,3-0.2,4.1c0,0.5,0,1,0,1.3
			          c0,0.3,0,0.7,0,0.9l0,0l0,0c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0.1,0,0.1c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.5,0.2
			          c0.2,0,0.4-0.1,0.5-0.2l0,0c0.1-0.1,0.2-0.2,0.3-0.3c0.4-0.8,0.6-1.7,0.7-2.6c0.1-1.1,0.3-2.6,0.4-4.2c0-0.7,0.1-1.4,0.1-2.1
			          c0.1-1.2,0.1-2.4,0.2-3.8c0.1-1.4,0.2-2.9,0.3-4.5c0.1-0.9,0.1-1.8,0.2-2.7c0-0.4,0.1-0.9,0.1-1.3c0.1,0.4,0.2,0.9,0.2,1.4v0.1
			          c0.2,1.4,0.5,3,0.8,4.5c0.4,2.1,1.1,4,2.1,5.9c0.5,0.8,1.2,1.5,2,2c1.2,0.7,2.7,0.7,4,0.1c1.4-0.6,2.6-1.5,3.6-2.6
			          c1.2-1.3,2.2-2.7,3.1-4.3c0.2-0.4,0.4-0.9,0.7-1.4c0-0.1,0.1-0.2,0.1-0.3c0,1.3,0.2,2.5,0.3,3.5c0.1,1.1,0.2,1.9,0.3,2.7
			          c0.1,0.5,0.2,1,0.3,1.5c0.1,0.5,0.1,0.7,0.2,1.1c0.2,0.6,0.4,1.2,0.6,1.8c0.1,0.3,0.2,0.5,0.3,0.8c0.1,0.2,0.2,0.5,0.4,0.7
			          c0.1,0.2,0.3,0.4,0.5,0.4c0.2,0.2,0.5,0.3,0.7,0.4l0.2,0.1l0.4,0.1l0,0c0.2,0,0.4,0,0.6-0.1c0.2-0.1,0.4-0.3,0.4-0.5
			          c0-0.1,0-0.1,0-0.2L696.9,182z"/>
	          </g>
	          <g>
		          <path d="M45.9,99.4h0.4L60.8,1.6h30.1v137.7H70.5V40.5h-0.4l-14.6,98.8H35L19.3,41.9h-0.4v97.4H0V1.6h30.1L45.9,99.4z
			          "/>
		          <path d="M110.8,34.6c0-22,11.6-34.6,32.9-34.6s32.9,12.6,32.9,34.6v71.6c0,22-11.6,34.6-32.9,34.6
			          s-32.9-12.6-32.9-34.6V34.6z M132.5,107.6c0,9.8,4.3,13.6,11.2,13.6c6.9,0,11.2-3.7,11.2-13.6V33.2c0-9.8-4.3-13.6-11.2-13.6
			          c-6.9,0-11.2,3.7-11.2,13.6V107.6z"/>
		          <path d="M228.6,113.9h0.4L245.3,1.6h19.9L244,139.3h-32.3L190.4,1.6h21.8L228.6,113.9z"/>
		          <path d="M302.6,59.6h29.7v19.7h-29.7v40.3H340v19.7h-59V1.6h59v19.7h-37.4V59.6z"/>
		          <path d="M404.7,99.4h0.4l14.6-97.8h30.1v137.7h-20.4V40.5h-0.4l-14.6,98.8h-20.4l-15.8-97.4h-0.4v97.4h-18.9V1.6H389
			          L404.7,99.4z"/>
		          <path d="M493,59.6h29.7v19.7H493v40.3h37.4v19.7h-59V1.6h59v19.7H493V59.6z"/>
		          <path d="M569.2,39.5h-0.4v99.7h-19.5V1.6h27.1L598.3,84h0.4V1.6H618v137.7h-22.2L569.2,39.5z"/>
		          <path d="M633.1,1.6H700v19.7h-22.6v118h-21.6v-118h-22.6V1.6z"/>
	          </g>
          </g>
          </svg>';
          break;

      case 'brand.monogram':
        $html = '<svg x="0px" y="0px" viewBox="0 0 100 144.9" style="enable-background:new 0 0 100 144.9;" xml:space="preserve">
        <g>
	        <path d="M65.9,21.3c2.3-0.9,4.2-3.6,5.1-5.7c1-2,1.5-4.8,1-7.9c-0.3-2.5-1.6-6.3-5.2-7.4c-4.8-1.5-8.4,2.2-8.4,2.2
		        c-2.6,2.5-4.9,5.5-5.4,8.6c-0.3,2.3,0.1,4.4,1.2,6.1C55.8,19.7,61.5,22.9,65.9,21.3z"/>
	        <path d="M98.9,38.5c-3.3-8.9-11-7.6-11-7.6s-28,1.7-48,1.3c-17.4-0.4-22.1,0.3-29.6,0.1c-5.2,0-7.6,0.4-7.6,0.4s-3.9,0.7-2.5,3.9
		        c1,2,6.2,11,13.7,11.1c6.1,0.1,10.2-1.2,16-1.6c9.4-0.6,15.9-0.4,15.9-0.4s2.5,0,6.3,0.1c1.2,0.6,1,1.7,1,1.7l0.4,10.7
		        c0,1.6-2,1.4-2,1.4s-21.2,0.9-24.5,1c-5.2,0.3-9.8,2.3-11.7,7.2c-1.7,4.2-0.1,9.7,1.9,13.1c3.7,6.4,6.8,7.8,10.8,16
		        c5.5,11.4,0.9,22.1-1.2,27.1c-1.4,3.5,1.4,4.2,3.2,3.8c1.9-0.4,4.3-2.2,6.6-4.8c4.8-5.8,6.9-14.6,7.2-17.2c0.6-6.5,0-11.7-2.7-17.3
		        c-3-6.4-6.5-7.9-5.5-11.5c1.3-4.3,18,0.9,18,0.9c0.3,0.1,0.5,0.2,0.7,0.4l1.6,40.8c0,0,1,25.7,6.3,25.7c5.5,0,9.1-12.4,9.1-27.6
		        c0-20.9,0.7-69.6,0.7-69.6s0-0.9,0.4-1.4c9.5,0.1,18.9,0.3,22.2,0.3C100.1,46.6,101.1,44.4,98.9,38.5z"/>
        </g>
        </svg>';
        break;

      case 'brand.monogram-wide':
        $html = '<svg x="0px" y="0px" viewBox="0 0 580.3 243" style="enable-background:new 0 0 580.3 243;" xml:space="preserve">
          <g>
	          <g opacity="0.55">
		          <path d="M166.6,63.9C161,48.8,148.2,51,148.2,51s-47.1,2.9-80.9,2.2c-29.4-0.7-37.1,0.5-49.8,0.2
			          c-8.7,0-12.9,0.7-12.9,0.7s-6.6,1.2-4.1,6.6C2.2,64.1,11,79.2,23.6,79.4c10.2,0.2,17.2-1.9,27-2.7c15.8-1,26.7-0.7,26.7-0.7
			          s4.1,0,10.7,0.2c1.9,1,1.7,2.9,1.7,2.9l0.7,18c0,2.7-3.4,2.4-3.4,2.4s-35.7,1.5-41.3,1.7c-8.7,0.5-16.5,3.9-19.7,12.2
			          c-2.9,7-0.2,16.3,3.2,22.1c6.3,10.7,11.4,13.1,18.2,27c9.2,19.2,1.5,37.2-1.9,45.7c-2.4,5.8,2.4,7,5.3,6.3c3.2-0.7,7.3-3.6,11.2-8
			          c8-9.7,11.7-24.6,12.1-28.9c1-10.9,0-19.7-4.6-29.2c-5.1-10.7-10.9-13.4-9.2-19.4c2.2-7.3,30.4,1.5,30.4,1.5
			          c0.5,0.1,0.9,0.4,1.1,0.6l2.8,68.6c0,0,1.7,43.3,10.7,43.3c9.2,0,15.3-20.9,15.3-46.4c0-35.2,1.2-117.2,1.2-117.2s0-1.5,0.7-2.4
			          c16,0.2,31.8,0.5,37.4,0.5C168.6,77.5,170.3,73.8,166.6,63.9z"/>
		          <path d="M111.1,34.3c3.7-1.4,6.8-5.9,8.2-9.1c1.6-3.3,2.3-7.7,1.6-12.7c-0.5-4-2.6-10.1-8.4-12
			          c-7.7-2.3-13.6,3.5-13.6,3.5c-4.2,4-8,8.9-8.7,13.8c-0.5,3.7,0.2,7,1.9,9.8C94.9,31.7,104.1,36.9,111.1,34.3z"/>
	          </g>
	          <g>
		          <polygon points="325.7,50.9 325.7,185.4 233.7,50.9 201.1,50.9 201.1,243 223.8,243 233.7,243 233.7,108.8 325.7,243 358.4,243
			          358.4,50.9 335.4,50.9 "/>
		          <polygon points="547.4,50.9 485.7,193.4 424,50.9 391,50.9 391,243 423.7,243 423.7,128.7 472.8,243 498.5,243 547.4,128.7
			          547.4,243 580.3,243 580.3,50.9 "/>
	          </g>
          </g>
          </svg>';
        break;

      case 'icon.arrow':
        $html = '<svg x="0px" y="0px" viewBox="0 0 11.6 21.4" style="enable-background:new 0 0 11.6 21.4;" xml:space="preserve">
          <path d="M1,21.4c-0.3,0-0.5-0.1-0.7-0.3c-0.4-0.4-0.4-1,0-1.4l8.9-9l-8.9-9c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l9.6,9.7
	        c0.4,0.4,0.4,1,0,1.4l-9.6,9.7C1.5,21.3,1.3,21.4,1,21.4z"/>
        </svg>';
        break;

      case 'icon.quotemark':
        $html = '<svg width="202" height="156" viewBox="0 0 202 156">
          <path d="M85.9254 0C35.4254 0 0 39.942 0 94.9565C0 134.145 15.8284 156 44.4701 156C67.8358 156 82.9104 142.435 82.9104 119.826C82.9104 97.2174 68.5896 85.1594 46.7313 85.1594C30.1493 85.1594 19.597 91.942 10.5522 106.261C6.02985 54.2609 39.194 9.79711 85.9254 9.79711V0ZM202 0C151.5 0 116.075 39.942 116.075 94.9565C116.075 134.145 131.903 156 160.545 156C183.91 156 198.985 142.435 198.985 119.826C198.985 97.2174 184.664 85.1594 162.806 85.1594C146.224 85.1594 135.672 91.942 126.627 106.261C122.104 54.2609 155.269 9.79711 202 9.79711V0Z"/>
        </svg>';
        break;

      case 'icon.email':
        $html = '<svg x="0px" y="0px" viewBox="0 0 25 20" style="enable-background:new 0 0 25 20;" xml:space="preserve">
          <path d="M21.8,0H3.2C1.5,0,0,1.4,0,3.2v13.6C0,18.6,1.5,20,3.2,20h18.5c1.8,0,3.2-1.4,3.2-3.2V3.2C25,1.4,23.5,0,21.8,0z M3.2,1.8
	        h18.5c0.6,0,1.1,0.4,1.3,1L12.5,10L1.9,2.8C2.1,2.2,2.6,1.8,3.2,1.8z M21.8,18.2H3.2c-0.8,0-1.4-0.6-1.4-1.4V4.9l10.1,7
	        c0.2,0.1,0.3,0.2,0.5,0.2s0.4-0.1,0.5-0.2l10.1-7v11.9C23.2,17.6,22.5,18.2,21.8,18.2z"/>
        </svg>';
        break;

      case 'icon.facebook':
        $html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 8.6 16" style="enable-background:new 0 0 8.6 16;" xml:space="preserve">
          <path d="M8.6,0H6.3c-1,0-2,0.4-2.8,1.2C2.8,1.9,2.3,2.9,2.3,4v2.4H0v3.2h2.3V16h3.1V9.6h2.3l0.8-3.2H5.5V4c0-0.2,0.1-0.4,0.2-0.6C5.8,3.3,6,3.2,6.3,3.2h2.3V0z"/>
        </svg>';
        break;

      case 'icon.houzz':
        $html = '<svg width="95" height="94" viewBox="0 0 95 94" xmlns="http://www.w3.org/2000/svg">
          <path d="M85.2111 0H10.0137C7.52074 0 5.12989 0.990347 3.36711 2.75319C1.60433 4.51603 0.614014 6.90697 0.614014 9.4V84.6C0.614014 87.093 1.60433 89.484 3.36711 91.2468C5.12989 93.0096 7.52074 94 10.0137 94H85.2111C87.7041 94 90.0949 93.0096 91.8577 91.2468C93.6205 89.484 94.6108 87.093 94.6108 84.6V9.4C94.6108 6.90697 93.6205 4.51603 91.8577 2.75319C90.0949 0.990347 87.7041 0 85.2111 0ZM74.9185 77.3385H54.7091V57.105H40.6096V77.3385H20.3768V16.6615H34.4764V30.7615L74.9185 42.4175V77.3385Z"/>
        </svg>';
        break;

      case 'icon.instagram':
        $html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
          <g>
            <path d="M8,3.6c-0.9,0-1.7,0.3-2.4,0.7C4.8,4.8,4.3,5.5,3.9,6.3C3.6,7.1,3.5,7.9,3.7,8.8c0.2,0.8,0.6,1.6,1.2,2.2
	            c0.6,0.6,1.4,1,2.3,1.2c0.9,0.2,1.7,0.1,2.5-0.2c0.8-0.3,1.5-0.9,2-1.6c0.5-0.7,0.7-1.5,0.7-2.4c0-1.1-0.5-2.2-1.3-3
	            C10.3,4.1,9.2,3.6,8,3.6z M10,9.9c-0.5,0.5-1.3,0.8-2,0.8c-0.6,0-1.1-0.2-1.6-0.5C5.9,10,5.5,9.5,5.3,9S5.1,7.9,5.2,7.4
	            c0.1-0.5,0.4-1,0.8-1.4c0.4-0.4,0.9-0.7,1.5-0.8s1.1-0.1,1.7,0.2c0.5,0.2,1,0.6,1.3,1c0.3,0.5,0.5,1,0.5,1.6
	            C10.9,8.7,10.6,9.4,10,9.9z"/>
            <path d="M12.5,2.6c-0.6,0-1,0.5-1,1c0,0.6,0.5,1,1,1c0.6,0,1-0.5,1-1C13.5,3,13.1,2.6,12.5,2.6z"/>
            <path d="M11.3,0H4.7C2.1,0,0,2,0,4.5v6.9C0,13.9,2.1,16,4.7,16h6.7c2.6,0,4.6-2,4.6-4.5V4.5C16,2,13.9,0,11.3,0z
	             M14.7,11.5c0,0.9-0.4,1.7-1,2.3c-0.6,0.6-1.5,1-2.4,1H4.6c-0.9,0-1.8-0.4-2.4-1c-0.6-0.6-1-1.5-1-2.3V4.5c0-0.9,0.4-1.7,1-2.3
	            s1.5-1,2.4-1h6.7c0.9,0,1.8,0.4,2.4,1c0.6,0.6,1,1.5,1,2.3V11.5z"/>
          </g>
        </svg>';
        break;

      case 'icon.linkedin':
        $html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
          <path d="M18,0H2C1.8,0,1.5,0.1,1.2,0.2C1,0.3,0.8,0.4,0.6,0.6C0.4,0.8,0.3,1,0.2,1.2C0.1,1.5,0,1.7,0,2v16c0,0.3,0.1,0.5,0.2,0.8
	        c0.1,0.2,0.2,0.5,0.4,0.6c0.2,0.2,0.4,0.3,0.7,0.4C1.5,19.9,1.8,20,2,20h16c0.3,0,0.5-0.1,0.8-0.2c0.2-0.1,0.5-0.2,0.7-0.4
	        c0.2-0.2,0.3-0.4,0.4-0.6c0.1-0.2,0.2-0.5,0.2-0.8V2c0-0.5-0.2-1-0.6-1.4C19,0.2,18.5,0,18,0z M6.4,15.8H3.7V7.3h2.7V15.8z M5.1,6.2
	        c-0.9,0-1.4-0.6-1.4-1.3c0-0.8,0.6-1.3,1.4-1.3c0.9,0,1.4,0.6,1.4,1.3C6.5,5.6,6,6.2,5.1,6.2z M16.2,15.8h-2.7v-4.7
	        c0-1.1-0.4-1.9-1.4-1.9c-0.7,0-1.2,0.5-1.4,1c-0.1,0.2-0.1,0.4-0.1,0.7v4.9H8V10C8,8.9,8,8,8,7.3h2.3l0.1,1.2h0.1
	        c0.3-0.6,1.2-1.4,2.7-1.4c1.8,0,3.1,1.2,3.1,3.7L16.2,15.8z"/>
        </svg>';
        break;

      case 'icon.pinterest':
        $html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
          <g>
	          <path d="M8,0C6.2,0,4.3,0.7,2.9,1.8C1.5,3,0.5,4.7,0.2,6.5c-0.3,1.8,0,3.7,0.9,5.4c0.9,1.6,2.3,2.9,4.1,3.6c-0.1-0.6-0.1-1.6,0-2.3
		          c0.1-0.6,0.9-4,0.9-4C5.9,8.8,5.8,8.4,5.8,8C5.8,6.9,6.5,6,7.3,6c0.7,0,1,0.5,1,1.1c0,0.7-0.4,1.7-0.7,2.7
		          c-0.2,0.8,0.4,1.4,1.2,1.4c1.4,0,2.5-1.5,2.5-3.7c0-1.9-1.4-3.2-3.3-3.2c-2.3,0-3.6,1.7-3.6,3.5c0,0.7,0.3,1.4,0.6,1.8
		          c0,0,0,0.1,0.1,0.1c0,0,0,0.1,0,0.1c-0.1,0.3-0.2,0.8-0.2,0.9c0,0.1-0.1,0.2-0.3,0.1c-1-0.5-1.6-1.9-1.6-3.1C2.9,5.3,4.7,3,8.2,3
		          c2.8,0,4.9,2,4.9,4.6c0,2.8-1.7,5-4.1,5c-0.8,0-1.6-0.4-1.8-0.9c0,0-0.4,1.5-0.5,1.9c-0.2,0.7-0.7,1.6-1,2.1
		          C6.7,16,7.9,16.1,9,15.9c1.1-0.1,2.2-0.5,3.2-1.2s1.8-1.4,2.4-2.4c0.6-1,1-2,1.2-3.2C16.1,8,16,6.9,15.7,5.8
		          c-0.3-1.1-0.9-2.1-1.6-3c-0.7-0.9-1.7-1.6-2.7-2.1C10.3,0.3,9.2,0,8,0z"/>
          </g>
        </svg>';
        break;

      case 'icon.twitter':
        $html = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.7 16" style="enable-background:new 0 0 19.7 16;" xml:space="preserve">
          <path d="M19.7,0c-0.9,0.6-1.8,1.1-2.8,1.4c-0.5-0.6-1.3-1.1-2-1.3C14-0.1,13.2,0,12.4,0.3c-0.8,0.3-1.4,0.8-1.9,1.5C10.1,2.4,9.8,3.2,9.8,4v0.9c-1.6,0-3.1-0.3-4.5-1c-1.4-0.7-2.6-1.7-3.5-3c0,0-3.6,8,4.5,11.6c-1.8,1.2-4,1.9-6.3,1.8c8,4.5,17.9,0,17.9-10.2c0-0.2,0-0.5-0.1-0.7C18.7,2.4,19.4,1.2,19.7,0z"/>
          </svg>';
        break;

    }

    return $html;

  }

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

}
