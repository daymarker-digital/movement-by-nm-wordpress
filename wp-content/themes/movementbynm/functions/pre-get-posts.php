<?php

/*
//////////////////////////////////////////////////////////
////  Query String Filter
//////////////////////////////////////////////////////////
*/

function projects_pre_get_posts( $query ) {

  $order = 'DESC';

  // bail early if is in admin
  // OR if not main query
	if ( !is_admin() && $query->is_main_query() ) {

    if ( $query->is_category() || $query->is_tax() ) {
      $query->set( 'order', $order );
      $query->set( 'orderby', 'date' );
      $query->set( 'posts_per_page', 7);
    }

  }

}

add_action( 'pre_get_posts', 'projects_pre_get_posts' );
