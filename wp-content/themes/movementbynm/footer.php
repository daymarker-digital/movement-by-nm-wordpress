<?php

/*
*
*	Filename: footer.php
*
*/

echo '</main>';

// ---------------------------------------- Header
include( locate_template( './snippets/footer.php' ) );
include( locate_template( './snippets/json-ld.php' ) );

wp_footer();

echo '</body>';
echo '</html>';

?>
