<?php

/*
*
*	Filename: single.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

if ( have_posts() ) {

  echo '<div class="article">';

  	while ( have_posts() ) {

  		// init post data
  		the_post();

      echo '<div class="article__header">';

        // default post data
        $magazine_brand = $featured_caption = $featured_portrait = false;

        // get data
        if ( have_rows( 'magazine_header', 'options' ) ) {
          while( have_rows( 'magazine_header', 'options' ) ) {
            the_row();
            $magazine_brand = get_sub_field( 'brand' ) ?: [];
          }
        }

        $categories = get_the_category() ?: [];
        $featured = $Templates->get_featured_image_by_post_id( $theme_post_id ) ?: [];
        $title = get_the_title() ?: '';

        if ( have_rows( 'featured' ) ) {
          while( have_rows( 'featured' ) ) {
            the_row();

            $title = get_sub_field( 'title' ) ?: $title;
            $featured_portrait = get_sub_field( 'image' ) ?: [];
            $featured_caption = get_sub_field( 'caption' ) ?: '';

          }
        }

        // print data
        echo !empty($magazine_brand) ? '<div class="article__brand">' . $Templates->render_lazyload_image( [ 'image' => $magazine_brand ] ) . '</div>' : '';

        if ( !empty( $categories ) ) {

          $cat_id = $categories[0]->term_id;
          $cat_name = $categories[0]->name;
          $cat_link = get_category_link( $cat_id );

          echo '<div class="article__category category">';
            echo '<a href="' . $cat_link . '">' . $cat_name . '</a>';
          echo '</div>';

        }


        echo $title ? '<h1 class="article__title">' . $title . '</h1>' : '';

        include( locate_template( './snippets/content--article-meta.php' ) );

        echo '<div class="article__featured-images">';
          if ( !empty($featured) ) {
            echo '<div class="article__featured-image article__featured-image--landscape d-none d-lg-block">';
              echo $Templates->render_lazyload_image( [ 'image' => $featured ] );
            echo '</div>';
          }
          if ( !empty($featured_portrait) ) {
            echo '<div class="article__featured-image article__featured-image--portrait d-lg-none">';
              echo $Templates->render_lazyload_image( [ 'image' => $featured_portrait ] );
            echo '</div>';
          }
          if ( $featured_caption ) {
            echo '<div class="article__caption caption">';
              echo $featured_caption;
            echo '</div>';
          }
        echo '</div>';

      echo '</div>';

      echo '<div class="article__body">';
        include( locate_template( './snippets/content--article-flexible-content.php' ) );
      echo '</div>';

      echo '<div class="article__footer">';
        include( locate_template( './snippets/content--article-share-posts.php' ) );
        include( locate_template( './snippets/content--article-adjacent-posts.php' ) );
        include( locate_template( './snippets/content--article-additional-posts.php' ) );
      echo '</div>';

  	}

  echo '</div>';

}

get_footer();

?>
