# Movement by NM | WordPress

## Site URL
https://movementbynm.com/\
https://movementbyndev.wpengine.com/\

## Deployment Instructions
1.  Copy Production Environment to Development in WP Engine
2.  A nested numbered list

## General Todo
- [ ] Padding check with live content on production
- [ ] Colour option for the text block link
- [ ] Remove header scripts plugin
- [ ] Update Bitbucket repo
- [ ] Push changes to development
- [ ] Push changes to production
