//////////////////////////////////////////////////////////
////  Team Members
//////////////////////////////////////////////////////////

const TeamMembers = (() => {

	let debugThis = false;
	let info = { name : 'TeamMembers', version : '2.0' };

	//////////////////////////////////////////////////////////
	//// Show Team Member Details
	//////////////////////////////////////////////////////////

	const showTeamMemberDetails = () => {

    $('.team__grid-item').on('click', function(event){

      $('.team__grid-item').removeClass('active');
      let details = $(this).find('.team-member__details').html();
      $(this).nextAll(".team__grid-item--bio:first").empty().append(details);
      $(this).addClass('active');

    });

	};

	//////////////////////////////////////////////////////////
	//// Show Team Member Groups
	//////////////////////////////////////////////////////////

	const showTeamMemberGroups = () => {

    let targets = '.team__nav-item, .team__group-info-item, .team__group';

    $('.team__nav-item').on('click', function(event){

      // get target group ID
      let targetGroupID = $(this).attr('data-target') || false;

      // get current index
      let currentIndex = $(this).index();

      // reset active state
      $( targets ).removeClass('active');

      // if group ID and target exist
      if ( targetGroupID && $(targetGroupID).length ) {
        $(targetGroupID).addClass('active');
      }

      $('.team__group-info-item').eq(currentIndex).addClass('active');

      $(this).addClass('active');

    });

	};

	//////////////////////////////////////////////////////////
	////  Initialize
	//////////////////////////////////////////////////////////

	const init = () => {
    showTeamMemberGroups();
    showTeamMemberDetails();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
		init : init
	};

})();
