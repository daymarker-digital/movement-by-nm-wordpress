//////////////////////////////////////////////////////////
////  Modals
//////////////////////////////////////////////////////////

const Modals = (() => {

  let DEBUG = false;
  let INFO = { name : 'Modals', version : '2.5' };

  //////////////////////////////////////////////////////////
  ////  Newsletter
  //////////////////////////////////////////////////////////

  const newsletter = {

    cookie: {
      name: "nm--newsletter-popup",
      value: "seen",
      expires: 180
    },

    target: "modal--newsletter-popup",

    setCookie: function() {
      Browser.setCookie( this.cookie.name, this.cookie.value, this.cookie.expires );
    },

    show: function( $delay ) {

      let target = this.target;
      let onClose = this.setCookie();

      setTimeout(function(){
        MicroModal.show( target, {
          awaitCloseAnimation: true,
          onShow: function(modal) {
            // console.log("micromodal opened");
          },
          onClose: function(modal) {
            // console.log("micromodal closed");
            onClose;
          }
        });
      }, $delay );

    },

    init: function() {

      let cookieNotSeen = Browser.getCookie( this.cookie.name ) !== "seen";
      let modalExists = $("#" + this.target).length;

      let daysSaved = parseInt( $("#" + this.target).attr('data-days-saved') ) || 180;
      let timeout = parseInt( $("#" + this.target).attr('data-timeout') ) || 1500;

      // console.log( 'this.cookie', this.cookie );

      this.cookie.expires = daysSaved;

      // console.log( 'this.cookie', this.cookie );

      // cookieNotSeen = true;

      if ( modalExists && cookieNotSeen ) {
        this.show( timeout );
      }

      $("#" + this.target + " [data-micromodal-close]").on("click", function(){
        MicroModal.close( this.target );
      });

      $('.modal__overlay').on('click', function(event){
        console.log( event );
      });

    },

  };

  //////////////////////////////////////////////////////////
  ////  Share
  //////////////////////////////////////////////////////////

  const notifyMe = {

    target: 'modal--notify-me',

    init: function() {

      let modalExists = $("#" + this.target).length;
      let target = this.target;

      if ( modalExists ) {

        $(".js-trigger--notify-me").on("click", function(){

          MicroModal.show( target, {
            disableFocus: true,
            awaitCloseAnimation: true,
            onShow: function(modal) {
              console.log("micromodal opened");
            },
            onClose: function(modal) {
              console.log("micromodal closed");
            }
          });

        });

        $("#" + this.target + " [data-micromodal-close]").on("click", function(){
          MicroModal.close( target );
        });

      }

    },

  };

  //////////////////////////////////////////////////////////
  ////  Share
  //////////////////////////////////////////////////////////

  const share = {

    target: 'modal--share',

    init: function() {

      let modalExists = $("#" + this.target).length;
      let target = this.target;

      if ( modalExists ) {

        $(".js-trigger--share").on("click", function(){

          MicroModal.show( target, {
            disableFocus: true,
            awaitCloseAnimation: true,
            onShow: function(modal) {
              console.log("micromodal opened");
            },
            onClose: function(modal) {
              console.log("micromodal closed");
            }
          });

        });

        $("#" + this.target + " [data-micromodal-close]").on("click", function(){
          MicroModal.close( target );
        });

      }

    },

  };

  //////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {
    newsletter.init();
  };

  //////////////////////////////////////////////////////////
  //// Init
  //////////////////////////////////////////////////////////

  const init = () => {
    main();
  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    init : init
  };

});
