//////////////////////////////////////////////////////////
////  Sizing
//////////////////////////////////////////////////////////

const Sizing = (() => {

  let debug = true;
  let info = { name : 'Sizing', version : '1.0' };
  let breakpoints = {
    'sm' : 567,
    'md' : 768,
    'lg' : 992,
    'xl' : 1200,
    'xxl' : 1400
  };
  let sizes = {
    'full-height': {
      'ratio': 1
    },
    'two-thirds-height': {
      'ratio': 0.6666
    },
    'half-height': {
      'ratio': 0.5
    }
  };

  //////////////////////////////////////////////////////////
  ////  Resize Element | LEGACY
  //////////////////////////////////////////////////////////

  const resizeElementHeight = () => {

    setHeight();

    Browser.onResize ( setHeight, 150 );

    function setHeight() {

      let viewportHeight = window.innerHeight;
      let headerHeight = $('#header').outerHeight();

      for ( const key in sizes ) {

        let element =  $('.background[data-appearance="' + key + '"]');

        let height = viewportHeight * sizes[key].ratio;

        if ( element.hasClass('hero__background') ) {
          height = height - headerHeight;
        }

        element.css({
          'height': height + 'px'
        });

      }

    }

  };

  //////////////////////////////////////////////////////////
  ////  Resize Element
  //////////////////////////////////////////////////////////

  const abc = () => {

    setHeight();

    Browser.onResize ( setHeight, 150 );

    function setHeight() {

      $('.background[data-enable-resize="true"]').each(function( index, element ) {

        let viewportHeight = window.innerHeight;
        let viewportWidth = window.innerWidth;
        let headerHeight = $('#header').outerHeight() || false;
        let appearance = $(element).attr('data-appearance') || 1;
        let resizeMobile = $(element).attr('data-resize-mobile') === 'true' || false;
        let resizeDesktop = $(element).attr('data-resize-desktop') === 'true' || false;
        let offsetHeader = $(element).attr('data-resize-offset-header') === 'true' || false;
        let height = viewportHeight * sizes[appearance].ratio;

        if ( offsetHeader ) {
          height -= headerHeight;
        }

        height += 'px';

        if ( viewportWidth >= breakpoints.lg ) {
          if ( !resizeDesktop ) {
            height = '';
          }
        } else {
          if ( !resizeMobile ) {
            height = '';
          }
        }

        element.style.height = height

      });

    }

  };

  //////////////////////////////////////////////////////////
  ////  Public Method | Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    abc();
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info,
    init
  };

})();
