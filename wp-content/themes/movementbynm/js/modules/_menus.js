//////////////////////////////////////////////////////////
////  Menus
//////////////////////////////////////////////////////////

const Menus = (() => {

  let DEBUG = true;
  let INFO = { name : 'Menus', version : '1.0' };
  let BREAKPOINTS = {
    'sm' : 567,
    'md' : 768,
    'lg' : 992,
    'xl' : 1200,
    'xxl' : 1400
  };

  //////////////////////////////////////////////////////////
  ////  Hover
  //////////////////////////////////////////////////////////

  const hover = () => {

    /*
    $('.navigation-item--main').hover(function () {
      $(this).find('.navigation--sub').stop(true,true).fadeIn(350);
    }, function () {
        $(this).find('.navigation--sub').stop(true,true).delay(175).fadeOut(175);
    });
    */

    $('.header__menu a').hover(function () {
      $(this).closest('.menu__item').addClass('hover');
    }, function () {
      $(this).closest('.menu__item').removeClass('hover');
    });

  };

  //////////////////////////////////////////////////////////
  ////  Push Menu
  //////////////////////////////////////////////////////////

  const pushMenuRight = () => {

    let targetElements = '#footer, main[role="main"], .push-navigation[data-position="right"]';

    $( '.js--toggle-push-navigation-right' ).on( 'click touch touchmove', function(event) {

      event.stopPropagation();

      $( targetElements ).toggleClass( 'pushed-left' );

      $( '#footer, main[role="main"]' ).on('click touchstart', function(event) {
        $( targetElements ).removeClass( 'pushed-left' );
      });

    });

  };

  //////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {

    let triggerEl = '.js--mobile-menu-trigger';
    let mobileMenuEl = '#mobile-menu';

    $( triggerEl ).on( 'click', function(event) {

      let action = $(this).attr('data-trigger-action') || false;
      if ( action && $(mobileMenuEl).length ) {

        switch ( action ) {
          case 'close':
            $(mobileMenuEl).removeClass('active');
            break;
          case 'open':
            $(mobileMenuEl).addClass('active');
            break;
        }

      }

    });	// on click

  };

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    hover();
    main();
    pushMenuRight();
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info : INFO,
    init : init
  };

})();
