//////////////////////////////////////////////////////////
////  Forms
//////////////////////////////////////////////////////////

var Forms = (function () {

  let DEBUG = false;
  let INFO = { name : 'Forms', version : '2.0' };
  let FIELDS = {};

  //////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {

    $('.js--validate-me').on('click', '[type="submit"]', function(event){

      event.preventDefault();

      let thisform = $(this).closest('form');
      let isValid = isFormValid( thisform );

      if ( isValid && !DEBUG ) {
        submitForm( thisform );
      }

      if ( DEBUG ) {
        console.log( 'Fields', FIELDS );
      }

    });

  };

  //////////////////////////////////////////////////////////
  ////  Is Field Valid
  //////////////////////////////////////////////////////////

  const isFieldValid = ( $field, $value, $type ) => {

    let isValid = false;
    let name = $field.attr('name') || 'no-name';
    let type = $field.attr('type') || 'no-type';
    let value = $field.val() || 'no-value';

    switch ( $type ) {
      case 'INPUT':
        switch ( type ) {
          case 'email':
            if ( validator.isEmail( $value ) ) {
              isValid = true;
            }
            break;
          case 'radio':
            console.log( 'radio' );
            console.log( $field );
            break;
          case 'tel':
            if ( validator.isMobilePhone( $value ) ) {
              isValid = true;
            }
            break;
          case 'text':
            if ( ! validator.isEmpty( $value ) ) {
              isValid = true;
            }
            break;
        }
        break;
      case 'TEXTAREA':
        if ( ! validator.isEmpty( $value ) ) {
          isValid = true;
        }
        break;
    }

    FIELDS[ name ] = {
      valid: isValid,
      type: type,
      value: value
    };

    return isValid;

  };

  //////////////////////////////////////////////////////////
  ////  Is Form Valid
  //////////////////////////////////////////////////////////

  const isFormValid = ( $form ) => {

    let isValid = true;
    let requiredFields = $form.find('.required');
    let rudeField = $form.find('.rude');
    let requiredValues = {};

    $( requiredFields ).each(function(){

      let thisField = $(this);
      let fieldValue = thisField.val();
      let fieldName = thisField.attr('name');
      let fieldType = thisField.prop('nodeName');
      let fieldValid = isFieldValid( thisField, fieldValue, fieldType );

      if ( fieldValid ) {
        thisField.closest('.form__field').removeClass('error');
      } else {
        thisField.closest('.form__field').addClass('error');
      }

      requiredValues[fieldName] = { valid: fieldValid, value: fieldValue, type: fieldType };

    });

    if ( DEBUG ) {
      console.log( 'isFormValid', requiredValues );
    }

    $.each( requiredValues, function( key, value ) {
      if ( value.valid == false ) {
        isValid = false;
        return false;
      }
    });

    if ( DEBUG ) {
      console.log( 'rudeField.val():', rudeField.val() );
    }

    if ( rudeField.val() ) {
      isValid = false;
    }

    return isValid;

  };

  //////////////////////////////////////////////////////////
  ////  Submit Form
  //////////////////////////////////////////////////////////

  const submitForm = ( $form ) => {

    let action = $form.attr('action') || false;
    let data = new FormData( $form[0] ) || false;

    if ( $form && action && data ) {
      $.ajax({
        url: action,
        type: 'POST',
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        formElement: $form,
        success: function ( $data, $textStatus, $jqXHR ) {
          confirmation( this.formElement );
        },
        error: function( $jqXHR, $textStatus, $errorThrown ) {
          console.log( '[ Forms.init() ] Ajax Error' );
          console.log( [ $jqXHR, $textStatus, $errorThrown ] );
        }
      });
    }

    // ---------------------------------------- Confirmation

    function confirmation( $form ) {

      console.log( 'confirmation( $form )', $form );

      setTimeout(function() {
        $form.find('.form__main').fadeOut( 300, function(){
          setTimeout(function() {
            $form.find('.form__success').fadeIn( 600, function(){
              reset( $form );
            });
          }, 150 );
        });
      }, 150 );

    }

    // ---------------------------------------- Reset

    function reset( $form ) {
      $form[0].reset();
    }

  };

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    main();
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info : INFO,
    init : init
  };

})();
