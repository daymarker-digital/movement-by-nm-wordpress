//////////////////////////////////////////////////////////
////  Menus
//////////////////////////////////////////////////////////

const DateTime = (() => {

  let DEBUG = true;
  let INFO = { name : 'DateTime', version : '1.0' };
  let BREAKPOINTS = {
    'sm' : 567,
    'md' : 768,
    'lg' : 992,
    'xl' : 1200,
    'xxl' : 1400
  };

  //////////////////////////////////////////////////////////
  ////  The Date & Time
  //////////////////////////////////////////////////////////

  const liveDateAndTime = () => {

    let refresh = 1000;
    let target = 'magazine__date';
    let element = document.getElementById('magazine__date') || false;
    let count = 0;
    let days = [
      'Sun',
      'Mon',
      'Tue',
      'Wed',
      'Thu',
      'Fri',
      'Sat'
    ]
    let months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];

    if ( element ) {

      let myVar = setInterval( myTimer, refresh );

      function myTimer() {

        // create date object
        let d = new Date();

        // get date stuff
        let year = d.getFullYear() // 2019
        let date = d.getDate() // 23
        let dayName = days[d.getDay()];
        let monthName = months[d.getMonth()];
        let minutes = d.getMinutes();
        let seconds = d.getSeconds();
        let hours = d.getHours();
        let ampm = hours >= 12 ? 'PM' : 'AM';

        // for 12 hour clock
        hours = hours % 12;
        hours = hours ? hours : 12;

        seconds = ( seconds < 10 ? '0' + seconds : seconds );
        minutes = ( minutes < 10 ? '0' + minutes : minutes );

        let formatted = `${hours}:${minutes}:${seconds}${ampm}<span>/</span>${dayName}<span>/</span>${monthName} ${date} ${year}`;

        count++;

        element.innerHTML = formatted;

        if ( count == 1 ) {
          element.classList.add("shown");
        }

      }

    }

  };

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    liveDateAndTime();
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info : INFO,
    init : init
  };

})();
