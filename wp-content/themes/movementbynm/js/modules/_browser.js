var Browser = (function () {

  var ua = window.navigator.userAgent;

  //////////////////////////////////////////////////////////
  ////  Info
  //////////////////////////////////////////////////////////

  var Info = { name : 'Browser', version : 1.0 };

  //////////////////////////////////////////////////////////
  ////  Actions
  //////////////////////////////////////////////////////////

  var Actions = {

    //////////////////////////////////////////////////////////
    ////  On Resize
    //////////////////////////////////////////////////////////

    onResize : function ( $function, $delay ) {

      var timer; // Set resizeTimer to empty so it resets on page load

      // On resize, run the $function and reset the timeout
      // $delay in milliseconds. Change as you see fit.

      $(window).on('resize', function() {

        clearTimeout( timer );

        timer = setTimeout( $function, $delay );

      });

      $function;

    },

    //////////////////////////////////////////////////////////
    ////  On Scroll
    //////////////////////////////////////////////////////////

    onScroll : function ( $function, $delay ) {

      var timer; // Set resizeTimer to empty so it resets on page load

      // On resize, run the $function and reset the timeout
      // $delay in milliseconds. Change as you see fit.

      $(window).on('scroll', function() {

        clearTimeout( timer );

        timer = setTimeout( $function, $delay );

      });

      $function;

    }

  };

  //////////////////////////////////////////////////////////
  ////  Cookies
  //////////////////////////////////////////////////////////

  var Cookies = {

    //////////////////////////////////////////////////////////
    ////  Get All Cookies
    //////////////////////////////////////////////////////////

    getAll : function () {

      console.log( '[ BrowserActions() ] Get All Cookies:' );
      console.log( document.cookie );
      console.log( '[ BrowserActions() ] Complete' );

    },

    //////////////////////////////////////////////////////////
    ////  Get Cookie
    //////////////////////////////////////////////////////////

    getCookie : function ( $cookie_name ) {

      var nameEQ = $cookie_name + "=";
      var ca = document.cookie.split(';');

      for ( var i=0; i < ca.length; i++ ) {
        var c = ca[i];
        while ( c.charAt(0)==' ' ) c = c.substring( 1, c.length );
        if ( c.indexOf( nameEQ ) == 0 ) return c.substring( nameEQ.length, c.length );
      }

      return null;

    },

    //////////////////////////////////////////////////////////
    ////  Remove Cookie
    //////////////////////////////////////////////////////////

    removeCookie : function ( $cookie_name ) {

      document.cookie = $cookie_name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/;";

    },

    //////////////////////////////////////////////////////////
    ////  Set Cookie
    //////////////////////////////////////////////////////////

    setCookie : function ( $cookie_name, $cookie_value, $days_until_expired ) {

      var expires;

      if ( $days_until_expired ) {

        var date = new Date();
        date.setTime( date.getTime() + ( $days_until_expired*24*60*60*1000 ) );
        expires = "; expires=" + date.toGMTString();

      } else {
        expires = "";
      }

      document.cookie = $cookie_name + "=" + $cookie_value + expires + "; path=/";

    }

  };

  //////////////////////////////////////////////////////////
  ////  Details
  //////////////////////////////////////////////////////////

  var Details = {

    //////////////////////////////////////////////////////////
    ////  Is Internet Explorer
    //////////////////////////////////////////////////////////

    isIE : function () {

      // Test values; Uncomment to check result …

  		// IE 10
  		// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  		// IE 11
  		// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  		// Edge 12 (Spartan)
  		// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  		// Edge 13
  		// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  		var msie = ua.indexOf('MSIE ');

  		if ( msie > 0 ) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  		}

  		var trident = ua.indexOf('Trident/');
  		if ( trident > 0 ) {
  		  // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  		}

  		var edge = ua.indexOf('Edge/');
  		if ( edge > 0 ) {
  		  // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  		}

  		// other browser
  		return false;

    },

    //////////////////////////////////////////////////////////
    ////  Is iOS Device
    //////////////////////////////////////////////////////////

    isiOSDevice : function () {
		  return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    },

    //////////////////////////////////////////////////////////
    ////  Is Mobile Device
    //////////////////////////////////////////////////////////

    isMobileDevice : function () {
  		if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test( navigator.userAgent ) ) {
  			return true;
  		}
  		return false;
    },

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    info : Info,
    onResize : Actions.onResize,
    onScroll : Actions.onScroll,
    offsetFromTop : Actions.offsetFromTop,
    getAllCookies : Cookies.getAll,
    getCookie : Cookies.getCookie,
    removeCookie : Cookies.removeCookie,
    setCookie : Cookies.setCookie,
    isIE : Details.isIE,
    isiOSDevice : Details.isiOSDevice,
    isMobileDevice : Details.isMobileDevice,
  };

})();
