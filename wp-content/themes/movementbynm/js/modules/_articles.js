//////////////////////////////////////////////////////////
////  Articles
//////////////////////////////////////////////////////////

const Articles = (() => {

  let debug = false;
  let info = { name : 'Articles', version : '1.0' };

  let breakpoints = new Breakpoints();
  let throttled = false;

  //////////////////////////////////////////////////////////
  ////  Gallery
  //////////////////////////////////////////////////////////

  const category = () => {

    let count = 0;

    positionCategory();

    Browser.onResize ( positionCategory, 150 );

    function positionCategory() {

      let viewportWidth = window.innerWidth;
      let offset = ( viewportWidth >= breakpoints.sizes.lg ? 25 : 12 );

      $(".js--position-me").each(function( index, item ) {

        let type = $(item).attr('data-position-type') || false;
        let width = $(item).find('a').outerWidth();

        switch ( type ) {
          case 'magazine':
            //offset = ( viewportWidth >= breakpoints.sizes.lg ? 25 : 12 );
            break;
          case 'additional-post':
            //offset = ( viewportWidth >= breakpoints.sizes.lg ? -6 : -4 );
            break;
        }

        $(item).css({
          'top': width + offset + 'px'
        });

      });

      // console.log( 'positionCategory', count );

      count++;

    };

  };

  //////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {
    category();
  };

  //////////////////////////////////////////////////////////
  ////  Public Method | Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );
    main();
    if ( debug ) console.log( `${info.name}.init() Finished` );
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info,
    init
  };

});
