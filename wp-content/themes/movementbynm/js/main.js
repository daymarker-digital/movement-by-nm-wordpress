// @codekit-prepend "./modules/_articles.js";
// @codekit-prepend "./modules/_breakpoints.js";
// @codekit-prepend "./modules/_browser.js";
// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_gliders.js";
// @codekit-prepend "./modules/_menus.js";
// @codekit-prepend "./modules/_mobileMenu.js";
// @codekit-prepend "./modules/_modals.js";
// @codekit-prepend "./modules/_scrolling.js";
// @codekit-prepend "./modules/_sizing.js";
// @codekit-prepend "./modules/_teamMembers.js";
// @codekit-prepend "./modules/_tools.js";

AOS.init({
  offset: 150,
  delay: 0,
  duration: 550,
  easing: 'ease-in-out',
  startEvent: 'load'
});

let tools = new Tools();
let modules = [
  new Credits(),
  new Articles(),
  new Forms(),
  new Gliders(),
  new MobileMenu(),
  new Modals(),
  new Scrolling(),
  new TeamMembers(),
];

window.addEventListener( 'load', function (e) {
  AOS.refresh();
  modules.forEach( module => module.init() );
});

window.addEventListener( 'resize', tools.debounce(() => {
  //console.log('resize debounced');
}, 300));

window.addEventListener( 'resize', tools.throttle(() => {
  //console.log('resize throttled');
}, 300));

window.addEventListener( 'scroll', tools.debounce(() => {
  //console.log('scroll debounced');
}, 300));

window.addEventListener( 'scroll', tools.throttle(() => {
  //console.log('scroll throttled');
}, 300));
