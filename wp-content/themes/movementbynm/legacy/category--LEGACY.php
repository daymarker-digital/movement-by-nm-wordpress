<?php

/*
*
* Filename: category.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

echo '<div class="magazine">';

  include( locate_template( './snippets/layout--magazine--header.php' ) );

  include( locate_template( './snippets/layout--magazine--body.php' ) );

echo '</div>';

get_footer();

?>
