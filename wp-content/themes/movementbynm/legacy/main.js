///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "../node_modules/jquery/dist/jquery.min.js";
// @codekit-prepend quiet "../node_modules/@popperjs/core/dist/umd/popper.min.js";
// @codekit-prepend quiet "../node_modules/bootstrap/dist/js/bootstrap.min.js";
// @codekit-prepend quiet "../node_modules/@glidejs/glide/dist/glide.min.js";
// @codekit-prepend quiet "../node_modules/micromodal/dist/micromodal.min.js";
// @codekit-prepend quiet "../node_modules/validator/validator.min.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Daymarker
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_articles.js";
// @codekit-prepend "./modules/_browser.js";
// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_gliders.js";
// @codekit-prepend "./modules/_menus.js";
// @codekit-prepend "./modules/_modals.js";
// @codekit-prepend "./modules/_scrolling.js";
// @codekit-prepend "./modules/_sizing.js";
// @codekit-prepend "./modules/_team-members.js";
// @codekit-prepend "./modules/_theme.js";

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts - Immediately!
//////////////////////////////////////////////////////////////////////////////////////////

$(function(){

  // Sizing
  Sizing.init();

  // Date Time
  Articles.init();

}());

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts - When Document Ready!
//////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function() {

  // Gliders
  Gliders.init();

  // Forms
  Forms.init();

  // Menus
  Menus.init();

  // Team Members
  TeamMembers.init();

});

let credits = new Credits();
let modals = new Modals();
let scrolling = new Scrolling();

Theme.init([
  credits,
  modals,
  scrolling
]);
