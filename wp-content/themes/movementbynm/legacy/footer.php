<?php

/*
*
* Filename: footer.php
*
*/

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet vars
//////////////////////////////////////////////////////////

if ( get_field( 'enable_main_newsletter' ) ) {

  $block_name = 'lead-generator';

  echo '<section class="section section--lead-generator lead-generator lead-generator--main">';
    echo '<div class="container"><div class="row"><div class="col-12 col-lg-8 offset-lg-2">';

      if ( have_rows( 'leads', 'options' ) ) {
        while ( have_rows( 'leads', 'options' ) ) {

          // init data
          the_row();

          if ( have_rows( 'main' ) ) {
            while ( have_rows( 'main' ) ) {

              // init data
              the_row();

              // default data
              $banner_image = $banner_link = $form = $form_post_url = $heading = $message = false;

              // get data
              if ( get_sub_field( 'banner_image' ) ) {
                $banner_image = get_sub_field( 'banner_image' );
              }
              if ( get_sub_field( 'banner_link' ) ) {
                $banner_link = get_sub_field( 'banner_link' );
              }
              if ( get_sub_field( 'form' ) ) {
                $form = get_sub_field( 'form' );
              }
              if ( get_sub_field( 'heading' ) ) {
                $heading = get_sub_field( 'heading' );
              }
              if ( get_sub_field( 'message' ) ) {
                $message = get_sub_field( 'message' );
              }

              // print data
              if ( $heading ) {
                echo '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>';
              }
              if ( $message ) {
                echo '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
              }

              if ( $banner_image ) {
                echo '<div class="' . $block_name . '__image image">';
                  echo $banner_link ? '<a href="' . $banner_link . '" target="_blank">' : '';
                  echo $Templates->render_lazyload_image( [ 'image' => $banner_image ] );
                  echo $banner_link ? '</a>' : '';
                echo '</div>';
              }

              if ( $form ) {
                echo '<div class="' . $block_name . '__form">';
                  echo $Templates->render( 'form', [ 'id' => $form->ID, 'class_modifier' => 'main-newsletter' ] );
                echo '</div>';
              }

            }
          }

        }
      }

    echo '</div></div></div>';
  echo '</section>';

}

echo '</main>';

echo '<!-- Footer -->';
include( locate_template( './snippets/theme--footer.php' ) );

echo '<!-- Newsletter Popup Modal -->';
include( locate_template( './snippets/modal--newsletter-popup.php' ) );

// WordPress Footer Hook
wp_footer();

echo '</body>';

echo '</html>';

?>
