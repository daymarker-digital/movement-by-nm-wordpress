<?php

public function lazyload_image ( $media = false, $params = [] ) {

  // default data
  $html = '';
  $classes = 'lazyload lazyload-item lazyload-item--image';
  
  // default settings
  $alt_text = '';
  $animation_timing = 280;
  $animation_delay = 0;
  $background = false;
  $is_svg = false;
  $preload = true;
  $responsive = true;
  $sizes_title = $this->custom_image_title;
  
  // conditionally update media settings
  if ( !empty( $media ) ) {
    if ( isset( $media['subtype'] ) && 'svg+xml' == $media['subtype'] ) {
      $is_svg = true;
    }
    if ( isset( $media['alt'] ) && !empty( $media['alt'] ) ) {
      $alt_text = $media['alt'];
    }
  }
  
  // conditionally update params settings
  if ( !empty( $params ) ) {
    if ( isset( $params['alt_text'] ) && $params['alt_text'] ) {
      $alt_text = $params['alt_text'];
    }
    if ( isset( $params['delay'] ) && $params['delay'] ) {
      $animation_delay = $params['delay'];
    }
    if ( isset( $params['timing'] ) && $params['timing'] ) {
      $animation_timing = $params['timing'];
    }
    if ( isset( $params['background'] ) && $params['background'] ) {
      $background = true;
    }
    if ( isset( $params['preload'] ) && !$params['preload'] ) {
      $preload = false;
    }
    if ( isset( $params['responsive'] ) && !$params['responsive'] ) {
      $responsive = false;
    }
  }
  
  // build classes
  if ( $background ) {
    $classes .= ' lazyload-item--background';
  } else {
    $classes .= ' lazyload-item--inline';
  }
  if ( $preload ) {
    $classes .= ' lazypreload';
  }

  // build element
  if ( !empty( $media ) ) {

    if ( $background ) {
      
      $html = '<div
        class="' . $classes . '"
        data-bg="' . $media['url'] . '"
        data-animation-timing="' . $animation_timing . '"
        data-animation-delay="' . $animation_delay . '"
        style="background-url(' . $media['sizes'][$sizes_title . '-1'] . ');"
        ></div>';

     } else {

      $html = '<img
        class="' . $classes . '"
        data-src="' . $media['url'] . '"
        data-animation-timing="' . $animation_timing . '"
        data-animation-delay="' . $animation_delay . '"';

      if ( !$is_svg ) {

        $html .= ' src="' . $media['sizes'][$sizes_title . '-10'] . '"';

        if ( $responsive ) {

          $srcset = '';
          $sizes = $this->custom_image_sizes;

          foreach ( $sizes as $index => $size ) {
            if ( 0 == $index ) {
              $srcset .= $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
            } else {
              $srcset .= ', ' . $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
            }
          }

          $html .= ' data-sizes="auto"';
          $html .= ' data-srcset="' . $srcset . '"';

        }

      }

      if ( $alt_text ) {
        $html .= ' alt="' . $alt_text . '"';
      }

      $html .= ' />';

    }

  }

  return $html;

}


?>