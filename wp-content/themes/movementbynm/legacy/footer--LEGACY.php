<?php

/*
*
* Filename: footer.php
*
*/

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

//////////////////////////////////////////////////////////
////  Snippet vars
//////////////////////////////////////////////////////////

if ( have_rows( 'page_newsletter' ) ) {
  while ( have_rows( 'page_newsletter' ) ) {

    // init data
    the_row();

    if ( get_sub_field( 'enable' ) ) {

      echo '<section class="section section--newsletter newsletter newsletter--main">';

        if ( have_rows( 'newsletter_main', 'options' ) ) {
          while ( have_rows( 'newsletter_main', 'options' ) ) {

            // init data
            the_row();

            // default data
            $heading = $insta_banner = $insta_link = $message = false;

            // get data
            if ( get_sub_field( 'heading' ) ) {
              $heading = get_sub_field( 'heading' );
            }
            if ( get_sub_field( 'instagram_banner' ) ) {
              $insta_banner = get_sub_field( 'instagram_banner' );
            }
            if ( get_sub_field( 'instagram_link' ) ) {
              $insta_link = get_sub_field( 'instagram_link' );
            }
            if ( get_sub_field( 'message' ) ) {
              $message = get_sub_field( 'message' );
            }

            if ( $heading ) {
              echo '<h2 class="newsletter__heading heading">' . $heading . '</h2>';
            }
            if ( $message ) {
              echo '<div class="newsletter__message rte">' . $message . '</div>';
            }
            if ( $insta_banner && $insta_link ) {
              echo '<div class="newsletter__banner">';
                echo '<a href="' . $insta_link . '">';
                  echo $DD->lazyload_image( $insta_banner );
                echo '</a>';
              echo '</div>';
            }

            include( locate_template( './snippets/form--newsletter-main.php' ) );

          }
        }

      echo '</section>';

    }

  }
}

echo '</main>';

// Footer
echo '<!-- Footer -->';
include( locate_template( './snippets/theme--footer.php' ) );

// WordPress Footer Hook
wp_footer();

echo '</body>';

echo '</html>';

?>
