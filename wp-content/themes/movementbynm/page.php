<?php

/*
*
* Filename: page.php
*
*/

get_header();

if ( have_posts() ) {
  while ( have_posts() ) {

    // init post data
    the_post();

    // default data
    the_content();

  }
}

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

include( locate_template( "./snippets/layout--hero.php" ) );
include( locate_template( "./snippets/layout--flexible-content.php" ) );

get_footer();

?>
