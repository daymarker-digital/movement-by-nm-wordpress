<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

<head>

  <title><?php wp_title(''); ?></title>

	<?php

    // ---------------------------------------- Theme Vars
    $THEME = new CustomTheme();
    $assets_dir = $THEME->get_theme_directory('assets');
    $theme_dir = $THEME->get_theme_directory();
    $theme_classes = $THEME->get_theme_classes();

    // ---------------------------------------- Google Tag Manager
    include( locate_template( './snippets/google-tag-manager.php' ) );

    // ---------------------------------------- Pre-connect External Resources
    echo $THEME->render_preconnect_resources([
      'https://cdn.jsdelivr.net',
      'https://www.google-analytics.com'
    ]);

    // ---------------------------------------- Pre-load Fonts
    echo $THEME->render_preload_fonts([
      'AvenirNext-Bold',
      'AvenirNext-BoldItalic',
      'AvenirNext-DemiBold',
      'AvenirNext-DemiBoldItalic',
      'AvenirNext-Heavy',
      'AvenirNext-HeavyItalic',
      'AvenirNext-Italic',
      'AvenirNext-Medium',
      'AvenirNext-MediumItalic',
      'AvenirNext-Regular',
      'AvenirNext-UltraLight',
      'AvenirNext-UltraLightItalic',
    ]);

  ?>

  <link rel="apple-touch-icon" href="<?= $theme_dir; ?>/apple-touch-icon.png?v=<?= filemtime( get_template_directory() . '/apple-touch-icon.png' ); ?>">
  <link rel="shortcut icon" href="<?= $theme_dir; ?>/favicon.ico?v=<?= filemtime( get_template_directory() . '/favicon.ico' ); ?>">

  <meta charset="<?= get_bloginfo('charset'); ?>">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Daymarker Digital">
  <meta http-equiv="Expires" content="7" />

  <link rel="preload" href="<?= $theme_dir; ?>/style.css" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="<?= $assets_dir; ?>/main.min.js?ver=<?= filemtime( get_template_directory() . '/assets/main.min.js' ); ?>" as="script">

  <!-- NEW STUFF -->
  <meta name="google-site-verification" content="ZnC33Qb1Z7Cvmr5PxL04hNzryj4UCn7tERVd_O52Scc" />
  <meta name="msvalidate.01" content="3FDE8AEE7FE3FCCA3B936BA4AA39ACB9" />
  <!-- /NEW STUFF -->

  <?php

    // ---------------------------------------- SEO
    echo $THEME->render_seo();

    // ---------------------------------------- Inline Styles
    include( locate_template( './snippets/inline-styles.php' ) );

    // ---------------------------------------- External JavaScript
    include( locate_template( './snippets/external-javascript.php' ) );

    // ---------------------------------------- WP Head Hook
    wp_head();

  ?>

</head>

<body class='<?= $theme_classes; ?> sticky-footer yep updated-03.26.23'>

  <?php

    // ---------------------------------------- Theme Vitals
    echo is_user_logged_in() || is_admin() ? $THEME->get_theme_info( 'vitals' ) : '';

    // ---------------------------------------- Mobile Menu
    include( locate_template( './snippets/mobile-menu.php' ) );

    // ---------------------------------------- Header
    include( locate_template( './snippets/header.php' ) );

  ?>

  <main class="<?= $theme_classes; ?>" role="main">
