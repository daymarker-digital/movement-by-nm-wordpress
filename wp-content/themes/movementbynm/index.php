<?php

  /**
  *
  *	Template Name: Index
  *	Filename: index.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  if ( have_posts() ) {
    while ( have_posts() ) {

	    // init post data
	    the_post();

	    // default data
      the_content();

    }
  }

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
