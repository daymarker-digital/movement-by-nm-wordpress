<?php

/*
*
* Template Name: 404
* Filename: 404.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme vars
//////////////////////////////////////////////////////////

$DD = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Template vars
//////////////////////////////////////////////////////////

$block_name = 'error-404';
$cta = $heading = $icon = $message = false;

if ( have_rows( 'error_404', 'options' ) ) {
  while( have_rows( 'error_404', 'options' ) ) {

    // init data
    the_row();

    // get data
    if ( get_sub_field( 'cta' ) ) {
      $cta = get_sub_field( 'cta' );
    }
    if ( get_sub_field( 'heading' ) ) {
      $heading = get_sub_field( 'heading' );
    }
    if ( get_sub_field( 'icon' ) ) {
      $icon = get_sub_field( 'icon' );
    }
    if ( get_sub_field( 'message' ) ) {
      $message = get_sub_field( 'message' );
    }

  }
}

echo '<section class="section section--' . $block_name . ' ' . $block_name . '">';
  echo '<div class="' . $block_name . '__main">';
    echo '<div class="' . $block_name . '__content">';

      if ( $icon ) {
        echo '<div class="' . $block_name . '__icon">' . $Templates->render_lazyload_image( [ 'alt_text' => '404', 'image' => $icon ] ) . '</div>';
      }
      if ( $heading ) {
        echo '<h1 class="' . $block_name . '__heading heading">' . $heading . '</h1>';
      }
      if ( $message ) {
        echo '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
      }

      if ( $cta && isset( $cta['appearance'] ) && !empty( $cta['appearance'] ) ) {
        $cta_template = 'cta.' . $cta['appearance'];
        echo $Templates->render( $cta_template, $cta );
      }

    echo '</div>';
  echo '</div>';
echo '</section>';

get_footer();

?>
