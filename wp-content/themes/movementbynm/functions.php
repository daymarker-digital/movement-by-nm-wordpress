<?php

/*
*
*	Filename: functions.php
*
*/

$includes = [
  'tools',
  'advanced-custom-fields',
  'dashboard-customizer',
  'image-sizes',
  'optimization',
  'custom-post-types',
  'custom-theme-base',
  'custom-theme-templates',
  'custom-theme',
  'pre-get-posts',
  'security',
  'utilities',
  'wysiwyg-formats',
  'enqueue-scripts-styles',
];

foreach( $includes as $include ) {
  include_once( 'functions/' . $include . '.php' );
}

function change_post_author_meta( $output, $presentation ) {

  $post_id = get_the_ID() ?: 0;
  $post_featured_data = get_field( 'featured', $post_id ) ?: [];
  $post_author = $post_featured_data['author'] ?? '';

  return $post_author && is_single() ? "{$post_author}" : "{$output}";

}

add_filter( 'wpseo_meta_author', 'change_post_author_meta', 10, 2 );


class DaymarkerDigital {

  //////////////////////////////////////////////////////////
  ////  Vars
  //////////////////////////////////////////////////////////

  private $version = 1.0;

  public $custom_image_title = "custom-image-size";
  public $custom_image_sizes = array( 1, 10, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 );

  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////

  public function __construct () {}

  //////////////////////////////////////////////////////////
  ////  Theme Classes
  //////////////////////////////////////////////////////////

  public function theme_classes () {

    global $post;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$post_template = basename( get_page_template( $post_ID ), ".php" );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--'. $post_template;
  		$classes .= ' '. $post_template;

  	}

  	return $classes;

  }

  //////////////////////////////////////////////////////////
  ////  Theme Info
  //////////////////////////////////////////////////////////

  public function theme_info ( $param = 'version' ) {

    global $post;

    switch ( $param ) {

      case 'version':
				return $this->version;
				break;
			case 'post_ID':
				return get_the_ID();
				break;
			case 'post_type':
				return get_post_type( get_the_ID() );
				break;
			case 'template':
				return basename( get_page_template(), ".php" );
				break;
			case 'wp_version':
				return get_bloginfo( "version" );
				break;
			case 'php_version':
				return phpversion();
				break;
    }

  }

  //////////////////////////////////////////////////////////
  ////  Theme Directory
  //////////////////////////////////////////////////////////

  public function theme_directory ( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  //////////////////////////////////////////////////////////
  ////  Google Maps Directions
  //////////////////////////////////////////////////////////

  public function directions ( $address = false, $address_2 = false, $city = false, $region = false, $postal = false, $country = false, $name = false ) {

    $result = "";
    $search_query = "https://www.google.com/maps/search/?api=1&query=";

    if ( $address ) {
      $result = $address;
      if ( $address_2 ) {
        $result = $address_2 . "–" . $address;
      }
    }

    if ( $city ) {
      $result .= ' ' . $city;
    }
    if ( $region ) {
      $result .= ' ' . $region;
    }
    if ( $postal ) {
      $result .= ' ' . $postal;
    }
    if ( $country ) {
      $result .= ' ' . $country;
    }
    if ( $name ) {
      $result = $name . ' ' . $result;
    }

    if ( $result != "" ) {
      $result = urlencode( trim( $result ) );
      $result = $search_query . $result;
      return $result;
    }

    return false;

  }

  //////////////////////////////////////////////////////////
  ////  Get Featured Image
  //////////////////////////////////////////////////////////

  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = [];
    $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

}

//////////////////////////////////////////////////////////
////  Daymarker Digital Templates
//////////////////////////////////////////////////////////

include_once( 'snippets/function--templates.php' );

