<?php

  /**
  *
  *	Filename: header.php
  *
  */

  // ---------------------------------------- Data
  $THEME = new CustomTheme();
  $id = get_the_ID() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'team';
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  $template_data = get_field( 'the_team' ) ?: [];

if ( have_rows( 'the_team' ) ) {

  echo '<section class="section section--' . $template  . ' ' . $template  . '">';
    echo '<div class="container"><div class="row"><div class="col-12 col-sm-10 offset-sm-1">';
      echo '<div class="row row--inner">';

        //////////////////////////////////////////////////////////
        ////  Left Column
        //////////////////////////////////////////////////////////

        echo '<div class="col-12 col-lg-4">';

          echo '<nav class="' . $template  . '__nav">';
           echo '<ul class="' . $template  . '__nav-list">';
              while ( have_rows( 'the_team' ) ) {
                the_row();
                $heading = get_sub_field( 'heading' );
                $row_index = get_row_index();
                $block_id = $template . '--' . Tools::handleize( $heading );
                echo '<li class="' . $template  . '__nav-item' . ( $row_index == 1 ? ' active' : '' ) . '" data-target="#' . $block_id . '">' . $heading . '</li>';
              }
            echo '</ul>';
          echo '</nav>';

          echo '<ul class="' . $template  . '__group-info-list">';
            while ( have_rows( 'the_team' ) ) {
              the_row();
              $message = get_sub_field( 'message' );
              $row_index = get_row_index();
              echo '<li class="' . $template  . '__group-info-item' . ( $row_index == 1 ? ' active' : '' ) . '">';
                echo '<div class="' . $template  . '__message message rte">' . $message . '</div>';
              echo '</li>';
            }
          echo '</ul>';

        echo '</div>';

        //////////////////////////////////////////////////////////
        ////  Right Column
        //////////////////////////////////////////////////////////

        echo '<div class="col-12 col-lg-8">';

          while ( have_rows( 'the_team' ) ) {

            the_row();

            $heading = get_sub_field( 'heading' );
            $team_row_index = get_row_index();
            $member_count = count( get_sub_field( 'members_list' ) );
            $block_id = $template . '--' . Tools::handleize( $heading );

            if ( have_rows( 'members_list' ) ) {

              echo '<div class="' . $template  . '__group' . ( $team_row_index == 1 ? ' active' : '' ) . '" id="' . $block_id . '">';
                echo '<div class="' . $template  . '__grid" data-member-count="' . $member_count . '">';

                  while ( have_rows( 'members_list' ) ) {

                    // init data
                    the_row();

                    $current_index = get_row_index();
                    $remainder = $current_index % 3;
                    $show_bio_block = false;

                    if ( $current_index > 0 && $remainder == 0 ) {
                      $show_bio_block = true;
                    }
                    if ( $current_index == $member_count && $remainder > 0 ) {
                      $show_bio_block = true;
                    }

                    echo '<div class="' . $template  . '__grid-item" data-index="' . $current_index . '" data-remainder="' . $remainder . '">';

                      $member = get_sub_field( 'member' );

                      if ( $member ) {

                        $member_id = $member->ID;

                        echo '<div class="team-member">';

                          echo '<div class="team-member__hr"></div>';

                          if ( have_rows( 'team_member', $member_id ) ) {
                            while ( have_rows( 'team_member', $member_id ) ) {

                              // init data
                              the_row();

                              // get data
                              $full_name = '';
                              $first_name = get_sub_field( 'f_name' );
                              $middle_name = get_sub_field( 'm_name' );
                              $last_name = get_sub_field( 'l_name' );
                              $profile_picture = get_sub_field( 'profile_picture' );
                              $bio = get_sub_field( 'bio' );

                              // build data
                              if ( $first_name ) {
                                $full_name .= '<span class="first">' . $first_name . '</span>';
                                if ( $middle_name ) {
                                  $full_name .= '<span class="middle">' . $middle_name . '</span>';
                                }
                                if ( $last_name ) {
                                  $full_name .= '<span class="last">' . $last_name . '</span>';
                                }
                              }

                              if ( $full_name ) {
                                echo '<h3 class="team-member__name">' . $full_name . '</h3>';
                              }

                              if ( true ) {
                                echo '<div class="team-member__photo">' . $Templates->render_lazyload_image( [ 'image' => $profile_picture ] ) . '</div>';
                              }

                              echo '<div class="team-member__details" style="display: none;">';

                                if ( $bio ) {
                                  echo '<div class="team-member__bio rte">' . $bio . '</div>';
                                }

                                if ( have_rows( 'social' ) ) {
                                  echo '<div class="team-member__social">';
                                    while ( have_rows( 'social' ) ) {

                                      // init data
                                      the_row();

                                      $icon = get_sub_field( 'icon' );
                                      $link = get_sub_field( 'link' );

                                      if ( $icon && $link ) {
                                        echo '<div class="team-member__social-item">';
                                          echo '<a href="' . $link . '" target="_blank" rel="nofollow noopener">';
                                            echo '<img src="' . $icon["url"] . '" />';
                                          echo '</a>';
                                        echo '</div>';
                                      }

                                    }
                                  echo '</div>';
                                }

                              echo '</div>';

                            }
                          }

                         echo '</div>';

                      }

                    echo '</div>';

                    if ( $show_bio_block ) {
                      echo '<div class="team__grid-item team__grid-item--bio"></div>';
                    }

                  }

                echo '</div>';
              echo '</div>';

            }

          }

        echo '</div>';

      echo '</div>';
    echo '</div></div></div>';
  echo '</section>';

}

?>
