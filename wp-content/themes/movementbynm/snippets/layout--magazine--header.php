<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

if ( have_rows( 'magazine_header', 'options' ) ) {
  while( have_rows( 'magazine_header', 'options' ) ) {

    // init data
    the_row();

    // default data
    $brand = $navigation = false;

    // get data
    if ( get_sub_field( 'brand' ) ) {
      $brand = get_sub_field( 'brand' );
    }
    if ( get_sub_field( 'navigation' ) ) {
      $navigation = get_sub_field( 'navigation' );
    }

    // print data
    if ( $brand || $navigation ) {
      echo '<div class="magazine__header">';

        // inject JS live clock here
        echo '<div class="magazine__date" id="magazine__date">' . date("h:i:s A") . '</div>';

        if ( $brand ) {
          echo '<div class="magazine__brand">' . $Templates->render_lazyload_image( [ 'image' => $brand ] ) . '</div>';
        }

        if ( $navigation ) {
          echo '<nav class="magazine__menu menu">';
            echo '<ul class="menu__list">';
            foreach ( $navigation as $i => $item ) {

              // default data
              $cat_id = $cat_link = $cat_name = false;

              if ( isset( $item['link'] ) && !empty( $item['link'] ) ) {

                $cat_id = $item['link']->term_id;
                $cat_link = get_category_link( $cat_id );
                $cat_name = $item['link']->name;

                if ( $cat_link && $cat_name ) {
                  echo '<li class="menu__item">';
                    echo '<a href="' . $cat_link . '">' . $cat_name . '</a>';
                  echo '</li>';
                }

              }

            }
          echo '</nav>';
        }

      echo '</div>';
    }

  }
}

?>
