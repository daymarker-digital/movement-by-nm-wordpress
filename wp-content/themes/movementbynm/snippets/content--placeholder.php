<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'placeholder';

?>

<section class="section section--<?php echo $block_name . ' ' . $block_name; ?>">

  <div class="<?php echo $block_name; ?>__header d-none">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
          <h1>H1 Placeholder</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="<?php echo $block_name; ?>__grid d-none">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">

          <h2>H2 Grid System</h2>

          <div class="row row--inner">
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
            <div class="col-1">
              <span>1</span>
            </div>
          </div>

          <div class="row row--inner">
            <div class="col-2">
              <span>2</span>
            </div>
            <div class="col-2">
              <span>2</span>
            </div>
            <div class="col-2">
              <span>2</span>
            </div>
            <div class="col-2">
              <span>2</span>
            </div>
            <div class="col-2">
              <span>2</span>
            </div>
            <div class="col-2">
              <span>2</span>
            </div>
          </div>

          <div class="row row--inner">
            <div class="col-3">
              <span>3</span>
            </div>
            <div class="col-3">
              <span>3</span>
            </div>
            <div class="col-3">
              <span>3</span>
            </div>
            <div class="col-3">
              <span>3</span>
            </div>
          </div>

          <div class="row row--inner">
            <div class="col-4">
              <span>4</span>
            </div>
            <div class="col-4">
              <span>4</span>
            </div>
            <div class="col-4">
              <span>4</span>
            </div>
          </div>

          <div class="row row--inner">
            <div class="col-6">
              <span>6</span>
            </div>
            <div class="col-6">
              <span>6</span>
            </div>
          </div>

          <div class="row row--inner">
            <div class="col-12">
              <span>12</span>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="<?php echo $block_name; ?>__typography">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">

          <h2>H2 typography</h2>

          <p>Pellentesque at interdum enim. Suspendisse vulputate convallis mi quis auctor. Cras at urna mi. Quisque pretium tempus lacus in viverra. Sed auctor erat enim, sed accumsan orci tristique sit amet. Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</p>

          <p>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum. Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt a. In eget purus massa. Nulla facilisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris laoreet sapien vel odio accumsan, sed posuere libero vestibulum. Praesent est felis, tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida ac pharetra ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

          <ol>
            <li>Tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida</li>
            <li>Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt</li>
            <li>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum</li>
            <li>Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</li>
          </ol>

          <ul>
            <li>Tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida</li>
            <li>Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt</li>
            <li>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum</li>
            <li>Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</li>
          </ul>

          <p>Sed ac rutrum turpis, id posuere justo. Nullam finibus egestas neque eget luctus. Etiam vel dapibus erat. Sed hendrerit dui sed eros ultrices gravida a a turpis. Phasellus lectus nisl, molestie sit amet ullamcorper at, faucibus et mauris. Pellentesque consequat tempor turpis eu posuere. Nam faucibus est vitae odio blandit, sed maximus sem suscipit. Suspendisse pharetra maximus eros, at semper ante blandit eu. Nam aliquam sollicitudin purus, sed imperdiet diam. Sed lacinia tristique eros a malesuada. Nullam convallis eu risus blandit tristique. Nam eget odio convallis erat imperdiet bibendum non nec massa. Pellentesque ut accumsan leo, a euismod tellus.</p>

          <h3>H3 Typography</h3>

          <p>Pellentesque condimentum ultrices est eget hendrerit. Integer molestie magna massa, sit amet dapibus erat condimentum non. Cras egestas odio ac diam laoreet condimentum. Donec consectetur interdum enim at convallis. Praesent ornare volutpat dapibus. Nunc ornare, nibh at viverra malesuada, mi erat scelerisque dui, ut posuere elit purus id enim. Aliquam mollis libero at arcu mattis finibus. Praesent viverra sollicitudin nunc quis feugiat. Sed a justo pulvinar, sollicitudin mauris in, lobortis nibh. Praesent porttitor at ligula vitae laoreet. Nunc placerat lectus a aliquet ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas malesuada lectus ut magna varius, vitae venenatis nisi imperdiet. Nam ut egestas mi, ut pretium arcu.</p>

          <p>hasellus nulla nulla, fringilla rhoncus imperdiet ac, luctus id diam. Donec vel sapien feugiat, placerat tellus vel, pulvinar mi. Cras sed pulvinar velit. Vivamus eget finibus erat. Nulla eu consequat enim. Proin eget erat aliquet, pretium magna ac, scelerisque arcu. Integer ornare lorem ligula, sollicitudin dignissim ipsum iaculis vel. Sed facilisis nulla leo. Duis efficitur egestas eros in vehicula.</p>

          <h4>H4 Typography</h4>

          <p>Nulla facilisi. Quisque cursus venenatis dolor, id porta justo cursus et. Donec urna felis, congue vel bibendum non, ultricies ut massa. Suspendisse semper augue velit, nec blandit enim iaculis a. Nulla vel egestas ligula. Morbi mi neque, luctus et nisi accumsan, tempus scelerisque libero. Nunc quis suscipit eros. In velit leo, tempus nec neque id, tempor tempor sapien. Pellentesque pharetra magna in facilisis imperdiet. Proin scelerisque rutrum neque eu dapibus.</p>

          <p>Vivamus consectetur velit vestibulum, dictum quam et, convallis enim. Vestibulum molestie arcu dolor, eu rutrum dui laoreet sit amet. Nullam luctus auctor augue, eu tempus ex efficitur vel. Phasellus sit amet mattis tortor. Aenean augue tortor, porttitor sed maximus vel, maximus id diam. Aliquam pharetra dolor risus, non tincidunt tortor dignissim at. Proin mollis volutpat metus, ac fringilla elit faucibus quis. Maecenas hendrerit aliquam ipsum, quis vehicula quam rutrum nec.</p>

          <h2>H2 Typography</h2>

          <p>Maecenas aliquet ipsum a porttitor tincidunt. Proin sagittis lectus gravida diam hendrerit accumsan. Quisque pharetra dapibus nulla, a tempor felis malesuada non. Morbi dapibus nibh a leo consequat, vitae vulputate lorem gravida. Morbi aliquam nunc quis leo varius dictum. Donec feugiat fermentum lorem, non vehicula massa fermentum vel. Vestibulum a mauris erat. Morbi pretium lobortis elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

          <p>Pellentesque interdum, dui eu facilisis sollicitudin, diam ipsum lobortis augue, malesuada fermentum nunc tellus eu nibh. Aliquam erat volutpat. In ullamcorper nisl id commodo laoreet. Suspendisse a nisl vitae enim aliquet dapibus. Donec mollis, arcu in semper pellentesque, metus mauris mattis lorem, vel commodo odio est a diam. Quisque volutpat ligula non felis elementum, eget suscipit dolor pretium. Phasellus aliquet ac nisl a pretium. Vivamus et aliquam velit, non malesuada quam. Vivamus congue molestie nisl eu vestibulum. Maecenas ut sodales ligula.</p>

          <ol>
            <li>Tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida</li>
            <li>Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt</li>
            <li>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum</li>
            <li>Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</li>
          </ol>

          <p>hasellus nulla nulla, fringilla rhoncus imperdiet ac, luctus id diam. Donec vel sapien feugiat, placerat tellus vel, pulvinar mi. Cras sed pulvinar velit. Vivamus eget finibus erat. Nulla eu consequat enim. Proin eget erat aliquet, pretium magna ac, scelerisque arcu. Integer ornare lorem ligula, sollicitudin dignissim ipsum iaculis vel. Sed facilisis nulla leo. Duis efficitur egestas eros in vehicula.</p>

          <h6>Nulla facilisi. Quisque cursus venenatis dolor, id porta justo cursus et. Donec urna felis, congue vel bibendum non, ultricies ut massa. Suspendisse semper augue velit, nec blandit enim iaculis a. Nulla vel egestas ligula. Morbi mi neque, luctus et nisi accumsan, tempus scelerisque libero. Nunc quis suscipit eros. In velit leo, tempus nec neque id, tempor tempor sapien. Pellentesque pharetra magna in facilisis imperdiet. Proin scelerisque rutrum neque eu dapibus.</h6>

          <p>Vivamus consectetur velit vestibulum, dictum quam et, convallis enim. Vestibulum molestie arcu dolor, eu rutrum dui laoreet sit amet. Nullam luctus auctor augue, eu tempus ex efficitur vel. Phasellus sit amet mattis tortor. Aenean augue tortor, porttitor sed maximus vel, maximus id diam. Aliquam pharetra dolor risus, non tincidunt tortor dignissim at. Proin mollis volutpat metus, ac fringilla elit faucibus quis. Maecenas hendrerit aliquam ipsum, quis vehicula quam rutrum nec.</p>

        </div>
      </div>
    </div>
  </div>

  <div class="<?php echo $block_name; ?>__collapsible d-none">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">

          <h2>Typography H2</h2>

          <p>Maecenas aliquet ipsum a porttitor tincidunt. Proin sagittis lectus gravida diam hendrerit accumsan. Quisque pharetra dapibus nulla, a tempor felis malesuada non. Morbi dapibus nibh a leo consequat, vitae vulputate lorem gravida. Morbi aliquam nunc quis leo varius dictum. Donec feugiat fermentum lorem, non vehicula massa fermentum vel. Vestibulum a mauris erat. Morbi pretium lobortis elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

          <p>Pellentesque interdum, dui eu facilisis sollicitudin, diam ipsum lobortis augue, malesuada fermentum nunc tellus eu nibh. Aliquam erat volutpat. In ullamcorper nisl id commodo laoreet. Suspendisse a nisl vitae enim aliquet dapibus. Donec mollis, arcu in semper pellentesque, metus mauris mattis lorem, vel commodo odio est a diam. Quisque volutpat ligula non felis elementum, eget suscipit dolor pretium. Phasellus aliquet ac nisl a pretium. Vivamus et aliquam velit, non malesuada quam. Vivamus congue molestie nisl eu vestibulum. Maecenas ut sodales ligula.</p>

          <div class="collapsible">

            <h2>Collapsible Content</h2>

            <button class="collapsible__trigger" data-toggle="collapse" data-target="#collapse-example" type="button">Toggle Content</button>

            <div class="collapsible__main collapse" id="collapse-example">
              <div class="collapsible__content">
                <p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>

                <p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>
              </div>
              <!-- /.collapsible__body -->
            </div>

          </div>

          <h2>Typography H2</h2>

          <p>Pellentesque at interdum enim. Suspendisse vulputate convallis mi quis auctor. Cras at urna mi. Quisque pretium tempus lacus in viverra. Sed auctor erat enim, sed accumsan orci tristique sit amet. Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</p>

          <p>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum. Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt a. In eget purus massa. Nulla facilisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris laoreet sapien vel odio accumsan, sed posuere libero vestibulum. Praesent est felis, tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida ac pharetra ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

          <p>Sed ac rutrum turpis, id posuere justo. Nullam finibus egestas neque eget luctus. Etiam vel dapibus erat. Sed hendrerit dui sed eros ultrices gravida a a turpis. Phasellus lectus nisl, molestie sit amet ullamcorper at, faucibus et mauris. Pellentesque consequat tempor turpis eu posuere. Nam faucibus est vitae odio blandit, sed maximus sem suscipit. Suspendisse pharetra maximus eros, at semper ante blandit eu. Nam aliquam sollicitudin purus, sed imperdiet diam. Sed lacinia tristique eros a malesuada. Nullam convallis eu risus blandit tristique. Nam eget odio convallis erat imperdiet bibendum non nec massa. Pellentesque ut accumsan leo, a euismod tellus.</p>

        </div>
      </div>
    </div>
  </div>

</section>
