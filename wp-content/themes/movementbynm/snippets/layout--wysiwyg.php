<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'wysiwyg';

if ( get_sub_field( 'wysiwyg' ) ) {
  echo '<section class="section section--' . $block_name . ' ' . $block_name . '">';
    echo '<div class="' . $block_name . '__main">';
      echo '<div class="container"><div class="row"><div class="col-12 col-sm-10 offset-sm-1">';
        echo '<div class="' . $block_name . '__content rte">';
          echo get_sub_field( 'wysiwyg' );
        echo '</div>';
      echo '</div></div></div>';
    echo '</div>';
  echo '</section>';
}

?>
