<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'image-with-text';

if ( have_rows( 'image_text' ) ) {
  while ( have_rows( 'image_text' ) ) {

    // init data
    the_row();

    // default data
    $cta = $image = $layout = $message = false;

    // get data
    if ( get_sub_field( 'cta' ) ) {
      $cta = get_sub_field( 'cta' );
    }
    if ( get_sub_field( 'image' ) ) {
      $image = get_sub_field( 'image' );
    }
    if ( get_sub_field( 'layout' ) ) {
      $layout = get_sub_field( 'layout' );
    }
    if ( get_sub_field( 'message' ) ) {
      $message = get_sub_field( 'message' );
    }

    if ( $image && $message ) {
      echo '<section class="section section--' . $block_name . ' ' . $block_name . '" data-layout="' . $layout . '">';
        echo '<div class="' . $block_name . '__main">';
          echo '<div class="container"><div class="row"><div class="col-12">';
            echo '<div class="' . $block_name . '__content">';

              echo '<div class="' . $block_name . '__image">' . $Templates->render_lazyload_image( [ 'image' => $image ] ) . '</div>';

              echo '<div class="' . $block_name . '__message rte">';
                echo $message;
                echo $cta ? $Templates->render( 'cta', [ 'cta' => $cta ] ) : '';
              echo '</div>';

            echo '</div>';
          echo '</div></div></div>';
        echo '</div>';
      echo '</section>';
    }

  }
}

?>
