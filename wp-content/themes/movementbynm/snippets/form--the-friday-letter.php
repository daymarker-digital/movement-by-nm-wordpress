<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$Templates = new DDTemplates();

?>

<div id="mc_embed_signup">
  <form action="https://bynm.us19.list-manage.com/subscribe/post?u=e12aa83d4cc53bc863a0aa208&amp;id=391a8c1743" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form form--subscribe" target="_blank" novalidate="novalidate">

    <div id="mc_embed_signup_scroll">

      <div class="mc-field-group">
        <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>
        <input type="email" value="" name="EMAIL" class="required email mce_inline_error" id="mce-EMAIL" aria-required="true" aria-invalid="true">
        <div for="mce-EMAIL" class="mce_inline_error">This field is required.</div>
      </div>

      <div class="mc-field-group" style="display:none">
        <label for="mce-group[3815]">Lead Generation </label>
        <select name="group[3815]" class="REQ_CSS" id="mce-group[3815]">
          <option value=""></option>
          <option value="1" am="" option=""></option>
          <option value="2">YF</option>
          <option value="4">MBNM</option>
          <option value="8">Strong</option>
          <option value="16">MF</option>
          <option value="32">Stay Connected</option>
          <option value="64" selected="">&gt;Friday Letter</option>
        </select>
      </div>

    </div>

    <div id="mce-responses" class="clear">
      <div class="response" id="mce-error-response" style="display:none"></div>
      <div class="response" id="mce-success-response" style="display:none"></div>
    </div>

    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e12aa83d4cc53bc863a0aa208_391a8c1743" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>

  </form>
</div>

<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script>
<script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
