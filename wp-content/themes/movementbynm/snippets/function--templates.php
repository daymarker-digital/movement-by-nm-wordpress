<?php

class DDTemplates extends DaymarkerDigital {

  //////////////////////////////////////////////////////////
  ////  Vars
  //////////////////////////////////////////////////////////

  private $version = 1.0;

  //////////////////////////////////////////////////////////
  ////  Render
  //////////////////////////////////////////////////////////

  public function render ( $template = false, $params = [] ) {

    $html = '';
    $class_modifier = false;

    if ( !empty( $params ) ) {

      if ( isset( $params['class_modifier'] ) && !empty( $params['class_modifier'] ) ) {
        $class_modifier = $params['class_modifier'];
      }

      switch ( $template ) {

        //////////////////////////////////////////////////////////
        ////  Article | Image
        //////////////////////////////////////////////////////////

        case 'article.image':

          // default data
          $caption = $image = $index = false;

          // deconstruct $params
          extract( $params );

          if ( $image ) {

            $html .= '<div class="article__image image" data-index="' . $index . '">';
              $html .= $this->render_lazyload_image( [ 'image' => $image ] );
              if ( $caption ) {
                $html .= '<div class="article__caption caption">';
                  $html .= $caption;
                $html .= '</div>';
              }
            $html .= '</div>';

          }

          break;

        ////////////////////////////////////////////////////
        ////  Article | Preview
        //////////////////////////////////////////////////////////

        case 'article.preview':

          $count = $post_id = $type = false;

          // deconstruct $params
          extract( $params );

          if ( $post_id ) {

            $categories = get_the_category( $post_id ) ?: [];
            $featured_image = $this->get_featured_image_by_post_id( $post_id );
            $permalink = get_the_permalink( $post_id ) ?: '';
            $subtitle = '';
            $title = get_the_title( $post_id ) ?: '';


            if ( have_rows( 'featured', $post_id ) ) {
              while( have_rows( 'featured', $post_id ) ) {

                the_row();

                $featured_image_portrait = get_sub_field( 'image' ) ?: [];
                $subtitle = get_sub_field( 'subtitle' ) ?: '';
                $title = get_sub_field( 'title' ) ?: $title;

              }
            }

            // print data
            $html .= '<article class="article" data-post-id="' . $post_id . '" data-post-count="' . $count . '">';

              $html .= '<div class="article__content">';

                $html .= '<div class="article__featured-images">';

                  if ( !empty( $categories ) ) {
                    $cat_id = $categories[0]->term_id;
                    $cat_name = $categories[0]->name;
                    $cat_link = get_category_link( $cat_id );
                    $html .= '<div class="article__category category js--position-me" data-position-type="' . $type . '">';
                      $html .= '<a href="' . $cat_link . '">' . $cat_name . '</a>';
                    $html .= '</div>';
                  }

                  if ( $featured_image ) {
                    $html .= '<div class="article__featured-image article__featured-image--landscape">';
                      $html .= '<a class="article__link" href="' . $permalink . '">';
                        $html .= $this->render_lazyload_image([ 'image' => $featured_image ]);
                      $html .= '</a>';
                    $html .= '</div>';
                  }

                  if ( $featured_image_portrait ) {
                    $html .= '<div class="article__featured-image article__featured-image--portrait">';
                      $html .= '<a class="article__link" href="' . $permalink . '">';
                        $html .= $this->render_lazyload_image([ 'image' => $featured_image_portrait ]);
                      $html .= '</a>';
                    $html .= '</div>';
                  }

                $html .= '</div>';

                $html .= '<h2 class="article__title updated"><a class="article__link" href="' . $permalink . '">' . $title . '</a></h2>';
                $html .= $subtitle ? '<strong class="article__subtitle">' . $subtitle . '</strong>' : '';
                $html .= '<div class="article__cta"><a class="article__link" href="' . $permalink . '">Read More</a></div>';

              $html .= '</div>';

            $html .= '</article>';

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Article | WYSIWYG
        //////////////////////////////////////////////////////////

        case 'article.wysiwyg':

          // default data
          $asides = $content = $index = false;

          // deconstruct $params
          extract( $params );

          if ( $content ) {

            $html .= '<div class="article__wysiwyg wysiwyg" data-index="' . $index . '">';
              $html .= '<div class="container"><div class="row">';

                $html .= '<div class="col-12 col-lg-6 order-lg-2 ' . ( !$asides ? 'offset-lg-3' : '' ) . '">';
                  $html .= '<div class="wysiwyg__content rte">';
                    $html .= $content;
                  $html .= '</div>';
                $html .= '</div>';

                if ( $asides ) {
                  foreach( $asides as $i => $aside ) {

                    // default data
                    $block_quote = $type = $images = false;

                    // deconstruct $aside
                    extract( $aside );

                    $html .= '<div class="col-12 col-lg-3 ' . ( $i == 0 ? 'order-lg-1' : 'order-lg-3' ) . '">';
                      $html .= '<div class="wysiwyg__aside aside aside--' . ( $i == 0 ? 'left' : 'right' ) . '">';

                        switch ( $type ) {
                          case 'block-quote':
                            if ( $block_quote ) {
                              $html .= '<div class="aside__block-quote">';
                                $html .= $block_quote;
                              $html .= '</div>';
                            }
                            break;
                          case 'images':
                            if ( $images ) {
                              $html .= '<ul class="aside__image-list">';
                                foreach ( $images as $i => $item ) {

                                  // default data
                                  $caption = $image = false;

                                  // deconstruct $aside
                                  extract( $item );

                                  if ( $image ) {
                                    $html .= '<li class="aside__image-list-item">';
                                      $html .= '<div class="aside__image image">';
                                        $html .= $this->render_lazyload_image( [ 'image' => $image ] );
                                      $html .= '</div>';
                                      if ( $caption ) {
                                        $html .= '<div class="aside__caption caption">';
                                          $html .= $caption;
                                        $html .= '</div>';
                                      }
                                    $html .= '</li>';
                                  }

                                }
                              $html .= '</ul>';
                            }
                            break;
                        }

                      $html .= '</div>';
                    $html .= '</div>';

                  }
                }

              $html .= '</div></div>';
            $html .= '</div>';

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Blog | Article
        //////////////////////////////////////////////////////////

        case 'background':

          // default data
          $image = $resize_desktop = $resize_mobile = $type = $video = false;
          $appearance = 'full-height';

          // deconstruct $params
          extract( $params );

          $html .= '<div
            class="' . ( $class_modifier ? $class_modifier . '__background ' : '' ) . 'background"
            data-enable-resize="true"
            data-resize-offset-header="false"
            data-resize-appearance="' . $appearance . '"
            data-resize-mobile="' . ( $resize_mobile ? 'true' : 'false' ) . '"
            data-resize-desktop="' . ( $resize_desktop ? 'true' : 'false' ) . '"
          >';

            if ( $image ) {
              $html .= '<div class="background__image">';
                $html .= $this->render_lazyload_image( [ 'background' => true, 'image' => $image ] );
              $html .= '</div>';
            }

            if ( 'video' == $type && $video ) {
              $html .= '<div class="background__video">';
                $html .= $this->render_lazyload_video( [ 'background' => true, 'video' => $video ] );
              $html .= '</div>';
            }

            $html .= '<div class="background__overlay"></div>';

          $html .= '</div>';

          return $html;

          break;

        //////////////////////////////////////////////////////////
        ////  Blog | Article
        //////////////////////////////////////////////////////////

        case 'blog.article':

          // default data
          $id = false;

          // deconstruct $params
          extract( $params );

          if ( $id ) {

            // default data
            $cat_id = $cat_name = $date = $excerpt = $featured_image = $permalink = $title = false;

            // get data
            if ( get_the_category( $id ) ) {
              $post_cats = get_the_category( $id );
              $count = 0;
              foreach ( $post_cats as $i => $cat ) {
                if ( 'uncategorized' != $cat->slug ) {
                  $cat_id = $cat->term_id;
                  $cat_name = $cat->name;
                  $count++;
                }
                if ( $count > 0 ) {
                  break;
                }
              }
              debug_this( [ $cat_id, $cat_name ] );
            }
            if ( get_the_date( "F j, Y", $id ) ) {
              $date = get_the_date( "F j, Y", $id );
            }
            $featured_image = $this->get_featured_image_by_post_id( $id );
            if ( get_permalink( $id ) ) {
              $permalink = get_permalink( $id );
            }
            if ( get_the_title( $id ) ) {
              $title = get_the_title( $id );
            }

            if ( $title ) {
              $html .= '<article class="article article--blog">';
                $html .= '<a href="' . $permalink . '">';

                  $html .= '<div class="article__featured-image">';
                    $html .= $featured_image ? $this->render_lazyload_image( [ 'image' => $featured_image ] ) : '';
                  $html .= '</div>';

                  $html .= '<div class="article__content">';
                    $html .= $date ? '<div class="article__date">' . $date . '</div>' : '';
                    $html .= $title ? '<h2 class="article__title">' . $title . '</h2>' : '';
                  $html .= '</div>';

                $html .= '</a>';
              $html .= '</article>';
            }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Call to Action
        //////////////////////////////////////////////////////////

        case 'cta':

          // default data
          $cta = false;

          // extract / deconstruct params
          extract( $params );

          if ( $cta ) {

            // default data
            $target = '_self';
            $colour = 'black';
            $link = false;
            $appearance = $link_external = $link_internal = $title = $type = false;

            // extract / deconstruct params
            extract( $cta );

            // conditionally get data
            switch( $type ) {
              case 'external':
                $target = '_blank';
                if ( $link_external ) {
                  $link = $link_external;
                }
                break;
              case 'internal':
                if ( $link_internal ) {
                  $link = $link_internal;
                }
                break;
              default:
                break;
            }

            if ( $title && $link ) {

              switch( $appearance ) {
                case 'circle':
                  $html .= '<div class="cta cta--circle" data-colour-theme="' . $colour . '">';
                    $html .= '<a class="cta__link" href="' . $link . '" target="' . $target . '">';
                      $html .= '<span class="cta__inner-circle">';
                        $html .= '<span class="cta__title">' . $title . '</span>';
                      $html .= '</span>';
                    $html .= '</a>';
                  $html .= '</div>';
                  break;
                case 'pill':
                  $html .= '<div class="cta cta--pill" data-colour-theme="' . $colour . '">';
                    $html .= '<a class="cta__link" href="' . $link . '" target="' . $target . '">';
                      $html .= '<span class="cta__title">' . $title . '</span>';
                    $html .= '</a>';
                  $html .= '</div>';
                  break;
                default:
                  break;
              }

            }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Form
        //////////////////////////////////////////////////////////

        case 'form':

          // default data
          $id = false;

          // deconstruct $params
          extract( $params );

          // build template
          if ( $id ) {

            // default data
            $form_rows = $hidden_fields = $post_url = $submit_button_title = false;
            $submit_button_title = 'Submit';
            $success_confirmation = '<p>Thanks for your interest!</p>';

            // get data
            if ( get_field( 'form_rows', $id ) ) {
              $form_rows = get_field( 'form_rows', $id );
            }
            if ( get_field( 'hidden_fields', $id ) ) {
              $hidden_fields = get_field( 'hidden_fields', $id );
            }
            if ( get_field( 'post_url', $id ) ) {
              $post_url = get_field( 'post_url', $id );
            }
            if ( get_field( 'submit_button_title', $id ) ) {
              $submit_button_title = get_field( 'submit_button_title', $id );
            }
            if ( get_field( 'success_confirmation', $id ) ) {
              $success_confirmation = get_field( 'success_confirmation', $id );
            }

            if ( $post_url && $form_rows ) {

              $html .= '<form class="form ' . ( $class_modifier ? 'form--' . $class_modifier : '' ) . ' js--validate-me" method="post" action="' . $post_url . '">';

                $html .= '<div class="form__main">';

                  if ( $hidden_fields ) {
                    $html .= '<div class="form__hidden-fields">';
                      $html .= $this->render( 'form.hidden-fields', [ 'fields' => $hidden_fields ] );
                    $html .= '</div>';
                  }

                  if ( $form_rows ) {
                    $html .= '<div class="form__fields">';

                      $html .= $this->render( 'form.rows', [ 'rows' => $form_rows ] );

                      $html .= '<div class="form__row form__row--rude">';
                        $html .= '<div class="form__field form__field--rude">';
                          $html .= '<input class="rude" type="text" name="rude" tabindex="-1">';
                        $html .= '</div>';
                      $html .= '</div>';

                    $html .= '</div>';
                  }

                  $html .= '<div class="form__action">';
                    $html .= '<button class="form__button button" type="submit">' . $submit_button_title . '</button>';
                  $html .= '</div>';

                $html .= '</div>';

               $html .= '<div class="form__success">';
                  $html .= '<div class="form__success-message message rte">';
                    $html .= $success_confirmation;
                  $html .= '</div>';
                $html .= '</div>';

              $html .= '</form>';

            }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Form | Hidden Field
        //////////////////////////////////////////////////////////

        case 'form.hidden-fields':

          // default data
          $fields = false;

          // extract $params
          extract( $params );

          // build data
          if ( $fields ) {
            foreach ( $fields as $i => $field ) {
              // default data
              $hidden_name = $hidden_value = false;
              // extract $params
              extract( $field );
              if ( $hidden_name && $hidden_value ) {
                $html .= '<div class="form__field form__field--hidden">';
                  $html .= '<input type="text" name="' . $hidden_name . '" value="' . $hidden_value . '" readonly>';
                $html .= '</div>';
              }
            }
          }

          break;

        //////////////////////////////////////////////////////////
        ////  Form | Rows
        //////////////////////////////////////////////////////////

        case 'form.rows':

          // default data
          $rows = false;

          // extract $params
          extract( $params );

          // build data
          if ( $rows ) {

              foreach ( $rows as $i => $row ) {
                $i++;
                $html .= '<div class="form__row" data-row-index="' . ( $i > 10 ? $i : '0' . $i ) . '">';

                  // default data
                  $form_fields = false;

                  // extract $row
                  extract( $row );

                  foreach ( $form_fields as $j => $item ) {

                    // default data
                    $j++;
                    $type = $name = $value = $label = $placeholder = $required = false;

                    // extract $item
                    extract( $item );

                    if ( $name && $type ) {
                      $html .= '<div class="form__field" data-field-index="' . ( $j > 10 ? $j : '0' . $j ) . '">';
                        if ( $label ) {
                          $html .= '<label>' . $label . '<label>';
                        }
                        switch ( $type ) {
                          case 'email':
                          case 'text':
                          case 'phone':
                            $html .= '<input
                              type="' . $type . '"

                              name="' . $name . '"
                              class="' . ( $required ? 'required' : '' ) . '"
                              placeholder="' . ( $placeholder ? $placeholder : '' ) . '"
                              value="' . ( $value ? $value : '' ) . '"
                            >';
                            break;
                          case 'textarea':
                            $html .= '<textarea
                              name="' . $name . '"
                              class="' . ( $required ? 'required' : '' ) . '"
                              placeholder="' . ( $placeholder ? $placeholder : '' ) . '"
                              >' . ( $value ? $value : '' ) . '</textarea>';
                            break;
                        }
                        if ( $required ) {
                          $html .= '<div class="form__error">This field cannot be blank!</div>';
                        }
                      $html .= '</div>';
                    }

                  }

                $html .= '</div>';
              }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Form | Fields
        //////////////////////////////////////////////////////////

        case 'form.fields':

          /*
            $fields = [
              [
                [
                  'label' => '',
                  'name' => '_replyto',
                  'placeholder' => 'Enter your email address',
                  'readonly' => false,
                  'required' => true,
                  'type' => 'email',
                  'value' => '',
                  'error_message' => 'Please enter a valid email'
                ],
              ],
            ];
            $submit = [
              'classes' => 'button button--fill button--rounded',
              'title' => 'Connect',
            ];
          */

          // default data
          $fields = $submit = false;

          // deconstruct $params
          extract( $params );

          // build form fields
          if ( !empty( $fields ) ) {
            foreach( $fields as $i => $row ) {

              // form row
              $html .= '<div class="form__row" data-form-row-index="' . $i . '">';

                if ( !empty( $row ) ) {
                  foreach( $row as $j => $field ) {

                    // default data
                    $hidden = $label = $name = $placeholder = $readonly = $required = $type = $value = false;
                    $error_message = 'Invalid content!';

                    // deconstruct $field
                    extract( $field );

                    // form field
                    $html .= '<div class="form__field form__field--' . $type . '" data-form-field-index="' . $j . '" ' . ( $hidden ? 'hidden' : '' ) . '>';

                      if ( $label ) {
                        $html .= '<label class="form__label">' . $label . '</label>';
                      }

                      switch ( $type ) {
                        case 'email':
                        case 'text':
                          $html .= '<input
                            class="' . ( $required ? 'required' : '' ) . '"
                            type="' . $type . '"
                            name="' . $name . '"
                            placeholder="' . $placeholder . '"
                            value="' . $value . '"
                            ' . ( $readonly ? 'readonly' : '' ) . '
                          />';
                          break;
                      }

                      if ( $required ) {
                        $html .= '<div class="form__error">' . $error_message . '</div>';
                      }

                    $html .= '</div>';

                  }
                }

              $html .= '</div>';

            }
          }

          // build "rude" honey pot
          $html .= '<div class="form__row form__row--rude">';
            $html .= '<div class="form__field form__field--rude">';
              $html .= '<label class="form__label">Rude</label>';
              $html .= '<input class="rude" type="text" name="rude" tabindex="-1">';
            $html .= '</div>';
          $html .= '</div>';

          if ( $submit ) {

            // default data
            $classes = $title = false;

            // deconstruct $submit
            extract( $submit );

            if ( $title ) {
              $html .= '<div class="form__row form__row--action">';
                $html .= '<button class="form__button' . ( $classes ? ' ' . $classes : '' ) . '" type="submit">' . $title . '</button>';
              $html .= '</div>';
            }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Hero | Media
        //////////////////////////////////////////////////////////

        case 'hero.background':

          // extract / deconstruct params
          extract( $params );

          if ( $image ) {

            $html .= '<div class="hero__background background" data-appearance="' . $appearance . '">';

              $html .= '<div class="background__image">';
                $html .= $this->render_lazyload_image( [ 'background' => true, 'image' => $image ] );
              $html .= '</div>';

              if ( 'video' == $type ) {
                $html .= '<div class="background__video">';
                  $html .= $this->render_lazyload_video( [ 'background' => true, 'video' => $video ] );
                $html .= '</div>';
              }

              $html .= '<div class="background__overlay"></div>';

            $html .= '</div>';

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Call to Action | Circle
        //////////////////////////////////////////////////////////

        case 'form.newsletter':

          // default data
          $heading = $message = $form = false;

          // deconstruct $params
          extract( $params );

          // get data
          if ( $heading ) {
            $html .= '<h2 class="newsletter__heading heading">' . $heading . '</h2>';
          }
          if ( $message ) {
            $html .= '<div class="newsletter__message rte">' . $message . '</div>';
          }
          if ( $form ) {

            // default data
            $url = $form_rows = $form_fields = $submit = false;

            // get data
            if ( get_field( 'url', $form ) ) {
              $url = get_field( 'url', $form );
            }
            if ( get_field( 'form_rows', $form ) ) {
              $form_rows = get_field( 'form_rows', $form );
            }
            if ( get_field( 'submit', $form ) ) {
              $submit = get_field( 'submit', $form );
            }

            // print data
            if ( $url ) {
              $html .= '<form class="form form--newsletter js--validate-me" action="' . $url . '" method="post">';
                if ( !empty( $form_rows ) ) {
                  $html .=  '<div class="form__main">';
                    foreach( $form_rows as $i => $row ) {
                      $html .= '<div class="form__row" data-row-index="' . $i . '">';

                        // default data
                        $form_fields = false;

                        debug_this( $row );

                        // deconstruct $row
                        extract( $row );

                        debug_this( $form_fields );

                        if ( !empty( $form_fields ) ) {
                          foreach( $form_rows as $j => $field ) {
                            $html .= '<div class="form__field">';
                              debug_this( $field );
                            $html .= '</div>';
                          }
                        }

                      $html .= '</div>';
                    }
                }
              $html .= '</form>';
            }

          }

          break;

        //////////////////////////////////////////////////////////
        ////  Menu | Link
        //////////////////////////////////////////////////////////

        case 'menu.link':

          // default data
          $current_post_id = false;
          $target = '_self';
          $link = false;

          // extract / deconstruct $params
          extract( $params );

          if ( $link && !empty( $link ) ) {

            // default data
            $title = $type = $internal = $external = false;
            $link_id = $link_type = false;
            $is_active = false;

            // extract / deconstruct $link
            extract( $link );

            // get link data
            switch ( $type ) {
              case 'external':
                if ( $external ) {
                  $link = $external;
                  $target = '_blank';
                }
                break;
              case 'internal':
                if ( $internal ) {
                  $link_id = $internal->ID;
                  $link_type = $internal->post_type;
                  if ( $current_post_id == $link_id ) {
                    $is_active = true;
                  }
                  if ( 'attachment' == $link_type ) {
                    $link = $internal->guid;
                    $target = '_blank';
                  } else {
                    $link = get_permalink( $link_id );
                  }
                }
                break;
            }

            // conditionally built html
            if ( $link && $title ) {
              $html .= '<li class="menu__item' . ( $is_active ? ' active' : '' ) .'" data-link-type="' . $link_type . '" data-current-post-id="' . $current_post_id . '" data-link-id="' . $link_id . '">';
                $html .= '<a href="' . $link . '" target="' . $target . '">';
                  $html .= $title;
                $html .= '</a>';
              $html .= '</li>';
            }

          }

          break;

      }
    }

    return $html;

  }

  // ---------------------------------------- Background
  public function render_background( $params = [], $block_name = 'not-set' ) {

    $defaults = [
      'type' => 'image',
      'appearance' => 'full-height',
      'html' => '',
      'video' => false,
      'vimeo' => false,
      'image' => [],
    ];

    extract( array_merge( $defaults, $params ) );

    $html .= '<div
      class="' . $block_name . '__background background"
      data-appearance="' . $appearance  . '"
      data-enable-resize="true"
      data-resize-desktop="true"
      data-resize-mobile="true"
      data-resize-offset-header="false"
      data-type="' . $type  . '"
    >';
      $html .= '<div class="background__overlay"></div>';
      switch( $type ) {
        case 'image':
          $html .= $this->render_lazyload_image( [ 'background' => true, 'duration' => 650, 'image' => $image ] );
          break;
        case 'video':
          $html .= $this->render_lazyload_video( [ 'background' => true, 'duration' => 650, 'video' => $video ] );
          break;
        case 'vimeo':
          $html .= $this->render_lazyload_iframe( [ 'background' => true, 'duration' => 650, 'video_id' => $vimeo ] );
          break;
      }
    $html .= '</div>';

    return $html;

  }

  // ---------------------------------------- Content
  public function render_content( $params = [], $block_name = 'not-set' ) {

      $defaults = [
        'cta' => [],
        'heading' => '',
        'html' => '',
        'message' => '',
      ];

      extract( array_merge( $defaults, $params ) );

      $heading_tag = ( strpos( $block_name, 'hero' ) !== false ) ? 'h1' : 'h2';

      if ( $cta || $heading || $message ) {
        $html .= '<div class="' . $block_name . '__content">';
          $html .= $heading ? '<' . $heading_tag .' class="' . $block_name . '__heading heading">' . $heading . '</' . $heading_tag .'>' : '';
          $html .= $message ? '<div class="' . $block_name . '__message message"><p>' . $message . '</p></div>' : '';
          $html .= $cta ? '<div class="' . $block_name . '__cta">' . $this->render_cta( $cta ) . '</div>' : '';
        $html .= '</div>';
      }

      return $html;

  }

  // ---------------------------------------- Call to Action
  public function render_cta( $params = [] ) {

    $defaults = [
      'appearance' => 'not-set',
      'colour' => 'not-set',
      'html' => '',
      'link' => '',
      'link_external' => '',
      'link_internal' => '',
      'target' => '_self',
      'title' => '',
      'type' => '',
    ];

    extract( array_merge( $defaults, $params ) );

    switch( $type ) {
      case 'external':
        $target = '_blank';
        $link = $link_external ? $link_external : '';
        break;
      case 'internal':
        $link = $link_internal ? $link_internal : '';
        break;
    }

    if ( $title && $link ) {
      $html .= '<div class="cta cta--' . $appearance . '" data-colour-theme="' . $colour . '">';
        $html .= '<a class="cta__link" href="' . $link . '" target="' . $target . '"><span class="cta__title">' . $title . '</span></a>';
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Lazyload iFrame
  public function render_lazyload_iframe( $params = [] ) {

    $defaults = [
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'preload' => false,
      'aspect_ratio' => '16-9',
      'video_id' => '', // 160730254, 163590531, 221632885
      'video_source' => 'vimeo'
    ];

    extract( array_merge( $defaults, $params ) );

    $video_classes = 'lazyload lazyload-item lazyload-item--iframe';
    $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $video_classes .= $preload ? ' lazypreload' : '';
    $video_classes .= $classes ? ' ' . $classes : '';

    $video_source_url = ( 'vimeo' == $video_source ) ? 'https://player.vimeo.com/video/' : 'https://www.youtube.com/embed/';
    $video_source_url .= $video_id;
    $video_source_url .= ( $background ) ? '?autoplay=1&loop=1&autopause=0&muted=1&background=1' : '?autoplay=0&loop=1&autopause=0&muted=0&background=0';

    if ( $video_source_url && $video_id ) {

      $html = '
        <iframe
          class="' . $video_classes . '"
          data-aspect-ratio="' . $aspect_ratio . '"
          data-src="' . $video_source_url . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_lazyload_image( $params = [] ) {

    $defaults = [
      'alt_text' => '',
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'preload' => false,
      'image' => [],
      'image_srcset' => '',
      'is_svg' => false,
      'custom_sizes_title' => $this->custom_image_title,
      'custom_sizes' => $this->custom_image_sizes,
    ];

    extract( array_merge( $defaults, $params ) );

    $image_classes = 'lazyload lazyload-item lazyload-item--image';
    $image_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $image_classes .= $preload ? ' lazypreload' : '';
    $image_classes .= $classes ? ' ' . $classes : '';

    $image_full_source = isset($image['url']) ? $image['url'] : false;
    $image_placeholder_source = isset($image['sizes'][$custom_sizes_title . '-10']) ? $image['sizes'][$custom_sizes_title . '-10'] : false;
    $image_width = isset($image['width']) ? $image['width'] : '';
    $image_height = isset($image['height']) ? $image['height'] : '';

    $is_svg = ( isset($image['subtype']) && 'svg+xml' == $image['subtype'] ) ? true : false;

    foreach ( $custom_sizes as $i => $size ) {
      if ( 0 == $i ) {
        $image_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
      } else {
        $image_srcset .= ', ' . $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
      }
    }

    if ( $image_full_source ) {
      if ( $background ) {

        $html = '<div
          class="' . $image_classes . '"
          data-bg="' . $image_full_source . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"';
        $html .= $image_placeholder_source ? ' style="background-url(' . $image_placeholder_source . ')"' : '';
        $html .= '></div>';

       } else {

        if ( $is_svg ) {
          $html = '<img src="' . $image_full_source . '"';
            $html .= $image_width ? ' width="' . $image_width . '"' : '';
            $html .= $image_height ? ' height="' . $image_height . '"' : '';
            $html .= $alt_text ? ' alt="' . $alt_text . '"' : '';
            $html .= ' loading="lazy"';
          $html .= ' />';
        } else {
          $html = '<img
            class="' . $image_classes . '"
            data-src="' . $image_full_source . '"
            data-transition-delay="' . $delay . '"
            data-transition-duration="' . $duration . '"';
          $html .= $image_width ? ' width="' . $image_width . '"' : '';
          $html .= $image_height ? ' height="' . $image_height . '"' : '';
          $html .= $image_srcset ? ' data-sizes="auto" data-srcset="' . $image_srcset . '"' : '';
          $html .= $image_placeholder_source ? ' src="' . $image_placeholder_source . '"' : '';
          $html .= $alt_text ? ' alt="' . $alt_text . '"' : '';
          $html .= ' />';
        }

      }
    }

    return $html;

  }

  // ---------------------------------------- Lazyload Video
  public function render_lazyload_video( $params = [] ) {

    $defaults = [
      'background' => false,
      'classes' => '',
      'delay' => 0,
      'duration' => 750,
      'html' => '',
      'mime_type' => '',
      'preload' => false,
      'video' => [],
      'video_url' => '',
    ];

    extract( array_merge( $defaults, $params ) );

    $video_classes = 'lazyload lazyload-item lazyload-item--video';
    $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
    $video_classes .= $preload ? ' lazypreload' : '';
    $video_classes .= $classes ? ' ' . $classes : '';

    $video_url = isset($video['url']) ? $video['url'] : false;
    $mime_type = isset($video['mime_type']) ? $video['mime_type'] : false;

    if ( $video_url && $mime_type ) {
      $html = '<video
        class="' . $video_classes . '"
        src="' . $video_url . '"
        data-transition-delay="' . $delay . '"
        data-transition-duration="' . $duration . '"
        preload="none"
        muted=""
        data-autoplay=""
        data-poster=""
        loop
        playsinline
        muted
      >';
        $html .= '<source src="' . $video_url . '" type="' . $mime_type . '">';
      $html .= '</video>';
    }

    return $html;

  }

}
