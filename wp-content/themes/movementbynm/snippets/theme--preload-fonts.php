<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$VP = new DaymarkerDigital();
$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_dir = $DD->theme_directory();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$fonts = array(
  "Telegraf-Bold.woff2",
  "Telegraf-Regular.woff2",
  "Telegraf-UltraLight.woff2",
);

foreach ( $fonts as $index => $value ) {
  $font_src = $assets_dir . '/fonts/' . $value;
  echo '<link rel="preload" href="' . $font_src . '" as="font" type="font/woff2" crossorigin>';
}

?>
