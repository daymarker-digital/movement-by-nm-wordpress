<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'image-glider';

if ( have_rows( 'glider' ) ) {
  echo '<section class="section section--' . $block_name . ' ' . $block_name . '">';
    echo '<div class="glide glide--' . $block_name . ' js--glider" id="' . $block_name . '--glider--' . ( $row_index > 10 ? $row_index : '0' . $row_index ) . '">';
      echo '<div class="glide__track" data-glide-el="track">';
        echo '<div class="glide__slides">';

          while ( have_rows( 'glider' ) ) {

            // init data
            the_row();

            // default data
            $heading = $image = $message = false;

            // get data
            if ( get_sub_field( 'heading' ) ) {
              $heading = get_sub_field( 'heading' );
            }
            if ( get_sub_field( 'image' ) ) {
              $image = get_sub_field( 'image' );
            }
            if ( get_sub_field( 'message' ) ) {
              $message = get_sub_field( 'message' );
            }

            // print data
            if ( $image ) {
              echo '<div class="glide__slide">';
                echo '<div class="' . $block_name . '__item">';
                  echo '<div class="' . $block_name . '__image">' . $Templates->render_lazyload_image( [ 'image' => $image ] ) . '</div>';
                  if ( $heading || $message ) {
                    echo '<div class="' . $block_name . '__content">';
                      if ( $heading ) {
                        echo '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>';
                      }
                      if ( $message ) {
                        echo '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
                      }
                    echo '</div>';
                  }
                echo '</div>';
              echo '</div>';
            }

          }

        echo '</div>';
      echo '</div>';
    echo '</div>';
  echo '</section>';
}

?>
