<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'faqs';

if ( have_rows( 'faqs' ) ) {
  while( have_rows( 'faqs' ) ) {

    // init data
    the_row();

    // default data
    $faqs_list = $heading = $message = false;

    // get data
    if ( get_sub_field( 'faqs_list' ) ) {
      $faqs_list = get_sub_field( 'faqs_list' );
    }
    if ( get_sub_field( 'heading' ) ) {
      $heading = get_sub_field( 'heading' );
    }
    if ( get_sub_field( 'message' ) ) {
      $message = get_sub_field( 'message' );
    }

    if ( $faqs_list || $heading || $message ) {

      echo '<section class="section section--' . $block_name  . ' ' . $block_name  . '">';
        echo '<div class="container"><div class="row"><div class="col-12 col-sm-10 offset-sm-1">';

          if ( $heading ) {
            echo '<h2 class="' . $block_name  . '__heading heading"></h2>';
          }

          if ( $heading ) {
            echo '<div class="' . $block_name  . '__message message rte">';
              echo $heading;
            echo '</div>';
          }

          if ( $faqs_list ) {
            echo '<ul class="' . $block_name  . '__list">';
            foreach ( $faqs_list as $i => $faq ) {

              // default data
              $answer = $question = false;

              // extract $faq object
              extract( $faq );

              if ( $answer && $question ) {
                echo '<li class="' . $block_name  . '__item">';
                  echo '<div class="' . $block_name  . '__question">' . $question . '</div>';
                  echo '<div class="' . $block_name  . '__answer rte">' . $answer . '</div>';
                echo '</li>';
              }

            }
            echo '</ul>';
          }

        echo '</div></div></div>';
      echo '</section>';

    }

  }
}

?>
