<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

echo '<header id="header" class="header">';

  if ( have_rows( 'announcements', 'options' ) ) {
    while ( have_rows( 'announcements', 'options' ) ) {

      // init data
      the_row();

      // default data
      $enable = $items = false;

      if ( get_sub_field( 'enable' ) ) {
        $enable = get_sub_field( 'enable' );
      }
      if ( get_sub_field( 'list' ) ) {
        $items = get_sub_field( 'list' );
      }

      if ( $enable && !empty( $items ) ) {
        echo '<div class="header__announcements announcements">';

        if ( 1 == count( $items ) ) {
          echo '<div class="announcements__item rte">';
            if ( isset($items[0]['item']) && !empty($items[0]['item']) ) {
              echo $items[0]['item'];
            }
          echo '</div>';
        } else {
          echo '<div class="glide glide--announcements js--glider" id="announcements--glider">';
            echo '<div class="glide__track" data-glide-el="track">';
              echo '<div class="glide__slides">';
                foreach( $items as $i => $item ) {
                  if ( isset( $item['item'] ) && !empty( $item['item'] ) ) {
                    echo '<div class="glide__slide">';
                      echo '<div class="announcements__item rte">';
                        echo $item['item'];
                      echo '</div>';
                    echo '</div>';
                  }
                }
              echo '</div>';
            echo '</div>';
          echo '</div>';
        }

        echo '</div>';
      }

    }
  }

  echo '<div class="header__main">';

    if ( have_rows( 'header', 'options' ) ) {
      while ( have_rows( 'header', 'options' ) ) {

        // init data
        the_row();

        // default data
        $links = $logo = false;

        if ( get_sub_field( 'logo' ) ) {
          $logo = get_sub_field( 'logo' );
        }
        if ( get_sub_field( 'navigation' ) ) {
          $links = get_sub_field( 'navigation' );
        }

        if ( $logo ) {
          echo '<div class="header__brand">';
            echo '<a href="' . $home . '">' . $Templates->render_lazyload_image( [ 'image' => $logo ] ) . '</a>';
          echo '</div>';
        }

        if ( $links && !empty( $links ) ) {
          echo '<nav class="header__menu menu menu--main">';
            echo '<ul class="menu__list">';
              foreach( $links as $i => $link ) {
                echo $Templates->render( 'menu.link', [ 'link' => $link, 'current_post_id' => $theme_post_id ] );
              }
            echo '</ul>';
          echo '</nav>';
        }

      }
    }

    echo '<button class="header__burger js--toggle-push-navigation-right" type="button">';
      echo '<img src="' . $assets_dir . '/img/NM--web-asset--burger.svg" alt="Burger" />';
    echo '</button>';

  echo '</div>';

echo '</header>';

?>




