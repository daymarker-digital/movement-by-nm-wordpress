<?php

  /**
  *
  *	Filename: mobile-menu.php
  *
  */

  // ---------------------------------------- Data
  $THEME = new CustomTheme();
  $id = get_the_ID() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'mobile-menu';
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  $template_data = get_field( 'mobile_menu', 'options' ) ?: [];
  $cta = $template_data['cta'] ?? [];
  $wp_menu_handle = $template_data['wp_menu']['value'] ?? 'not-set';
  $wp_menu_name = $template_data['wp_menu']['label'] ?? '';


?>

<div class="<?= $template; ?>" id="<?= $template_id; ?>">
  <div class="<?= $template; ?>__main">

    <div class="<?= $template; ?>__brand">
      <a href="<?= $THEME->get_theme_directory('home'); ?>" target="_self" title="Home">
        <?= $THEME->render_svg([ 'type' => 'brand.monogram' ]); ?>
      </a>
    </div>

    <?php if ( $cta || $wp_menu_name ) : ?>
      <nav class="<?= $template; ?>__navigation navigation navigation--<?= $wp_menu_handle; ?>">
        <?= $THEME->render_navigation_wp([ 'current_id' => $id, 'menu_title' => $wp_menu_name ]); ?>
        <?= $THEME->render_cta([ 'classes' => 'navigation__cta', 'cta' => $cta ]); ?>
      </nav>
    <?php endif; ?>

  </div>
</div>
