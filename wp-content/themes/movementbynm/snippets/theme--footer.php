<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'footer';

echo '<footer id="footer" class="' . $block_name . '">';

  if ( have_rows( 'footer', 'options' ) ) {
    while( have_rows( 'footer', 'options' ) ) {

      // init data
      the_row();

      // default data
      $logo = false;

      if ( get_sub_field( 'logo' ) ) {
        $logo = get_sub_field( 'logo' );
      }

      echo '<div class="' . $block_name . '__main">';
        echo '<div class="container"><div class="row"><div class="col-12 col-sm-8 offset-sm-2 col-md-12 offset-md-0">';


          if ( have_rows( 'navigation' ) ) {

            echo '<div class="' . $block_name . '__navigation">';
              while ( have_rows( 'navigation' ) ) {

                // init data
                the_row();

                // default data
                $heading = false;
                $links = [];

                // get data
                if ( get_sub_field( 'heading' ) ) {
                  $heading = get_sub_field( 'heading' );
                }
                if ( get_sub_field( 'links' ) ) {
                  $links = get_sub_field( 'links' );
                }

                if ( $links && $heading ) {
                  echo '<nav class="menu menu--' . clean_string( $heading ) . '">';
                    echo '<ul class="menu__list">';
                      echo '<li class="menu__item menu__item--heading">' . $heading . '</li>';
                      foreach( $links as $i => $link ) {
                        echo $Templates->render( 'menu.link', [ 'link' => $link, 'current_post_id' => $theme_post_id ] );
                      }
                    echo '</ul>';
                  echo '</nav>';
                }

              }

              echo '<div class="' . $block_name . '__newsletter newsletter newsletter--footer">';

                if ( have_rows( 'leads', 'options' ) ) {
                  while ( have_rows( 'leads', 'options' ) ) {

                    // init data
                    the_row();

                    if ( have_rows( 'footer' ) ) {
                      while ( have_rows( 'footer' ) ) {

                        // init data
                        the_row();

                        // default data
                        $form = false;
                        $heading = 'Updates';

                        if ( get_sub_field( 'heading' ) ) {
                          $heading = get_sub_field( 'heading' );
                        }
                        if ( get_sub_field( 'form' ) ) {
                          $form = get_sub_field( 'form' );
                        }

                        if ( $heading ) {
                          echo '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>';
                        }
                        if ( $form ) {
                          echo '<div class="' . $block_name . '__form">';
                            echo $Templates->render( 'form', [ 'id' => $form->ID, 'class_modifier' => 'footer' ] );
                          echo '</div>';
                        }

                      }
                    }

                  }
                }

              echo '</div>';

            echo '</div>';
          }

          if ( $logo ) {
            echo '<div class="' . $block_name . '__brand">';
              echo '<a href="' . $home . '">' . $Templates->render_lazyload_image( [ 'image' => $logo ] ) . '</a>';
            echo '</div>';
          }

        echo '</div></div></div>';
      echo '</div>';

    }
  }

echo '</footer>';

?>

