<?php

//////////////////////////////////////////////////////////
////  Session
//////////////////////////////////////////////////////////

function Daymarker_session () {
	
	session_start();
	
	$max_session_duration = 3600;

	if ( isset( $_SESSION['session_start_time'] ) ) {
  
		// get time difference
    	$session_duration = time() - $_SESSION['session_start_time'];
    	
    	// compare duration to max duration
		if ( $session_duration > $max_session_duration ) {
			
			// reset session
        	//session_reset();
        	unset ( $_SESSION['session_start_time'] );
        	unset ( $_SESSION['user_location'] );
        	
    	}

	} 
	
	//error_log("checksession:".print_r($_SESSION,true),1,"");
	
	if ( !isset( $_SESSION['session_start_time'] ) ) {
		$_SESSION['session_start_time'] = time();
		$_SESSION['user_location'] = get_user_location_info ();
	}
	
}

// add_action('init', 'Daymarker_session', 1);

?>
