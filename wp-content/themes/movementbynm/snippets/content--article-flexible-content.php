<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

if ( have_rows( 'flex_content' ) ) {
  while ( have_rows( 'flex_content' ) ) {

    the_row();

    // default data
    $row_index = get_row_index();
    $row_layout = get_row_layout();

    switch( $row_layout ) {
      case 'image':

        $caption = get_sub_field( 'caption' );
        $image = get_sub_field( 'content' );

        echo $Templates->render( 'article.image', [ 'caption' => $caption, 'image' => $image, 'index' => $row_index ] );

        break;
      case 'wysiwyg';

        $content = get_sub_field( 'content' );
        $asides = get_sub_field( 'asides' );

        echo $Templates->render( 'article.wysiwyg', [ 'asides' => $asides, 'content' => $content, 'index' => $row_index ] );

        break;
    }

  }
}

?>
