<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

if ( isset( $post ) ) {

  // default data
  $adjacent_posts = [];

  // get data
  $adjacent_posts['prev'] = get_adjacent_post( false, '', true );
  $adjacent_posts['next'] = get_adjacent_post( false, '', false );

  echo '<div class="article__adjacent-posts adjacent-posts">';
    echo '<div class="container"><div class="row"><div class="col-12">';

      echo '<div class="adjacent-posts__hr"></div>';

      echo '<div class="adjacent-posts__content">';
        foreach( $adjacent_posts as $key => $value ) {

          // default data
          $link = false;

          // get data
          if ( $value ) {
            $link = get_permalink( $value->ID );
          }

          echo '<div class="adjacent-posts__button adjacent-posts__button--' . $key . '">';
            if ( $link ) {
              echo '<a href="' . $link . '">' . $key . '</a>';
            } else {
              echo '<span>' . $key . '</span>';
            }
          echo '</div>';

        }
      echo '</div>';

    echo '</div></div></div>';
  echo '</div>';

}

?>
