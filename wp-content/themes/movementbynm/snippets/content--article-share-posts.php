<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

if ( isset( $post ) ) {

  $block_name = 'share-post';
  $permalink = false;
  $encoded_permalink = $encoded_title = false;

  if ( get_the_permalink() ) {
    $encoded_permalink = urlencode( trim(get_the_permalink()) );
  }
  if ( get_the_title() ) {
    $encoded_title = urlencode( trim(get_the_title()) );
  }

  echo '<div class="article__' . $block_name . ' ' . $block_name . '">';
    echo '<div class="container"><div class="row"><div class="col-12">';

      echo '<h3 class="' . $block_name . '__heading heading">share this</h3>';

      echo '<ul class="' . $block_name . '__share-list">';

        //////////////////////////////////////////////////////////
        ////  Facebook | Vars
        //////////////////////////////////////////////////////////

        $app_id = 891339908301062;
        $display = "page";
        $share_url = 'https://www.facebook.com/dialog/share?app_id=' . $app_id . '&display=' . $display . '&href=' . $encoded_permalink . '&redirect_uri=' . $encoded_permalink;
        $icon_url = $assets_dir . '/img/NM--web-asset--icon-facebook--black.svg';

        echo '<li class="' . $block_name . '__share-item ' . $block_name . '__share-item--facebook">';
          echo '<a href="' . $share_url . '" target="_blank">';
            if ( $icon_url ) {
              echo '<img src="' . $icon_url . '" alt="Share on Facebook" />';
            } else {
              echo 'Share on Facebook';
            }
          echo '</a>';
        echo '</li>';

        //////////////////////////////////////////////////////////
        ////  Twitter | Vars
        //////////////////////////////////////////////////////////

        $from_twitter_account = 'movementbynm';
        $share_url = 'https://twitter.com/intent/tweet?url=' . $encoded_permalink . '&via=' . $from_twitter_account;
        $icon_url = $assets_dir . '/img/NM--web-asset--icon-twitter--black.svg';

        echo '<li class="' . $block_name . '__share-item ' . $block_name . '__share-item--twitter">';
          echo '<a href="' . $share_url . '" target="_blank">';
            if ( $icon_url ) {
              echo '<img src="' . $icon_url . '" alt="Share on Twitter" />';
            } else {
              echo 'Share on Twitter';
            }
          echo '</a>';
        echo '</li>';

        //////////////////////////////////////////////////////////
        ////  LinkedIn | Vars
        //////////////////////////////////////////////////////////

        $share_url = 'https://www.linkedin.com/sharing/share-offsite/?url=' . $encoded_permalink;
        $icon_url = $assets_dir . '/img/NM--web-asset--icon-linkedin--black.svg';

        echo '<li class="' . $block_name . '__share-item ' . $block_name . '__share-item--linkedin">';
          echo '<a href="' . $share_url . '" target="_blank">';
            if ( $icon_url ) {
              echo '<img src="' . $icon_url . '" alt="Share on Twitter" />';
            } else {
              echo 'Share on LinkedIn';
            }
          echo '</a>';
        echo '</li>';

      echo '</ul>';

    echo '</div></div></div>';
  echo '</div>';

}

?>
