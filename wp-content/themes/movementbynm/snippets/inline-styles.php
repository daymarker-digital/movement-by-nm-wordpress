<?php
  $typography = get_field( 'typography', 'options' ) ?: [];
  $link_underline_colour = $typography['link_underline_colour'] ?? '#000000';
?>

<!-- Theme Inline Styles -->
<style>

  <?php include( locate_template( './assets/main.css' ) ); ?>

  .block--text p a  {
    border-bottom-color: <?= $link_underline_colour; ?>;
  }

</style>
