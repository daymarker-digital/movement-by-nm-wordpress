<?php

  /**
  *
  *	Filename: header.php
  *
  */

  // ---------------------------------------- Data
  $THEME = new CustomTheme();
  $id = get_the_ID() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'header';
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  $template_data = get_field( 'header', 'options' ) ?: [];
  $cta = $template_data['cta'] ?? [];
  $wp_menu_handle = $template_data['wp_menu']['value'] ?? 'not-set';
  $wp_menu_name = $template_data['wp_menu']['label'] ?? '';
  $colour_theme = get_field( 'header_theme', $id ) ?: 'light';
  if ( is_category() || is_single() || is_404() ) {
    $colour_theme = 'dark';
  }

?>

<header class="<?= $template; ?>" id="<?= $template_id; ?>" data-colour-theme="<?= $colour_theme; ?>">
  <?= $THEME->render_container( 'open', 'col-12', 'container-fluid' ); ?>
    <div class="<?= $template; ?>__layout">

      <div class="<?= $template; ?>__brand">
        <a href="<?= $THEME->get_theme_directory('home'); ?>" target="_self" title="Home">
          <?= $THEME->render_svg([ 'type' => 'brand.logo' ]); ?>
        </a>
      </div>

      <?php if ( $cta || $wp_menu_name ) : ?>
        <nav class="<?= $template; ?>__navigation navigation navigation--<?= $wp_menu_handle; ?> d-none d-lg-inline-flex">
          <?= $THEME->render_navigation_wp([ 'current_id' => $id, 'menu_title' => $wp_menu_name ]); ?>
          <?= $THEME->render_cta([ 'classes' => 'navigation__cta', 'cta' => $cta ]); ?>
        </nav>
      <?php endif; ?>

      <button class="<?= $template; ?>__hamburger hamburger hamburger--squeeze js--mobile-menu-trigger d-inline-flex d-lg-none" type="button">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>

    </div>
  <?= $THEME->render_container( 'closed' ); ?>
</header>
