<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$is_category = is_category();
if ( $is_category ) {
  $category = get_queried_object();
  $cat_id = $category->term_id;
}

echo '<div class="magazine__body">';

  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

  // WP_Query arguments
  $args = array(
    'post_status'            => array( 'publish' ),
    'nopaging'               => false,
    'posts_per_page'         => '7',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'paged'                  => $paged,
  );

  if ( $is_category ) {
    $args['cat'] = [ $cat_id ];
  }

  // The Query
  $query = new WP_Query( $args );

  // The Loop
  if ( $query->have_posts() ) {

    $post_count = 1;

    while ( $query->have_posts() ) {

      // init data
      $query->the_post();

      // print article preview
      echo $Templates->render( 'article.preview', [ 'post_id' => get_the_ID(), 'count' => $post_count, 'type' => 'magazine' ] );

      // increment $post_count
      $post_count++;

    } // endwhile

    if ( $query->max_num_pages > 1 )  {

      $pagination_links = [];
      $max_pages = $query->max_num_pages;
      $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
      $pagination_args = [
        'base' => get_pagenum_link(1) . '%_%',
        'format' => $pagination_format,
        'total' => $max_pages,
        'current' => $paged,
        'aria_current' => false,
        'show_all' => true,
        'end_size' => 1,
        'mid_size' =>2,
        'prev_next' => false,
        'prev_text' => '',
        'next_text' => '',
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => '',
        'before_page_number' => '',
        'after_page_number' => ''
      ];

      $pagination_links = paginate_links( $pagination_args );

      echo '<div class="magazine__pagination pagination">';

        echo '<div class="pagination__button pagination__button--prev">';
          if ( get_previous_posts_link( 'prev' ) ) {
            echo get_previous_posts_link( 'prev' );
          } else {
            echo '<span>prev</span>';
          }
        echo '</div>';

        echo '<div class="pagination__vr"></div>';

        if ( $pagination_links && !empty( $pagination_links ) ) {
          echo '<ul class="pagination__links">';
          foreach( $pagination_links as $i => $link ) {
            echo '<li class="pagination__links-item">' . $link . '</li>';
          }
          echo '</ul>';
        }

        echo '<div class="pagination__vr"></div>';

        echo '<div class="pagination__button pagination__button--next">';
          if ( get_next_posts_link( 'next', $max_pages ) ) {
            echo get_next_posts_link( 'next', $max_pages );
          } else {
            echo '<span>next</span>';
          }
        echo '</div>';

      echo '</div>';

    }

  } // if posts

  // Restore original Post Data
  wp_reset_postdata();

echo '</div>';

?>
