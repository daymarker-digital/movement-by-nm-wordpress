<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

if ( have_rows( 'cta' ) ) {
  while ( have_rows( 'cta' ) ) {

    // init row
    the_row();

    // default data
    $title = $type = $link = false;
    $target = '_self';

    // get data
    if ( get_sub_field( 'title' ) ) {
      $title = get_sub_field( 'title' );
    }
    if ( get_sub_field( 'type' ) ) {
      $type = get_sub_field( 'type' );
    }
    switch ( $type ) {
      case 'external':
        if ( get_sub_field( 'link_external' ) ) {
          $link = get_sub_field( 'link_external' );
        }
        $target = '_blank';
        break;
      case 'internal':
        if ( get_sub_field( 'link_internal' ) ) {
          $link = get_sub_field( 'link_internal' );
          if ( 'attachment' == $link->post_type ) {
            $target = '_blank';
          }
          $link = $link->guid;
        }
        break;
    }

    // print data
    if ( $title && $link ) {
      echo '<div class="cta">';
        echo '<a href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      echo '</div>';
    }

  }
}

?>
