<?php

  /**
  *
  *	Filename: newsletter-form.php
  *
  */

  $THEME = new CustomTheme();

?>


<form
  class="form form--newsletter js--validate-me"
  action="https://formspree.io/f/xyylpgka"
  method="POST"
  enctype="multipart/form-data"
  data-redirect-url="<?= $THEME->get_theme_directory('home'); ?>/thank-you"
>

  <?=
    $THEME->render_form_field([
      'label' => 'Rude',
      'name' => 'rude',
      'type' => 'text'
    ]);
  ?>

  <div class="form__main">
    <strong class="form__heading">Updates</strong>
    <div class="form__section section">
      <?=
        $THEME->render_form_field([
          'name' => '_replyto',
          'placeholder' => 'Email',
          'required' => true,
          'type' => 'email'
         ]);
      ?>
      <button class="form__button button outlined" type="submit">
        <span class="button__icon">
          <?= $THEME->render_svg([ 'type' => 'icon.arrow' ]); ?>
        </span>
      </button>
    </div>
  </div>

  <div class="form__loading">
    <div class="form__spinner"></div>
  </div>

</form>
