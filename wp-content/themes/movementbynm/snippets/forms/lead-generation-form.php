<?php

  /**
  *
  *	Filename: lead-generation-form.php
  *
  */

  $THEME = new CustomTheme();

?>

<form
  class="form form--lead-generation js--validate-me"
  action="https://formspree.io/f/xyylpgka"
  method="POST"
  enctype="multipart/form-data"
  data-redirect-url="<?= $THEME->get_theme_directory('home'); ?>/thank-you"
>

  <?=
    $THEME->render_form_field([
      'label' => 'Rude',
      'name' => 'rude',
      'type' => 'text'
    ]);
  ?>

  <div class="form__main">
    <div class="form__section section">
      <?=
        $THEME->render_form_field([
          'name' => '_name',
          'placeholder' => 'Full Name',
          'required' => true,
          'type' => 'text'
         ]);
      ?>
      <?=
        $THEME->render_form_field([
          'name' => '_phone',
          'placeholder' => 'Phone',
          'required' => false,
          'type' => 'tel'
         ]);
      ?>
      <?=
        $THEME->render_form_field([
          'name' => '_replyto',
          'placeholder' => 'Email',
          'required' => true,
          'type' => 'email'
         ]);
      ?>
      <?=
        $THEME->render_form_field([
          'name' => 'message',
          'placeholder' => 'Your message...',
          'required' => true,
          'type' => 'textarea'
         ]);
      ?>
    </div>
    <button class="form__submit-button button outlined" type="submit">
      <span class="button__title">Submit</span>
    </button>
  </div>

  <div class="form__loading">
    <div class="form__spinner"></div>
  </div>

</form>
