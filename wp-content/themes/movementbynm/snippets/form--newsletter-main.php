<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

?>

<form class="form form--newsletter js--validate-me" action="https://formspree.io/f/xgepqaek" method="POST">

  <div class="form__main">

    <div class="form__row d-none">
      <div class="form__field">
        <input type="text" name="websource" value="Page" readonly>
      </div>
    </div>

    <div class="form__row d-none">
      <div class="form__field">
        <input type="text" name="tags" value="James, Bond, Murphy" readonly>
      </div>
    </div>

    <div class="form__row">
      <div class="form__field form__field--email">
        <input class="required" type="email" name="_replyto" placeholder="Email Address" value="">
        <span class="form__error">Please enter a valid email</span>
      </div>
    </div>

    <div class="form__row form__row--rude">
      <div class="form__field form__field--rude">
        <label class="form__label">Rude</label>
        <input class="rude" type="text" name="rude" tabindex="-1" value="">
      </div>
    </div>

    <div class="form__row form__row--action">
      <button class="form__button button button--fill button--rounded" type="submit">Subscribe</button>
    </div>

  </div>

  <div class="form__success-notification">
    <div class="form__success-notification-message">
      <p>Thanks for your interest!</p>
    </div>
  </div>

</form>
