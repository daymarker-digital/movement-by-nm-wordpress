<?php

//////////////////////////////////////////////////////////
////  Async & Defer Loading
//////////////////////////////////////////////////////////

function add_asyncdefer_attribute($tag, $handle) {
  // if the unique handle/name of the registered script has 'async' in it
  if (strpos($handle, 'async') !== false) {
    // return the tag with the async attribute
    return str_replace( '<script ', '<script async ', $tag );
  }
  // if the unique handle/name of the registered script has 'defer' in it
  else if (strpos($handle, 'defer') !== false) {
    // return the tag with the defer attribute
    return str_replace( '<script ', '<script defer ', $tag );
  }
  // otherwise skip
  else {
    return $tag;
  }
}

add_filter( 'script_loader_tag', 'add_asyncdefer_attribute', 10, 2 );

//////////////////////////////////////////////////////////
////  Enqueue Script & Style
//////////////////////////////////////////////////////////

function  DaymarkerDigital_enqueue_scripts () {

  // NOTE: 'in_footer' will be overruled if wordpress/theme/plugin needs file in header

  if ( $GLOBALS['pagenow'] != 'wp-login.php' && !is_admin() ) {

    // Script & Styles Variables
    $google_maps_api_key = 'AIzaSyBFFpFuchhgBnwP1Mbwx6BGsJIZDFpdtFg';
    $page_with_maps = false;

    // dequeue Gutenberg crap
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' );
    wp_dequeue_style( 'storefront-gutenberg-blocks' );

    // register and enqueue styles
    wp_register_style('css-style', get_template_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ), 'all' );
    wp_enqueue_style('css-style');

    /*
      'jquery' => array(
        'src' => get_template_directory_uri() . '/assets/jQuery.min.js',
        'dependency' => array(),
        'version' => filemtime( get_template_directory() . '/assets/jQuery.min.js' ),
        'in_footer' => true,
      ),
    */

    // array of scripts to be loaded
    $scripts = array(
      'daymarker-digital-async' => array(
        'src' => get_template_directory_uri() . '/assets/DaymarkerDigital.min.js',
        'dependency' => false,
        'version' => filemtime( get_template_directory() . '/assets/DaymarkerDigital.min.js' ),
        'in_footer' => true,
      ),
    );

    // removes default wp version of jQuery and extra bloat
    wp_deregister_script( 'jquery' );
    wp_deregister_script( 'comment-reply' );
    wp_deregister_script( 'wp-embed' );

    // register and enqueue scripts
    foreach ( $scripts as $handle => $value ) {

      wp_register_script( $handle, $value['src'], $value['dependency'], $value['version'], $value['in_footer'] );
      wp_enqueue_script( $handle );

    }

  }

}

add_action( 'wp_enqueue_scripts', 'DaymarkerDigital_enqueue_scripts', 10, 0 );

//////////////////////////////////////////////////////////
////  Conditionally Enqueue Script & Style
//////////////////////////////////////////////////////////

function  DaymarkerDigital_enqueue_scripts_conditionally () {

    if ( is_user_logged_in() || is_admin() ) {
      // wp_register_style('css-wp-admin-dashboard', get_template_directory_uri() . '/css/wp-admin-dashboard.css', array(), filemtime( get_stylesheet_directory() . '/css/wp-admin-dashboard.css' ), 'all' );
    // wp_enqueue_style('css-wp-admin-dashboard');
    }

}

add_action( 'init', 'DaymarkerDigital_enqueue_scripts_conditionally', 10, 0 );

?>
