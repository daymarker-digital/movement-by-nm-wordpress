<?php

//////////////////////////////////////////////////////////
////  Custom Image Size Function
//////////////////////////////////////////////////////////

function DaymarkerDigital_image_sizes() {

  // required
  add_theme_support( 'post-thumbnails' );

  // initialize instance of PoliteDepartment
  $DD = new DaymarkerDigital();

  // get custom sizes
  $custom_sizes = $DD->custom_image_sizes;

  foreach ( $custom_sizes as $index => $size ) {
    $size_title = $DD->custom_image_title;
    $size_title .= '-' . $size;
    add_image_size( $size_title, $size, 9999 );
  }

} // DaymarkerDigital_image_sizes()

add_action( 'after_setup_theme', 'DaymarkerDigital_image_sizes' );

?>
