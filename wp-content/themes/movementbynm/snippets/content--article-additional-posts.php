<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

if ( isset( $post ) ) {

  // default data
  $post_cats = $cat_id = false;
  $cats_count = 0;

  // The Query Args
  $query_args = [
    'post_type' => 'post',
    'post_status' => 'publish',
    'order' => 'DESC',
    'orderby' => 'date',
    'posts_per_page' => '3',
    'post__not_in' => [ $post->ID ]
  ];

  if ( get_the_category() ) {
    $post_cats = get_the_category();
    foreach ( $post_cats as $i => $cat ) {
      if ( 'uncategorized' != $cat->slug ) {
        $cat_id = $cat->term_id;
        $cats_count++;
      }
      if ( $cats_count > 0 ) {
        break;
      }
    }
    if ( $cat_id ) {
      $query_args['category__in'] = [ $cat_id ];
    }
  }

  // The Query
  $the_query = new WP_Query( $query_args );

  // The Loop
  if ( $the_query->have_posts() ) {

    $post_count = 1;

    echo '<div class="article__additional-posts additional-posts"">';
      echo '<div class="container"><div class="row"><div class="col-12">';
        echo '<div class="additional-posts__grid">';

          while ( $the_query->have_posts() ) {

            // init data
            $the_query->the_post();

            // get data
            if ( get_the_ID() ) {

              $id = get_the_ID();
              echo $Templates->render( 'article.preview', [ 'post_id' => $id, 'count' => $post_count, 'type' => 'additional-post' ] );
              $post_count++;

            }

          } // endwhile

        echo '</div>';
      echo '</div></div></div>';
    echo '</div>';

  // reset original post data
  wp_reset_postdata();

  } // endif

}

?>
