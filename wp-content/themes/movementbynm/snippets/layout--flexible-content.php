<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////


if ( have_rows( 'flex_content' ) ) {

  echo '<section class="section section--flexible-content flexible-content">';

    while ( have_rows( 'flex_content' ) ) {

      // init data
      the_row();

      $row_index = get_row_index();
      $row_layout = get_row_layout();

      switch( $row_layout ) {
        case 'image-glider':
          include( locate_template( "./snippets/layout--image-glider.php" ) );
          break;
        case 'image-with-text':
          include( locate_template( "./snippets/layout--image-with-text.php" ) );
          break;
        case 'lead-generator':
          include( locate_template( "./snippets/layout--lead-generator.php" ) );
          break;
        case 'wysiwyg':
          include( locate_template( "./snippets/layout--wysiwyg.php" ) );
          break;
      }

    }

  echo '</section>';

}

?>
