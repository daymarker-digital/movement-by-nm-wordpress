<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

$block_name = 'newsletter-popup';

?>

<?php if ( true ) : ?>

<div class="modal modal--<?php echo $block_name; ?> micromodal-slide" id="modal--<?php echo $block_name; ?>" aria-hidden="true" data-days-saved="90" data-timeout="2500">
  <div class="modal__overlay" tabindex="-1">
    <div class="modal__container" id="modal-container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">

      <div class="modal__main">

        <button class="modal__close modal__button" aria-label="Close modal" type="button" data-micromodal-close>
          <img src="<?php echo $assets_dir; ?>/img/NM--web-asset--close--black.svg" alt="" />
        </button>

        <?php

          $form = $heading = $message = false;

          if ( have_rows( 'leads', 'options' ) ) {
            while ( have_rows( 'leads', 'options' ) ) {

              // init data
              the_row();

              if ( have_rows( 'popup' ) ) {
                while ( have_rows( 'popup' ) ) {

                  // init data
                  the_row();

                  // get data
                  if ( get_sub_field( 'form' ) ) {
                    $form = get_sub_field( 'form' );
                  }
                  if ( get_sub_field( 'heading' ) ) {
                    $heading = get_sub_field( 'heading' );
                  }
                  if ( get_sub_field( 'message' ) ) {
                    $message = get_sub_field( 'message' );
                  }

                  // print data
                  if ( $heading ) {
                    echo '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>';
                  }
                  if ( $message ) {
                    echo '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
                  }
                  if ( $form ) {
                    echo '<div class="' . $block_name . '__form">';
                      echo $Templates->render( 'form', [ 'id' => $form->ID, 'class_modifier' => $block_name ] );
                    echo '</div>';
                  }

                }
              }

            }
          }
        ?>

      </div>

    </div>
  </div>
</div>
<!-- /.modal -->

<?php endif; ?>
