<?php

//////////////////////////////////////////////////////////
////  Register Custom Menus
//////////////////////////////////////////////////////////

function DaymarkerDigital_menus () {

  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' )
    )
  );

}

add_action( 'init', 'DaymarkerDigital_menus' );

//////////////////////////////////////////////////////////
////  Print Menu
//////////////////////////////////////////////////////////

function print_menu ( $menu_title = 'Main Menu', $current_post_id = false ) {

  if ( wp_get_nav_menu_items ( $menu_title ) ) {

    $menu_items = wp_get_nav_menu_items ( $menu_title );

    // loop through menu object here, only output '<li>' elements
    foreach ( $menu_items as $index => $item ) {

      // default data
      $id = $link = $title = $type = false;
      $link_classes = '';

      $id = $item->object_id;
      $link = $item->url;
      $title = $item->title;
      $type = $item->type;
      $target = '_self';

      if ( $id == $current_post_id ) {
        $link_classes = 'active';
      }
      if ( 'custom' == $type ) {
        $target = '_blank';
      }

      echo '<li
        class="menu__item' . ( $link_classes ? ' ' . $link_classes : '' ) . '"
        data-link-type="' . $type . '"
        data-current-post-id="' . $current_post_id . '"
        data-link-id="' . $id . '"
      >';
        echo '<a href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      echo '</li>';

    }

  }

}

?>
