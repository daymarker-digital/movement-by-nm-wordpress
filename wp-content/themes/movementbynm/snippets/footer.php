<?php

  /**
  *
  *	Filename: footer.php
  *
  */

  // ---------------------------------------- Data
  $THEME = new CustomTheme();
  $id = get_the_ID() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'footer';
  $template_id = $THEME->get_unique_id([ 'prefix' => $template . '--' ]);

  $template_data = get_field( 'footer', 'options' ) ?: [];
  $menus = $template_data['menus'] ?? [];

?>

<footer class="<?= $template; ?> fixed" id="<?= $template_id; ?>">
  <?= $THEME->render_container( 'open' ); ?>
    <div class="<?= $template; ?>__layout">

      <?php if ( $menus ) : ?>
        <div class="<?= $template; ?>__menus">
          <?php
            foreach ( $menus as $i => $menu ) {

              $title = $menu['title'] ?? '';
              $wp_menu_handle = $menu['wp_menu']['value'] ?? 'not-set';
              $wp_menu_name = $menu['wp_menu']['label'] ?? '';

              if ( $wp_menu_name ) {
                echo '<nav class="' . $template . '__navigation navigation navigation--' . $wp_menu_handle . '">';
                  echo $title ? '<strong class="navigation__title">' . $title . '</strong>' : '';
                  echo $THEME->render_navigation_wp([ 'current_id' => $id, 'menu_title' => $wp_menu_name ]);
                echo '</nav>';
              }

            }
          ?>
        </div>
      <?php endif; ?>

      <div class="<?= $template; ?>__newsletter">
        <?php include( locate_template( './snippets/forms/newsletter-form.php' ) ); ?>
      </div>

      <div class="<?= $template; ?>__brand">
        <a href="<?= $THEME->get_theme_directory('home'); ?>" target="_self" title="Home">
          <?= $THEME->render_svg([ 'type' => 'brand.monogram-wide' ]); ?>
        </a>
      </div>

    </div>
  <?= $THEME->render_container( 'closed' ); ?>
</footer>
