<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();
$block_name = 'hero';

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

// default data
$heading = get_the_title();
$background = $content = $enable = $message = false;

// get data
if ( have_rows( 'hero' ) ) {
  while ( have_rows( 'hero' ) ) {

    // init data
    the_row();

    $enable = get_sub_field( 'enable' ) ?: false;
    $content = get_sub_field( 'content' ) ?: [];
    $background = get_sub_field( 'background' ) ?: [];

  }
}

// print data
if ( $enable ) {

  echo '<section class="section section--hero hero' . ( is_front_page() ? ' hero--index' : '' ) . '">';

    echo $Templates->render_background( $background, $block_name );

    echo '<div class="' . $block_name . '__main">';
      echo is_front_page() ? '<div class="' . $block_name . '__corner-shape"><span id="' . $block_name . '__corner-shape"></span></div>' : '';
      echo $Templates->render_content( $content, $block_name );
    echo '</div>';

  echo '</section>';

}

?>
