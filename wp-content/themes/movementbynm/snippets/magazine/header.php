<?php

  /**
  *
  *	Filename: magazine-header.php
  *
  */

  $THEME = $THEME ?? new CustomTheme();

  // ---------------------------------------- Template Data
  $snippet = 'magazine-header';
  $snippet_class = $snippet . ' block';
  $snippet_id = $THEME->get_unique_id([ 'prefix' => $snippet . '--' ]);

  // ---------------------------------------- ACF Data
  $snippet_data = get_field( 'magazine', 'options' ) ?: [];
  $brand = $snippet_data['brand'] ?? [];
  $navigation = $snippet_data['navigation'] ?? [];

?>

<div class="<?= $snippet; ?> magazine__header">

  <div class="<?= $snippet; ?>__date magazine__date" id="magazine__date"><?= date("h:i:s A"); ?></div>

  <?php if ( $brand ) : ?>
    <div class="<?= $snippet; ?>__brand magazine__brand"><?= $THEME->render_lazyload_image( [ 'image' => $brand ] ); ?></div>
  <?php endif; ?>

  <?php
    if ( !empty($navigation) ) {
      echo '<nav class="' . $snippet . '__navigation navigation">';
        foreach ( $navigation as $i => $item ) {

          $link = $title = '';
          $type = $item['type'] ?? 'category';

          switch( $type ) {
            case 'category': {
              $link_id = $item['link_category'] ?? false;
              $link_object = get_category( $link_id );
              $link = get_category_link( $link_object );
              $title = $link_object->name ?? '';
              break;
            }
            case 'page-post': {
              $link_id = $item['link_page'] ?? false;
              $link_object = get_post( $link_id );
              $link = get_permalink( $link_object );
              $title = $link_object->post_title ?? '';
              break;
            }
          }

          if ( $link && $title ) {
            echo '<div class="navigation__item">';
              echo '<a class="navigation__link" target="_self" href="' . $link . '" title="' . $title . '">' . $title . '</a>';
            echo '</div>';
          }

        }
      echo '</nav>';
    }
  ?>

</div>
