<?php

  /**
  *
  *	Filename: magazine-body.php
  *
  */

  $THEME = $THEME ?? new CustomTheme();

  // ---------------------------------------- Template Data
  $snippet = 'magazine-body';
  $snippet_class = $snippet . ' block';
  $snippet_id = $THEME->get_unique_id([ 'prefix' => $snippet . '--' ]);

  // ---------------------------------------- ACF Data
  $snippet_data = get_field( 'magazine', 'options' ) ?: [];
  $exclude = $snippet_data['exclude'] ?? [];

  // ---------------------------------------- WP Data
  $wp_paged = get_query_var( 'paged' ) ?: 1;
  $wp_post_count = 1;
  $wp_args = [
    'post_status'            => [ 'publish' ],
    'nopaging'               => false,
    'posts_per_page'         => '7',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'paged'                  => $wp_paged,
    'category__not_in'      => $exclude,
  ];
  $wp_query = new WP_Query( $wp_args );

  // ---------------------------------------- LEGACY
  $DD = new DaymarkerDigital();
  $home = $DD->theme_directory('home');
  $assets_dir = $DD->theme_directory('assets');
  $theme_post_id = $DD->theme_info('post_ID');
  $Templates = new DDTemplates();

?>

<div class="magazine__body">

  <?php

    if ( $wp_query->have_posts() ) {

      while ( $wp_query->have_posts() ) {

        // init data
        $wp_query->the_post();

        // print article preview
        echo $Templates->render( 'article.preview', [
          'post_id' => get_the_ID(),
          'count' => $wp_post_count,
          'type' => 'magazine'
        ]);

        // increment $wp_post_count
        $wp_post_count++;

      }

      if ( $wp_query->max_num_pages > 1 )  {

        $max_pages = $wp_query->max_num_pages;
        $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
        $pagination_links = paginate_links([
          'base' => get_pagenum_link(1) . '%_%',
          'format' => $pagination_format,
          'total' => $max_pages,
          'current' => $wp_paged,
          'aria_current' => false,
          'show_all' => true,
          'end_size' => 1,
          'mid_size' =>2,
          'prev_next' => false,
          'prev_text' => '',
          'next_text' => '',
          'type' => 'array',
          'add_args' => false,
          'add_fragment' => '',
          'before_page_number' => '',
          'after_page_number' => ''
        ]) ?: [];

        echo '<div class="magazine__pagination pagination">';

          echo '<div class="pagination__button pagination__button--prev">';
            echo get_previous_posts_link( 'prev' ) ?: '<span>prev</span>';
          echo '</div>';

          echo '<div class="pagination__vr"></div>';

          if ( $pagination_links ) {
            echo '<ul class="pagination__links">';
            foreach( $pagination_links as $i => $link ) {
              echo '<li class="pagination__links-item">' . $link . '</li>';
            }
            echo '</ul>';
          }

          echo '<div class="pagination__vr"></div>';

          echo '<div class="pagination__button pagination__button--next">';
            echo get_next_posts_link( 'next', $max_pages ) ?: '<span>next</span>';
          echo '</div>';

        echo '</div>';

      }

    }

    wp_reset_postdata();

  ?>

</div>
