<?php

  /**
  *
  *   Fifty-Fifty Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'fifty-fifty';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('fifty_fifty') ?: [];
  $cta = $acf_data['cta'] ?? [];
  $heading = $acf_data['heading'] ?? '';
  $image = $acf_data['image'] ?? '';
  $layout = $acf_data['layout'] ?? 'image-left';
  $message = $acf_data['message'] ?? '';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?= $THEME->render_container( 'open', 'col-12' ); ?>
    <div class="<?= $block_name; ?>__layout" data-layout="<?= $layout; ?>">

      <?php if ( $image ) : ?>
        <div class="<?= $block_name; ?>__image">
          <?= $THEME->render_lazyload_image([ 'image' => $image ]); ?>
        </div>
      <?php endif; ?>

      <?php if ( $cta || $heading || $message ) : ?>
        <div class="<?= $block_name; ?>__content">
          <?= $heading ? '<h2 class="' . $block_name . '__heading heading--2">' . $heading . '</h2>' : ''; ?>
          <?= $message ? '<div class="' . $block_name . '__message body-copy">' . $message . '</div>' : ''; ?>
          <?= $THEME->render_cta([ 'classes' => $block_name . '__cta', 'cta' => $cta ]); ?>
        </div>
      <?php endif; ?>

    </div>
  <?= $THEME->render_container( 'closed' ); ?>
</section>
