<?php

  /**
  *
  *   Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'text';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF | Content
  $acf_data = get_field('text') ?: [];
  $acf_heading = $acf_data['heading'] ?? '';
  $acf_message = $acf_data['message'] ?? '';

  // ---------------------------------------- Bootstrap
  $bs_cols = 'col-12 col-lg-10 offset-lg-1';
  $bs_container = 'container';

?>

<?php if ( $acf_heading || $acf_message ) : ?>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

    <?=
      $THEME->render_block_styles([
        'block_id' => $block_id,
        'background' => $background,
        'padding_bottom' => $padding_bottom,
        'padding_top' => $padding_top,
      ]);
    ?>

    <?= $THEME->render_bs_container( 'open', $bs_cols, $bs_container ); ?>
      <div class="<?= $block_name; ?>__main" id="<?= $aos_id; ?>">

        <?php if ( $acf_heading ) : ?>
          <h2 class="<?= $block_name; ?>__heading heading--1"><?= $acf_heading; ?></h2>
        <?php endif; ?>

        <?php if ( $acf_message ) : ?>
          <div class="<?= $block_name; ?>__message body-copy"><?= $acf_message; ?></div>
        <?php endif; ?>

      </div>
    <?= $THEME->render_bs_container( 'closed', $bs_cols, $bs_container ); ?>
  </section>

<?php endif; ?>
