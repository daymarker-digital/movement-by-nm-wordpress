<?php

  /**
  *
  *   Instagram Feed Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'instagram-feed';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $block_data = get_field('instagram_feed') ?: [];
  $message = $block_data['message'] ?? 'Follow us';
  $lightwidget_id = $block_data['lightwidget_id'] ?? '';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?php if ( $message ) : ?>
    <div class="<?= $block_name; ?>__intro">
      <?= $THEME->render_container( 'open' ); ?>
        <div class="<?= $block_name; ?>__content">
          <div class="<?= $block_name; ?>__message heading--3"><?= $message; ?></div>
        </div>
      <?= $THEME->render_container( 'closed' ); ?>
    </div>
  <?php endif; ?>

  <?php if ( $lightwidget_id ) : ?>
    <div class="<?= $block_name; ?>__main acf-updated">
      <!-- LightWidget WIDGET -->
      <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
      <iframe src="//lightwidget.com/widgets/<?= $lightwidget_id; ?>.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
    </div>
  <?php endif; ?>

</section>
