<?php

  /**
  *
  *   Hero Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'hero-block';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('block_hero') ?: [];
  $heading = $acf_data['heading'] ?? '';
  $height = $acf_data['height'] ?? '';
  $background_type = $acf_data['background']['type'] ?? 'not-set';
  $background_image = $acf_data['background']['image'] ?? [];
  $background_embed_id = $acf_data['background']['embed_id'] ?? 'not-set';
  $background_embed_ratio = $acf_data['background']['embed_ratio'] ?? 'not-set';
  $background_embed_source = $acf_data['background']['embed_source'] ?? 'not-set';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-height="<?= $height; ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <div
    class="<?= $block_name; ?>__background"
    data-type="<?= $background_type; ?>"
    data-embed-aspect-id="<?= $background_embed_id; ?>"
    data-embed-aspect-ratio="<?= $background_embed_ratio; ?>"
    data-embed-aspect-source="<?= $background_embed_source; ?>"
  >
    <?=
      $THEME->render_lazyload_image([
        'background' => true,
        'classes' => $block_name . '__background-image',
        'image' => $background_image
      ]);
    ?>
    <?php if ( 'embed' === $background_type ) : ?>
      <?=
        $THEME->render_lazyload_embed([
          'aspect_ratio' => $background_embed_ratio,
          'background' => true,
          'classes' => $block_name . '__background-embed',
          'id' => $background_embed_id,
          'source' => $background_embed_source,
        ]);
      ?>
     <?php endif; ?>
  </div>

  <?php if ( $heading ) : ?>
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_container( 'open', 'col-12 col-lg-10 offset-lg-1' ); ?>
        <h1 class="<?= $block_name; ?>__heading heading--1"><?= strip_tags( $heading, '<a>' );?></h1>
      <?= $THEME->render_container( 'closed' ); ?>
    </div>
  <?php endif; ?>

</section>
