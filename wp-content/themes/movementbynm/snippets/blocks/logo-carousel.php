<?php

  /**
  *
  *   Logo Carousel Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'logo-carousel';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('logo_carousel') ?: [];
  $list = $acf_data['list'] ?: [];
  $glider = count($list) > 1 ? true : false;
  $glider_id = $block_id . '--glider';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?= $THEME->render_container( 'open', 'col-12' ); ?>
    <div class="<?= $block_name; ?>__main">
      <?php

        if ( $list ) {
          echo $glider ? '<div class="glide js--glider" id="' . $glider_id . '" data-glide-style="' . $block_name . '">' : '';
            echo $glider ? '<div class="glide__track" data-glide-el="track">' : '';
              echo $glider ? '<ul class="glide__slides">' : '';

                foreach ( $list as $i => $item ) {

                  $logo = $item['logo'] ?? [];
                  $url = $item['url'] ?? '';

                  if ( $logo ) {
                    echo $glider ? '<li class="glide__slide">' : '';
                      echo '<div class="' . $block_name . '__item">';
                        echo '<div class="' . $block_name . '__logo">';
                          echo $url ? '<a href="' . $url . '" target="_blank">' : '';
                            echo $THEME->render_lazyload_image([ 'image' => $logo ]);
                          echo $url ? '</a>' : '';
                        echo '</div>';
                      echo '</div>';
                    echo $glider ? '</li>' : '';
                  }

                }

              echo $glider ? '</ul>' : '';
            echo $glider ? '</div>' : '';
          echo $glider ? '</div>' : '';
        }

      ?>
    </div>
  <?= $THEME->render_container( 'closed' ); ?>
</section>
