<?php

  /**
  *
  *   Text Feature Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'text-feature';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('text_feature') ?: [];
  $heading = $acf_data['heading'] ?? '';
  $messages = $acf_data['messages'] ?? '';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?= $THEME->render_container( 'open', 'col-12 col-lg-10 offset-lg-1' ); ?>
    <div class="<?= $block_name; ?>__layout">

      <?php if ( $heading ) : ?>
        <h2 class="<?= $block_name; ?>__heading heading--2"><?= $heading;?></h2>
      <?php endif; ?>

      <?php if ( $messages ) : ?>
        <div class="<?= $block_name; ?>__messages-list">
          <?php
            foreach ( $messages as $i => $item ) {

              $subheading = $item['subheading'] ?? '';
              $message = $item['message'] ?? '';

              if ( $message || $subheading ) {
                echo '<div class="' . $block_name . '__messages-item">';
                  echo $subheading ? '<strong class="' . $block_name . '__subheading heading--5">' . $subheading . '</strong>' : '';
                  echo $message ? '<div class="' . $block_name . '__message body-copy">' . $message . '</div>' : '';
                echo '</div>';
              }

            }
          ?>
        </div>
      <?php endif; ?>

    </div>
  <?= $THEME->render_container( 'closed' ); ?>
</section>
