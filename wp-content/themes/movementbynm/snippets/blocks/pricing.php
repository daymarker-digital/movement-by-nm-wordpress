<?php

  /**
  *
  *   Pricing Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'pricing';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('pricing') ?: [];
  $background_image = $acf_data['image'] ?: [];
  $cta = $acf_data['cta'] ?: [];
  $options = $acf_data['options'] ?: [];

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?=
    $THEME->render_lazyload_image([
      'background' => true,
      'classes' => $block_name . '__background',
      'image' => $background_image
    ]);
  ?>

  <div class="<?= $block_name; ?>__overlay"></div>

  <div class="<?= $block_name; ?>__main">
    <?= $THEME->render_container( 'open', 'col-12' ); ?>
      <div class="<?= $block_name; ?>__layout">

        <?php
          if ( $options ) {
            echo '<div class="' . $block_name . '__options">';
              foreach ( $options as $i => $item ) {

                $incentive = $item['incentive'] ?? '';
                $name = $item['name'] ?? '';
                $price = $item['price'] ?? '';
                $promo = $item['promo'] ?? '';

                if ( $name && $price ) {
                  echo '<div
                    class="' . $block_name . '__option"
                    data-count="' . $i + 1 . '"
                    data-index="' . $i . '"
                    ' . ( $incentive ? 'data-incentive' : '' ) . '
                  >';
                    echo '<div class="' . $block_name . '__option-frame">';
                      echo $incentive ? '<div class="' . $block_name . '__option-incentive"><span>' . $incentive . '</span></div>' : '';
                      echo '<div class="' . $block_name . '__option-name">' . $name . '</div>';
                      echo '<strong class="' . $block_name . '__option-price">$' . $price . '</strong>';
                    echo '</div>';
                    echo $promo ? '<div class="' . $block_name . '__option-promo">' . $promo . '</div>' : '';
                  echo '</div>';
                }

              }
            echo '</div>';
          }
        ?>

        <?= $THEME->render_cta([ 'classes' => $block_name . '__cta', 'cta' => $cta ]); ?>

      </div>
    <?= $THEME->render_container( 'closed' ); ?>
  </div>

</section>
