<?php

  /**
  *
  *   Lead Generation Template
  *
  */

  $THEME = new CustomTheme();

  // Create class attribute allowing for custom "className" and "align" values.

  // ---------------------------------------- Template Data
  $block_name = 'nu-lead-generation';
  $block_name_class = $block_name . ' block';
  $block_id = $THEME->get_unique_id([ 'prefix' => $block_name . '--' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $block_data = get_field('lead_generation') ?: [];
  $form_action = $block_data['form']['action'] ?? '';
  $form_submit_button_title = $block_data['form']['submit_button_title'] ?? 'Submit';
  $form_hidden_fields = $block_data['form']['hidden_fields'] ?? [];
  $heading = $block_data['heading'] ?? '';
  $image = $block_data['image'] ?? '';
  $message = $block_data['message'] ?? '';
  $test = 'yes';

?>

<section class="<?= esc_attr( $block_name_class ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?php if ( $image ) : ?>
    <div class="<?= $block_name; ?>__media">
      <?= $THEME->render_lazyload_image([ 'background' => true, 'image' => $image ]); ?>
    </div>
  <?php endif; ?>

  <?php if ( $heading || $message ) : ?>
    <div class="<?= $block_name; ?>__main">

      <?php if ( $heading ) : ?>
        <h2 class="<?= $block_name; ?>__heading heading--2"><?= $heading; ?></h2>
      <?php endif; ?>
      <?php if ( $message ) : ?>
        <div class="<?= $block_name; ?>__message body-copy"><?= $message; ?></div>
      <?php endif; ?>

      <form
        class="<?= $block_name; ?>__form form form--lead-generation js--validate-me"
        action="<?= $form_action; ?>"
        method="POST"
        enctype="multipart/form-data"
        data-redirect-url="<?= $THEME->get_theme_directory('home'); ?>/thank-you"
      >

        <div class="form__main">

          <div class="form__field-group form__field-group--hidden">
            <?php if ( !empty($form_hidden_fields) ) : ?>
              <?php foreach ( $form_hidden_fields as $i => $item ) : ?>
                <?=
                  $THEME->render_form_input([
                    'name' => $item['name'] ?? 'name-not-set',
                    'readonly' => true,
                    'type' => 'text',
                    'value' => $item['value'] ?? 'value-not-set',
                  ]);
                ?>
              <?php endforeach; ?>
            <?php endif; ?>
            <?=
              $THEME->render_form_input([
                'name' => 'site_url',
                'readonly' => true,
                'type' => 'text',
                'value' => get_site_url(),
              ]);
            ?>
            <?=
              $THEME->render_form_input([
                'name' => 'websource',
                'readonly' => true,
                'type' => 'text',
                'value' => get_the_title(),
              ]);
            ?>
            <?=
              $THEME->render_form_input([
                'name' => 'rude',
                'type' => 'text',
                'value' => '',
              ]);
            ?>
          </div>
          <div class="form__field form__field--email">
            <?=
              $THEME->render_form_input([
                'classes' => 'form__input input input--primary',
                'name' => '_replyto',
                'placeholder' => 'Your Email',
                'required' => true,
                'type' => 'email'
              ]);
            ?>
          </div>
          <button class="form__button button button--primary" type="submit">
            <span class="button__title"><?= $form_submit_button_title; ?></span>
          </button>
        </div>

        <div class="form__loading">
          <div class="form__spinner"></div>
        </div>

      </form>
    </div>
  <?php endif; ?>

</section>
