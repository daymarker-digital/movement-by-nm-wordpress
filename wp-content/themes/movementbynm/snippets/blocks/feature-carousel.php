<?php

  /**
  *
  *   Feature Carousel Template
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'feature-carousel';
  $block_classes = $block_name . ' block block--' . $block_name;
  $block_data = $block['data'] ?? [];
  $block_id = $block_name . '--' . $block['id'];

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;
  $aos = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'transition' => 'fade-up' ]);

  // ---------------------------------------- ACF | Settings
  $background = $block_data['background'] ?? 'white';
  $padding_bottom = $block_data['padding_bottom'] ?? 0;
  $padding_top = $block_data['padding_top'] ?? 0;

  // ---------------------------------------- ACF Data
  $acf_data = get_field('feature_carousel') ?: [];
  $list = $acf_data['list'] ?: [];
  $glider = count($list) > 1 ? true : false;
  $glider_id = $block_id . '--glider';
  $heading = $acf_data['heading'] ?? '';
  $message = $acf_data['message'] ?? '';

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">

  <?=
    $THEME->render_block_styles([
      'block_id' => $block_id,
      'background' => $background,
      'padding_bottom' => $padding_bottom,
      'padding_top' => $padding_top,
    ]);
  ?>

  <?php if ( $heading || $message ) : ?>
    <div class="<?= $block_name; ?>__intro<?= $list ? ' with-glider' : ''; ?>">
      <?= $THEME->render_container( 'open' ); ?>
        <div class="<?= $block_name; ?>__content">
          <?php if ( $heading ) : ?>
            <h2><?= $heading; ?></h2>
          <?php endif; ?>
          <?php if ( $message ) : ?>
            <div class="body-copy"><?= $message; ?></div>
          <?php endif; ?>
        </div>
      <?= $THEME->render_container( 'closed' ); ?>
    </div>
  <?php endif; ?>

  <?php if ( $list ) : ?>
    <div class="<?= $block_name; ?>__main">

        <?php

          echo $glider ? '<div class="glide js--glider" id="' . $glider_id . '" data-glide-style="' . $block_name . '">' : '';

            if ( $glider ) {
              echo '<button class="glide__button button prev" type="button" data-target="#' . $glider_id . '">';
                echo $THEME->render_svg([ 'type' => 'icon.arrow' ]);
              echo '</button>';
            }

            echo $glider ? '<div class="glide__track" data-glide-el="track">' : '';
              echo $glider ? '<ul class="glide__slides">' : '';

                foreach ( $list as $i => $item ) {

                  $image = $item['image'] ?? [];
                  $caption = $item['caption'] ?? '';
                  $link = '';
                  $link_target = '_self';
                  $link_type = $item['link_type'] ?? 'page';

                  switch ( $link_type ) {
                    case 'external': {
                      $link = $item['link_external'] ?? '';
                      $link_target = '_blank';
                      break;
                    }
                    case 'page': {
                      $link = $item['link_page'] ?? '';
                      break;
                    }
                  }

                  if ( $image ) {
                    echo $glider ? '<li class="glide__slide">' : '';
                      echo '<div class="' . $block_name . '__item">';
                        echo '<div class="' . $block_name . '__image">';
                          echo $link ? '<a href="' . $link . '" target="' . $link_target . '" title="' . $caption . '">' : '';
                            echo $THEME->render_lazyload_image([ 'image' => $image ]);
                          echo $link ? '</a>' : '';
                        echo '</div>';
                        echo $caption ? '<strong class="' . $block_name . '__caption">' . $caption . '</strong>' : '';
                      echo '</div>';
                    echo $glider ? '</li>' : '';
                  }

                }

              echo $glider ? '</ul>' : '';
            echo $glider ? '</div>' : '';
          echo $glider ? '</div>' : '';

        ?>

    </div>
  <?php endif; ?>

</section>
