<?php

if ( isset( $post ) ) {

  // default data
  $photographer_link = $photographer_name = '';
  $post_date = ( get_the_date() ) ? get_the_date("M jS, Y") : false;

  $author = get_the_author_meta( 'first_name' ) ?: '';
  $author .= get_the_author_meta( 'last_name' ) ? ' ' . get_the_author_meta( 'last_name' ) : '';

  if ( have_rows( 'featured' ) ) {
    while ( have_rows( 'featured' ) ) {

      the_row();

      $author = get_sub_field( 'author' ) ?: $author;
      $author_link = get_sub_field( 'author_link' ) ?: '';
      $photographer_link = get_sub_field( 'photographer_link' ) ?: '';
      $photographer_name = get_sub_field( 'photographer_name' ) ?: '';

    }
  }


  // print data
  echo '<div class="article__meta">';

    if ( $author ) {
      echo '<div class="article__author">';
        if ( $author_link ) {
          echo 'Words <span><a href="' . $author_link . '" target="_blank" title="' . $author . '">' . $author . '</a></span>';
        } else {
          echo 'Words <span>' . $author . '</span>';
        }
      echo '</div>';
    }

    if ( $photographer_name ) {
      echo $author ? '<div class="article__meta-spacer">/</div>' : '';
      echo '<div class="article__photographer">';
        if ( $photographer_link ) {
          echo 'Photography <span><a href="' . $photographer_link . '" target="_blank" title="' . $photographer_name . '">' . $photographer_name . '</a></span>';
        } else {
          echo 'Photography <span>' . $photographer_name . '</span>';
        }
      echo '</div>';
    }

    if ( false ) {
      echo '<div class="article__meta-spacer">/</div>';
      echo '<div class="article__date">';
        echo $post_date;
      echo '</div>';
    }



  echo '</div>';

}

?>
