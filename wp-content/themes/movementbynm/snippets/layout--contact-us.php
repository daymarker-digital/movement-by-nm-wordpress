<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

// default data
$block_name = 'contact-us';
$general = $social = false;

if ( have_rows( 'contact' ) ) {
  while ( have_rows( 'contact' ) ) {

    // init data
    the_row();

    // get data
    if ( get_sub_field( 'general' ) ) {
      $general = get_sub_field( 'general' );
    }
    if ( get_sub_field( 'social' ) ) {
      $social = get_sub_field( 'social' );
    }

  }
}


echo '<section class="section section--' . $block_name  . ' ' . $block_name  . '">';
  echo '<div class="container"><div class="row"><div class="col-12 col-sm-10 offset-sm-1">';
   echo '<div class="row row--inner">';

      echo '<div class="col-12 col-md-8 col-lg-9 order-md-2">';
        if ( $general ) {

          echo '<div class="' . $block_name . '__general">';

            // default data
            $form = $heading = false;

            // extract $social
            extract( $general );

            if ( $heading ) {
              echo '<h2 class="' . $block_name  . '__heading heading">' . $heading . '</h2>';
            }

            echo '<div class="' . $block_name . '__form">';
              include( locate_template( './snippets/forms/contact-us-form.php' ) );
            echo '</div>';

          echo '</div>';

        }
      echo '</div>';

      echo '<div class="col-12 col-md-4 col-lg-3 order-md-1">';
        if ( $social ) {

          echo '<div class="' . $block_name . '__social">';

            // default data
            $heading = $links = false;

            // extract $social
            extract( $social );

            // print data
            if ( $heading ) {
              echo '<h2 class="' . $block_name  . '__heading heading">' . $heading . '</h2>';
            }

            if ( $links ) {
              echo '<ul class="' . $block_name  . '__links">';
              foreach( $links as $i => $item ) {

                // default data
                $icon = $link = $type = false;
                $target = '_self';

                extract( $item );

                switch( $type ) {
                  case 'email':
                    $link = 'mailto:' . $link;
                    break;
                  case 'social':
                    $target = '_blank';
                    break;
                }

                if ( $icon && $link ) {
                  echo '<li class="' . $block_name  . '__links-item">';
                    echo '<a href="' . $link . '" target="' . $target . '"><img src="' . $icon['url'] . '" /></a>';
                  echo '</li>';
                }

              }
              echo '</ul>';
            }

          echo '</div>';

        }
      echo '</div>';

    echo '</div>';
  echo '</div></div></div>';
echo '</section>';

?>
