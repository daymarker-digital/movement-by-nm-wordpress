<?php

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$home = $DD->theme_directory('home');
$assets_dir = $DD->theme_directory('assets');
$theme_post_id = $DD->theme_info('post_ID');

$Templates = new DDTemplates();

//////////////////////////////////////////////////////////
////  Snippet Vars
//////////////////////////////////////////////////////////

// default data
$block_name = 'lead-generator';
$form = $heading = $message = false;
$render_args = [];

// get data
if ( have_rows( 'background' ) ) {
  while( have_rows( 'background' ) ) {
    the_row();
    $render_args['class_modifier'] = $block_name;
    $render_args['type'] = get_sub_field('type');
    $render_args['appearance'] = get_sub_field('appearance');
    $render_args['video'] = get_sub_field('video');
    $render_args['image'] = get_sub_field('image');
    $render_args['resize_desktop'] = true;
    $render_args['resize_mobile'] = false;
  }
}
if ( get_sub_field( 'form' ) ) {
  $form = get_sub_field( 'form' );
}
if ( get_sub_field( 'heading' ) ) {
  $heading = get_sub_field( 'heading' );
}
if ( get_sub_field( 'message' ) ) {
  $message = get_sub_field( 'message' );
}

// print data
if ( true ) {
  echo '<section class="section section--' . $block_name . ' ' . $block_name . ' ' . $block_name . '--flexible-content">';

    echo $Templates->render( 'background', $render_args );

    echo '<div class="' . $block_name . '__main">';
      echo '<div class="' . $block_name . '__content">';

        if ( $heading ) {
          echo '<h2 class="' . $block_name . '__heading heading">' . $heading . '</h2>';
        }

        if ( $message ) {
          echo '<div class="' . $block_name . '__message message rte">' . $message . '</div>';
        }

        if ( $form ) {
          echo '<div class="' . $block_name . '__form">';
            echo $Templates->render( 'form', [ 'id' => $form->ID, 'class_modifier' => 'popup' ] );
          echo '</div>';
        }

      echo '</div>';
    echo '</div>';

  echo '</section>';
}

?>
